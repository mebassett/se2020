stack exec elm-code-gen
stack exec se2020-server &
pid=$!
while inotifywait -e modify $(stack path --local-install-root)/bin ; do
    echo "restarting server."
    kill $pid
    stack exec elm-code-gen
    stack exec se2020-server &
    pid=$!
    
done

