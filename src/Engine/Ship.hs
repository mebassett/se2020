module Engine.Ship
  ( basicShip
  , calcAttackingDamage
  , calcCounterDamage
  , shipFightersAvail
  , cargoSize
  , shipFromTemplate
  , Cargo(..)
  , Ship(..)
  , ShortShip(..)
  , shipToShortShip
  , isShipInSpace
  , isShipUnderAttack
  , isShipAttacking
  , isAttackingThisShip
  , ShipStatus(..)
  , ShipTemplate(..)
  )
where
import           BetaDistro                     ( betaDistro )
import           Data.Aeson                     ( FromJSON
                                                , ToJSON
                                                )
import           Data.Text                      ( Text )
import           Data.UUID                      ( UUID )
import           GHC.Generics                   ( Generic )
import qualified Generics.SOP                  as SOP

import           Engine.Ids                     ( StarNum(..)
                                                , ShipNum(..)
                                                , PlayerNum(..)
                                                , PlanetNum(..)
                                                )
import           Control.Monad.Random           ( Rand )
import           System.Random                  ( RandomGen )

data Ship = Ship { shipName :: Text
                 , shipClass :: Text
                 , shipLocation :: StarNum
                 , shipId :: ShipNum
                 , shipMaxCargo :: Double
                 , shipMaxFighters :: Int
                 , shipCargo :: Cargo
                 , shipFighters :: Int
                 , shipOwner :: PlayerNum
                 , shipStatus :: ShipStatus
                 , shipMiningRateFuel :: Double
                 , shipMiningRateMetal :: Double
                 }
  deriving (Generic, Show, SOP.Generic, SOP.HasDatatypeInfo, Eq, Read)
instance ToJSON Ship

data ShortShip = ShortShip { shortShipClass :: Text
                           , shortShipLocation :: StarNum
                           , shortShipId :: ShipNum
                           , shortShipOwner :: PlayerNum
                           , shortShipFighters :: Int }
  deriving (Generic, Show, SOP.Generic, SOP.HasDatatypeInfo, Eq, Read)
instance ToJSON ShortShip
instance FromJSON ShortShip

shipToShortShip :: Ship -> ShortShip
shipToShortShip Ship {..} = ShortShip { shortShipClass    = shipClass
                                      , shortShipLocation = shipLocation
                                      , shortShipId       = shipId
                                      , shortShipOwner    = shipOwner
                                      , shortShipFighters = shipFighters
                                      }

data ShipStatus = SSSpaceIdle
                | SSSpaceMiningMetal
                | SSSpaceMiningFuel
                | SSSpaceAttacking ShipNum Int
                | SSSpaceAttackedBy [ShipNum]
                | SSTowedBy ShipNum
                | SSDocked
                | SSPlanet PlanetNum
  deriving (Eq, Generic, Read, Show, SOP.Generic, SOP.HasDatatypeInfo)
instance ToJSON ShipStatus

isShipInSpace :: ShipStatus -> Bool
isShipInSpace SSSpaceIdle            = True
isShipInSpace SSSpaceMiningMetal     = True
isShipInSpace SSSpaceMiningFuel      = True
isShipInSpace (SSTowedBy _         ) = True
isShipInSpace (SSSpaceAttacking _ _) = True
isShipInSpace (SSSpaceAttackedBy _ ) = True
isShipInSpace _                      = False

isShipUnderAttack :: Ship -> Bool
isShipUnderAttack = helper . shipStatus
 where
  helper (SSSpaceAttackedBy _) = True
  helper _                     = False

isShipAttacking :: Ship -> Bool
isShipAttacking = helper . shipStatus
 where
  helper (SSSpaceAttacking _ _) = True
  helper _                      = False

isAttackingThisShip :: ShipNum -> Ship -> Bool
isAttackingThisShip targetId = helper . shipStatus
 where
  helper (SSSpaceAttacking sid _) = sid == targetId
  helper _                        = False

data Cargo = Cargo { cargoMetal :: Double
                   , cargoFuel :: Double
                   , cargoElec :: Double
                   , cargoOrgan :: Double }
  deriving (Eq, Generic, Show, SOP.Generic, SOP.HasDatatypeInfo, Read)
instance ToJSON Cargo

emptyCargo :: Cargo
emptyCargo = Cargo 0.0 0.0 0.0 0.0

cargoSize :: Cargo -> Double
cargoSize c = cargoMetal c + cargoFuel c + cargoElec c + cargoOrgan c

data ShipTemplate = ShipTemplate { shipTClass :: Text
                                 , shipTPrice :: Double
                                 , shipTCargo :: Double
                                 , shipTFighters :: Int
                                 , shipTMiningRateFuel :: Double
                                 , shipTMiningRateMetal :: Double }
  deriving (Eq, Generic, Read, Show, SOP.Generic, SOP.HasDatatypeInfo)
instance ToJSON ShipTemplate
instance FromJSON ShipTemplate

shipFromTemplate :: PlayerNum -> UUID -> StarNum -> Text -> ShipTemplate -> Ship
shipFromTemplate owner shipNum location name template = Ship
  { shipName            = name
  , shipClass           = shipTClass template
  , shipLocation        = location
  , shipId              = ShipNum shipNum
  , shipOwner           = owner
  , shipMaxCargo        = shipTCargo template
  , shipMaxFighters     = shipTFighters template
  , shipCargo           = emptyCargo
  , shipFighters        = 0
  , shipMiningRateFuel  = shipTMiningRateFuel template
  , shipMiningRateMetal = shipTMiningRateMetal template
  , shipStatus          = SSSpaceIdle
  }

basicShip :: ShipTemplate
basicShip = ShipTemplate { shipTClass           = "Basic"
                         , shipTPrice           = 1000.0
                         , shipTCargo           = 150.0
                         , shipTFighters        = 12
                         , shipTMiningRateFuel  = 12.0 / (60.0 * 60.0)
                         , shipTMiningRateMetal = 7.0 / (60.0 * 60.0)
                         }


shipFightersAvail :: Double -> Ship -> Int
shipFightersAvail fuelRate ship = min
  (shipFighters ship)
  (floor ((cargoFuel . shipCargo) ship / (2 * fuelRate)))

calcAttackingDamage :: (RandomGen g) => Double -> Ship -> Rand g Int
calcAttackingDamage fuelRate attacker = damageDoneToDefendingShip
 where
  damageDoneToDefendingShip =
    let figsAvail    = fromIntegral $ shipFightersAvail fuelRate attacker
        attackerTime = case shipStatus attacker of
          SSSpaceAttacking _ t -> fromIntegral t
          _                    -> 1
    in  do
          x <- attackingDistro attackerTime
          return $ round $ x * figsAvail
  attackingDistro gameTicks = betaDistro (0.1 * gameTicks) 5.0 -- as per docs

calcCounterDamage :: (RandomGen g) => Ship -> Rand g Int
calcCounterDamage defender = case shipStatus defender of
  SSSpaceAttackedBy attackerIds ->
    damageDoneToAttackingShip $ length attackerIds
  _ -> pure 0
 where
  damageDoneToAttackingShip numShips = do
    x <- betaDistro alpha beta
    return $ floor
      ((x * fromIntegral (shipFighters defender)) / fromIntegral numShips)

  alpha = 1.0 -- as per docs, increasing this number means the longer defending ships last.
  beta  = 5.0 -- as per docks  
