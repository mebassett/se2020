module Engine.Ids
  ( StarNum(..)
  , ShipNum(..)
  , PlayerNum(..)
  , PlanetNum(..)
  )
where

import           Data.Aeson                     ( FromJSON
                                                , ToJSON
                                                )
import qualified Data.UUID                     as UUID
import qualified Generics.SOP                  as SOP
import           GHC.Generics                   ( Generic )

newtype StarNum = StarNum Int
  deriving (Eq, Generic, Ord, Read, Show, SOP.Generic, SOP.HasDatatypeInfo)
instance ToJSON StarNum
instance FromJSON StarNum

newtype PlanetNum = PlanetNum UUID.UUID
  deriving (Eq, Generic, Ord, Read, Show, SOP.Generic, SOP.HasDatatypeInfo)
instance ToJSON PlanetNum
instance FromJSON PlanetNum

newtype ShipNum = ShipNum UUID.UUID
  deriving (Eq, Generic, Ord, Read, Show, SOP.Generic, SOP.HasDatatypeInfo)
instance ToJSON ShipNum
instance FromJSON ShipNum

newtype PlayerNum = PlayerNum UUID.UUID
  deriving (Eq, Generic, Ord, Read, Show, SOP.Generic, SOP.HasDatatypeInfo)
instance ToJSON PlayerNum
instance FromJSON PlayerNum
