module Engine.GameTick
  ( gameTick
  )
where
import           Engine.Action                  ( runAction
                                                , GameAction(..)
                                                , Universe(..)
                                                )
import           Engine.Ship                   as Ship
import           Engine.Ids
import           Engine.Star                   as Star
import           Engine.Player                 as Player
import           GameConfig                     ( GameConfig(..) )
import qualified Notifications                 as Note
import           Control.Monad                  ( foldM
                                                , forM_
                                                , when
                                                )
import           Control.Monad.Random           ( Rand )
import           Control.Monad.Reader           ( ReaderT(..) )
import           Control.Monad.ST               ( ST(..)
                                                , runST
                                                )
import           Control.Monad.Trans            ( MonadTrans(..) )
import           Control.Monad.Writer.Strict    ( WriterT
                                                , MonadWriter(..)
                                                , runWriterT
                                                )
import           Control.Monad.State.Strict     ( StateT
                                                , MonadState(..)
                                                , modify'
                                                , execStateT
                                                )

import           Data.Foldable                  ( foldlM )
import qualified Data.IntMap.Strict            as IMap
import           Data.List                      ( (\\)
                                                , sortOn
                                                )
import qualified Data.Map.Strict               as Map
import qualified Data.Maybe                    as Maybe


import           System.Random                  ( RandomGen )





type ExecutableUniverse g = StateT Universe (WriterT [Note.GameMsg] (Rand g)) ()

liftRand
  :: (RandomGen g)
  => Rand g a
  -> StateT Universe (WriterT [Note.GameMsg] (Rand g)) a
liftRand = lift . lift

gameTick
  :: RandomGen g
  => [GameAction]
  -> Universe
  -> Rand g (Universe, [Note.GameMsg])
gameTick actions = runWriterT . execStateT (doGameTick actions)


doGameTick :: RandomGen g => [GameAction] -> ExecutableUniverse g
doGameTick actions = do
  doActions actions
  doMining
  doAttacks

canceledDefender :: ShipNum -> Ship -> Ship
canceledDefender attId defender = case shipStatus defender of
  SSSpaceAttackedBy attackerIds
    | attackerIds == [attId] -> defender { shipStatus = SSSpaceIdle }
    | attId `notElem` attackerIds -> defender
    | otherwise -> defender
      { shipStatus = SSSpaceAttackedBy $ attackerIds \\ [attId]
      }
  _ -> defender

shipDistance :: Universe -> Ship -> Ship -> Double
shipDistance uni centerShip farShip =
  let stars                = uniStars uni
      StarNum centerStarId = shipLocation centerShip
      StarNum farStarId    = shipLocation farShip
      c                    = stars IMap.! centerStarId
      f                    = stars IMap.! farStarId
      dist                 = sqrt
        ( (starXLoc c - starXLoc f)
        ^ 2
        + (starYLoc c - starYLoc f)
        ^ 2
        + (starZLoc c - starZLoc f)
        ^ 2
        )
  in  dist

removeShipFromUni :: ShipNum -> Universe -> Universe
removeShipFromUni shipNum uni =
  let newStars = IMap.map (\s -> s { starShips = starShips s \\ [shipNum] })
        $ uniStars uni
      newShips =
          Map.map
              (\s -> case shipStatus s of
                SSSpaceAttacking targetId _
                  | targetId == shipNum -> s { shipStatus = SSSpaceIdle }
                  | otherwise           -> s

                SSSpaceAttackedBy attackerIds
                  | attackerIds == [shipNum] -> s { shipStatus = SSSpaceIdle }
                  | otherwise -> s
                    { shipStatus = SSSpaceAttackedBy $ attackerIds \\ [shipNum]
                    }
                _ -> s
              )
            $ Map.delete shipNum
            $ uniShips uni
      newPlayers =
          Map.map
              (\p -> if playerCmdShip p == Just shipNum
                then p { playerCmdShip = Nothing }
                else p
              )
            $ uniPlayers uni
  in  uni { uniStars = newStars, uniShips = newShips, uniPlayers = newPlayers }



doAttacks :: RandomGen g => ExecutableUniverse g
doAttacks = do
  cleanup
  uni <- get
  forM_ (filter isShipAttacking $ Map.elems $ uniShips uni) $ \attacker ->
    case shipStatus attacker of
      SSSpaceAttacking defenderId _ ->
        let
          defender = uniShips uni Map.! defenderId
          attId    = shipId attacker
          canceledAttack =
            let newAttacker = attacker { shipStatus = SSSpaceIdle }
                newDefender = canceledDefender attId defender
                newShips =
                    Map.adjust (const newAttacker) attId
                      $ Map.adjust (const newDefender) defenderId
                      $ uniShips uni
            in  uni { uniShips = newShips }
          prepMsg recipientId msg = tell $ return Note.GameMsg
            { gameMsgSender    = Player.GamePlayer $ shipOwner defender
            , gameMsgRecipient = recipientId
            , gameMsgDatum     = msg
            }
        -- first let's cancel invalid attacks.
        -- an invalid attack is one where any one of the following conditions are true:
        -- 1) the ships are no longer in the same system
        -- 2) the attacking ship has run out of fuel
        -- 3) the attacking ship has no more fighters.
        -- if the attack becomes invalid, we notify the attacking ship / owner as to why
        -- and set the status of the ships to reflect what's happened.
        -- see docs/ship_attack_diagram.png
        in
          if shipLocation attacker /= shipLocation defender
            then do
              prepMsg (shipOwner attacker)
                $ Note.MTAttackCanceledShipMoved (shipToShortShip attacker)
                $ shipToShortShip defender
              put canceledAttack
            else
              if (cargoFuel . shipCargo) attacker
                 < (gameCfgRateFuelForAttack . uniCfg) uni
              then
                do
                  prepMsg (shipOwner attacker) $ Note.MTAttackCanceledNoFuel
                    (shipToShortShip attacker)
                    (shipToShortShip defender)
                  put canceledAttack
              else
                if shipFighters attacker < 1
                  then do
                    prepMsg (shipOwner attacker) $ Note.MTAttackCanceledNoFigs
                      (shipToShortShip attacker)
                      (shipToShortShip defender)
                    put canceledAttack
                  else
                    let fuelRate = gameCfgRateFuelForAttack $ uniCfg uni
                    in
                      do -- now we can actually attack!
                        damageByAttacker <- liftRand
                          $ calcAttackingDamage fuelRate attacker
                        let attackerFuelCost =
                              (fromIntegral damageByAttacker + fromIntegral
                                  (shipFightersAvail fuelRate attacker)
                                )
                                * fuelRate
                        chargeFuel attacker attackerFuelCost
                        if damageByAttacker > shipFighters defender
                          then
                            prepMsg (shipOwner defender) $ Note.MTShipDestroyed
                              (shipToShortShip defender)
                              (     playerToShortPlayer
                              $     uniPlayers uni
                              Map.! (shipOwner attacker)
                              )
                          else prepMsg (shipOwner defender) $ Note.MTUnderAttack
                            (shipToShortShip defender)
                            (     playerToShortPlayer
                            $     uniPlayers uni
                            Map.! (shipOwner attacker)
                            )

                        doDamage defender damageByAttacker
                        damageByDefender <- liftRand
                          $ calcCounterDamage defender
                        doDamage attacker damageByDefender
                        when (damageByDefender > shipFighters attacker)
                          $ prepMsg (shipOwner attacker)
                          $ Note.MTAttackCanceledShipDestroyed
                              (shipToShortShip attacker)
      _ -> return ()
 where
  chargeFuel ship fuel = modify'
    (\uni ->
      let
        cargo    = shipCargo ship
        newCargo = cargo { cargoFuel = max 0 (cargoFuel cargo - fuel) }
        newShips = Map.adjust (\s -> s { shipCargo = newCargo }) (shipId ship)
          $ uniShips uni
      in
        uni { uniShips = newShips }
    )

  doDamage ship damage = if damage > shipFighters ship
    then destroyShip (shipId ship)
    else modify'
      (\uni ->
        let newShips =
                Map.adjust (\s -> s { shipFighters = shipFighters s - damage })
                           (shipId ship)
                  $ uniShips uni
        in  uni { uniShips = newShips }
      )

  destroyShip shipNum = do
    uni <- get
    let ship    = uniShips uni Map.! shipNum
        ownerId = shipOwner ship
        owner   = uniPlayers uni Map.! ownerId
    when (isShipAttacking ship)
      $ let (SSSpaceAttacking targetId _) = shipStatus ship
            newTargetShip =
              canceledDefender shipNum $ uniShips uni Map.! targetId
            newShips = Map.adjust (const newTargetShip) targetId $ uniShips uni
        in  put $ uni { uniShips = newShips }
    when (playerCmdShip owner == Just shipNum)
      $ let otherShips =
              sortOn (shipDistance uni ship)
                $ filter (\s -> shipId s /= shipNum && shipOwner s == ownerId)
                $ Map.elems
                $ uniShips uni
            newPlayers =
              Map.adjust
                  (\p -> if
                    | null otherShips -> p { playerCmdShip = Nothing }
                    | otherwise -> p
                      { playerCmdShip = Just $ shipId $ head otherShips
                      }
                  )
                  ownerId
                $ uniPlayers uni
        in  put $ uni { uniPlayers = newPlayers }
    modify' $ removeShipFromUni shipNum


  -- ensure that the SSSpaceAttacking / SSSpaceAttackedBy statuses agree.
  -- while we are at it, increase attacking tick time.
  cleanup = modify'
    (\uni ->
      let newShips =
              Map.map
                  (\s -> case shipStatus s of
                    SSSpaceAttackedBy attackerIds ->
                      let newAttackerIds = filter
                            (\sid ->
                              let attackingShip = uniShips uni Map.! sid
                              in  isAttackingThisShip (shipId s) attackingShip
                            )

                            attackerIds
                      in  if null newAttackerIds
                            then s { shipStatus = SSSpaceIdle }
                            else s { shipStatus = SSSpaceAttackedBy newAttackerIds
                                   }
                    SSSpaceAttacking targetId tickTime ->
                      let targetShip       = uniShips uni Map.! targetId
                          attackingShipIds = case shipStatus targetShip of
                            SSSpaceAttackedBy attackers -> attackers
                            _                           -> []
                      in  if shipId s `notElem` attackingShipIds
                            then s { shipStatus = SSSpaceIdle }
                            else s
                              { shipStatus = SSSpaceAttacking targetId
                                                              (tickTime + 1)
                              }
                    _ -> s
                  )
                $ uniShips uni
      in  uni { uniShips = newShips }
    )

doActions :: [GameAction] -> ExecutableUniverse g
doActions actions = modify' $ \universe -> foldl runAction universe actions

doMining :: ExecutableUniverse g
doMining = do
  uni      <- get
  minedUni <- foldlM doMiningStar uni $ uniStars uni
  put minedUni
 where
  miningCutoff = 0.00001

  shipMiningRate ship =
    let rate =
            (case shipStatus ship of
                SSSpaceMiningMetal -> shipMiningRateMetal
                SSSpaceMiningFuel  -> shipMiningRateFuel
                _                  -> const 0
              )
              ship
        miningAvail = shipMaxCargo ship - cargoSize (shipCargo ship)
    in  min rate miningAvail

  shipMine metalFactor fuelFactor ship =
    let
      oldCargo = shipCargo ship
      newCargo
        | shipStatus ship == SSSpaceMiningMetal = oldCargo
          { cargoMetal = cargoMetal oldCargo
                           + (shipMiningRate ship * metalFactor)
          }
        | shipStatus ship == SSSpaceMiningFuel = oldCargo
          { cargoFuel = cargoFuel oldCargo + (shipMiningRate ship * fuelFactor)
          }
        | otherwise = oldCargo
      currentStatus = shipStatus ship
    in
      if (  currentStatus
         == SSSpaceMiningFuel
         || currentStatus
         == SSSpaceMiningMetal
         )
         && shipMiningRate ship
         <  miningCutoff
      then
        do
          tell $ return Note.GameMsg
            { gameMsgSender    = Player.GameAdmin
            , gameMsgRecipient = shipOwner ship
            , gameMsgDatum     = Note.MTShipCargoFull $ shipToShortShip ship
            }
          return ship { shipStatus = SSSpaceIdle }
      else
        return ship { shipCargo = newCargo }



  doMiningStar uni' star =
    let
      totalMiningMetal =
        sum
          $ Prelude.map shipMiningRate
          $ Prelude.filter (\s -> shipStatus s == SSSpaceMiningMetal)
          $ Maybe.mapMaybe (`Map.lookup` uniShips uni')
          $ starShips star
      totalMiningFuel =
        sum
          $ Prelude.map shipMiningRate
          $ Prelude.filter (\s -> shipStatus s == SSSpaceMiningFuel)
          $ Maybe.mapMaybe (`Map.lookup` uniShips uni')
          $ starShips star
      metalFactor =
        if starMetal star > totalMiningMetal || totalMiningMetal == 0.0
          then 1.0
          else starMetal star / totalMiningMetal
      fuelFactor = if starFuel star > totalMiningFuel || totalMiningFuel == 0.0
        then 1.0
        else starFuel star / totalMiningFuel
      (StarNum sid) = starId star
    in
      do
        minedShips <-
          mapM
              (\sh ->
                if shipLocation sh
                   == starId star
                   && (isShipInSpace . shipStatus) sh
                then
                  shipMine metalFactor fuelFactor sh
                else
                  return sh
              )
            $ uniShips uni'
        return $ uni'
          { uniStars = IMap.adjust
                           (const star
                             { starMetal = starMetal star
                                             - min (starMetal star)
                                                   totalMiningMetal
                             , starFuel  = starFuel star
                                             - min (starFuel star)
                                                   totalMiningFuel
                             }
                           )
                           sid
                         $ uniStars uni'
          , uniShips = minedShips
          }
