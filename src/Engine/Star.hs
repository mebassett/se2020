{-# LANGUAGE TypeFamilies #-}
module Engine.Star
  ( mkSol
  , solEarth
  , Planet(..)
  , PlanetData(..)
  , HomeworldData(..)
  , ShortPlanet(..)
  , Port(..)
  , Star(..)
  , DisplayableStar(..)
  , starDistance
  , StarNum(..)
  , mkStar
  )
where
import           BetaDistro                     ( bernoulliDistro
                                                , normalDistro
                                                )
import           GameConfig                     ( GameConfig(..)
                                                , GalaxyConfiguration(..)
                                                )
import           Engine.Ids                     ( StarNum(..)
                                                , ShipNum(..)
                                                , PlanetNum(..)
                                                , PlayerNum(..)
                                                )
import           Engine.Ship                    ( ShipTemplate
                                                , basicShip
                                                )
import           Data.Aeson                     ( ToJSON )
import           Control.Monad.Random           ( Rand
                                                , liftRand
                                                , MonadRandom(..)
                                                )
import           System.Random                  ( RandomGen(..) )

import           Data.Set                       ( Set
                                                , empty
                                                )
import           Data.Text                      ( Text )
import           Data.UUID                      ( UUID )
import qualified Data.Random.Normal            as Normal
import           GHC.Generics                   ( Generic )
import qualified Generics.SOP                  as SOP

data Port = StarPort { portFuel :: Double
                     , portMetal :: Double
                     , portElec :: Double
                     , portOrgan :: Double
                     , portMaxShips :: Int
                     , portCurrentShips :: Int
                     }
  deriving (Generic, Show, SOP.Generic, SOP.HasDatatypeInfo, Eq, Read)
instance ToJSON Port

data Star = Star { starName :: Text
                 , starId :: StarNum
                 , starXLoc :: Double
                 , starYLoc :: Double
                 , starZLoc :: Double
                 , starSize :: Int
                 , starFuel :: Double
                 , starMetal :: Double
                 , starPort :: Maybe Port
                 , starLinks :: Set StarNum
                 , starPlanets :: [PlanetNum]
                 , starShips :: [ShipNum] }
  deriving (Generic, Show, SOP.Generic, SOP.HasDatatypeInfo, Eq, Read)
instance ToJSON Star

data DisplayableStar = DisplayableStar
  { displayStarName :: Maybe Text
  , displayStarId :: Maybe StarNum
  , displayStarXLoc :: Double
  , displayStarYLoc :: Double
  , displayStarZLoc :: Double
  , displayStarSize :: Int
  , displayStarLinks :: [(Double, (Double, Double))]
  } deriving (Generic, Show, SOP.Generic, SOP.HasDatatypeInfo, Eq, Read)
instance ToJSON DisplayableStar

starDistance :: Star -> Star -> Double
starDistance s1 s2 =
  sqrt $ (starXLoc s1 - starXLoc s2) ^ 2 + (starYLoc s1 - starYLoc s2) ^ 2

calcStarResource
  :: RandomGen g
  => (GameConfig -> (Double, Double))
  -> GameConfig
  -> Rand g Double
calcStarResource resourceGetter cfg = do
  amount <- liftRand $ Normal.normal' $ resourceGetter cfg
  if amount > 0 then return amount else return 0

calcStarPort :: RandomGen g => GameConfig -> Rand g (Maybe Port)
calcStarPort cfg =
  getRandom
    >>= (\randVal -> if (randVal :: Double) < gameCfgPortProb cfg
          then return $ Just solPort
          else return Nothing
        )

mkStarLocation
  :: RandomGen g => GalaxyConfiguration -> Rand g (Double, Double, Double)
mkStarLocation (GalaxyClusters centers) = do
  xLoc <- getRandomR (-50, 50)
  yLoc <- getRandomR (-50, 50)
  zLoc <- getRandomR (-50, 50)
  let distance = minimum (map (dist (xLoc, yLoc, zLoc)) centers)
      prob     = 1 - distance
  closeEnough <- bernoulliDistro prob
  if closeEnough
    then return (xLoc, yLoc, zLoc)
    else mkStarLocation (GalaxyClusters centers)
 where
  dist (x1, y1, z1) (x2, y2, z2, rad) =
    min 1 (sqrt ((x1 - x2) ^ 2 + (y1 - y2) ^ 2 + (z1 - z2) ^ 2) / rad)

mkStarLocation GalaxySpiral {..} =
  let
    scatterRadius = spiralMinRadius * 0.4
    armAngleDeg   = 360
    spiralB       = (armAngleDeg / pi) * (spiralMinRadius / spiralMaxRadius)
    scatterTheta  = pi / fromIntegral spiralNumArms * 0.2
  in
    do
      r <-
        (+)
        <$> getRandomR (spiralMinRadius, spiralMaxRadius)
        <*> normalDistro 0 scatterRadius
      arm        <- fromIntegral <$> getRandomR (0, spiralNumArms - 1)
      thetaNoise <- normalDistro 0 scatterTheta
      let theta =
            spiralB
              * log (r / spiralMaxRadius)
              + thetaNoise
              + (arm * pi * 2 / fromIntegral spiralNumArms)
      z <- normalDistro 0 (spiralThickness * 0.5)
      return
        ( r * cos theta * spiralScale
        , r * sin theta * spiralScale
        , z * spiralScale
        )



mkStar :: RandomGen g => GameConfig -> Rand g Star
mkStar cfg = do
  (xLoc, yLoc, zLoc) <- mkStarLocation $ gameCfgGalaxy cfg
  size               <- getRandomR (1, 5)
  metalAmount        <- calcStarResource gameCfgNormalParamsMetal cfg
  fuelAmount         <- calcStarResource gameCfgNormalParamsFuel cfg
  thePort            <- calcStarPort cfg
  return Star { starName    = "TODO star name generator"
              , starId      = StarNum 0
              , starXLoc    = xLoc
              , starYLoc    = yLoc
              , starZLoc    = zLoc
              , starSize    = size
              , starFuel    = fuelAmount
              , starMetal   = metalAmount
              , starLinks   = empty
              , starPort    = thePort
              , starPlanets = []
              , starShips   = []
              }


data Planet = PlanetHomeworld HomeworldData
            | Planet PlanetData -- this is silly. 
                                -- haskell-to-elm has trouble making decoders
                                -- things like data = A Int String
                                --                  | B { struct ... }
                                -- I haven't reported a bug or investigated 
                                -- further because I'm lazy.
  deriving (Eq, Generic, Show, SOP.Generic, SOP.HasDatatypeInfo, Read)
instance ToJSON Planet

data PlanetData = PlanetData { planetId :: PlanetNum
                     , planetName :: Text
                     , planetOwner :: Maybe PlayerNum }
  deriving (Eq, Generic, Show, SOP.Generic, SOP.HasDatatypeInfo, Read)
instance ToJSON PlanetData

data HomeworldData = HomeworldData { homeworldId :: PlanetNum
                                   , homeworldName :: Text
                                   , homeworldShipsAvailable :: [ ShipTemplate ]
                                   , homeworldFighterCost :: Double
                                   }
  deriving (Eq, Generic, Show, SOP.Generic, SOP.HasDatatypeInfo, Read)
instance ToJSON HomeworldData


data ShortPlanet = ShortPlanet { shortPlanetId :: PlanetNum
                               , shortPlanetName :: Text }
  deriving (Eq, Generic, Show, SOP.Generic, SOP.HasDatatypeInfo, Read)
instance ToJSON ShortPlanet

solPort :: Port
solPort = StarPort { portMetal        = 1000000
                   , portFuel         = 1000000
                   , portElec         = 1000000
                   , portOrgan        = 1000000
                   , portMaxShips     = 100
                   , portCurrentShips = 0
                   }

solEarth :: UUID -> Planet
solEarth uuid =
  PlanetHomeworld $ HomeworldData (PlanetNum uuid) "Earth" [basicShip] 1000.0


sol :: UUID -> Star
sol uuid = Star "Sol"
                (StarNum 1)
                0.0
                0.0
                0.0
                3
                0
                0
                (Just solPort)
                empty
                [PlanetNum uuid]
                []

mkSol :: RandomGen g => GameConfig -> UUID -> Rand g Star
mkSol cfg uuid = do
  (xLoc, yLoc, zLoc) <- mkStarLocation $ gameCfgGalaxy cfg
  let s = sol uuid
  return $ s { starXLoc = xLoc, starYLoc = yLoc, starZLoc = zLoc }
