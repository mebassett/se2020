module Engine.Action
  ( runAction
  , ActionType(..)
  , GameAction(..)
  , Universe(..)
  )
where
import           Debug.Trace                    ( traceShow )
import           Engine.Ids
import           Engine.Player                 as Player
import           Engine.Star                   as Star
import           Engine.Ship                   as Ship
import           GameConfig                     ( GameConfig(..) )
import           Data.IntMap.Strict             ( (!) )
import qualified Data.IntMap.Strict            as IMap
import           Data.List                      ( nub
                                                , (\\)
                                                )
import qualified Data.Map.Strict               as Map
import qualified Data.Maybe                    as Maybe
import qualified Data.Set                      as Set
import           Data.Text                      ( Text )
import           Data.UUID                      ( UUID )


data Universe = Universe
  { uniStars :: IMap.IntMap Star.Star
  , uniShips :: Map.Map ShipNum Ship
  , uniPlanets :: Map.Map PlanetNum Planet
  , uniPlayers :: Map.Map PlayerNum Player
  , uniCfg :: GameConfig
  } deriving (Show, Read)

data GameAction = GA Player.GameUser ActionType
  deriving (Read, Show)

data ActionType
  = ATDockShip ShipNum
  | ATUndockShip ShipNum
  | ATDockBuyFuel ShipNum Double
  | ATDockBuyMetal ShipNum Double
  | ATDockSellFuel ShipNum
  | ATDockSellMetal ShipNum
  | ATIntroducePlayer Player
  | ATMoveShip ShipNum StarNum
  | ATNewShip StarNum Ship
  | ATPurchaseShip UUID ShipTemplate Text
  | ATChangeCommand ShipNum
  | ATPurchaseFighters ShipNum Int
  | ATShipStatus ShipNum ShipStatus
  | ATAttackShip ShipNum ShipNum
  | ATStopAttack ShipNum
  deriving (Read, Show)


runAction :: Universe -> GameAction -> Universe
runAction uni (GA user action) = if affordsAction user action
  then case action of
    ATDockShip shid ->
      let
        ship             = uniShips uni Map.! shid
        (StarNum starId) = shipLocation ship
        star             = uniStars uni ! starId
        newShips =
          Map.adjust (\s -> s { shipStatus = SSDocked }) shid $ uniShips uni
      in
        if Maybe.isJust $ starPort star
          then uni { uniShips = newShips }
          else uni

    ATUndockShip shid ->
      let newShips = Map.adjust (\s -> s { shipStatus = SSSpaceIdle }) shid
            $ uniShips uni
      in  uni { uniShips = newShips }

    ATDockBuyFuel shipNum amount' ->
      let
        ship             = uniShips uni Map.! shipNum
        (StarNum starId) = shipLocation ship
        star             = getShipStar ship
        port             = Maybe.fromJust $ starPort star -- affordsAction checks here.
        player           = getShipOwner ship
        maxAfford = roundDown $ (playerCash player / getFuelBuyPrice star)
        maxCargo         = shipMaxCargo ship - (cargoSize . shipCargo) ship
        maxFuel          = min maxAfford maxCargo
        amount''         = min maxFuel $ portFuel port
        amount           = min amount' amount''
        cost             = amount * getFuelBuyPrice star

        newCargo =
          (shipCargo ship) { cargoFuel = (cargoFuel . shipCargo) ship + amount }
        newPlayer   = player { playerCash = playerCash player - cost }
        newStarPort = port { portFuel = portFuel port - amount }
        newPlayers =
          Map.adjust (const newPlayer) (shipOwner ship) (uniPlayers uni)
        newShips =
          Map.adjust (\s -> s { shipCargo = newCargo }) shipNum (uniShips uni)
        newStars = IMap.adjust (\s -> s { starPort = Just newStarPort })
                               starId
                               (uniStars uni)
      in

        uni { uniShips   = newShips
            , uniPlayers = newPlayers
            , uniStars   = newStars
            }
    ATDockBuyMetal shipNum amount' ->
      let
        ship             = uniShips uni Map.! shipNum
        (StarNum starId) = shipLocation ship
        star             = getShipStar ship
        port             = Maybe.fromJust $ starPort star -- affordsAction checks here.
        player           = getShipOwner ship
        maxAfford = roundDown $ (playerCash player / getMetalBuyPrice star)
        maxCargo         = shipMaxCargo ship - (cargoSize . shipCargo) ship
        maxMetal         = min maxAfford maxCargo
        amount''         = min maxMetal $ portMetal port
        amount           = min amount' amount''
        cost             = amount * getMetalBuyPrice star

        newCargo         = (shipCargo ship)
          { cargoMetal = (cargoMetal . shipCargo) ship + amount
          }
        newPlayer   = player { playerCash = playerCash player - cost }
        newStarPort = port { portMetal = portMetal port - amount }
        newPlayers =
          Map.adjust (const newPlayer) (shipOwner ship) (uniPlayers uni)
        newShips =
          Map.adjust (\s -> s { shipCargo = newCargo }) shipNum (uniShips uni)
        newStars = IMap.adjust (\s -> s { starPort = Just newStarPort })
                               starId
                               (uniStars uni)
      in

        uni { uniShips   = newShips
            , uniPlayers = newPlayers
            , uniStars   = newStars
            }

    ATDockSellFuel shipNum ->
      let
        ship             = uniShips uni Map.! shipNum
        (StarNum starId) = shipLocation ship
        star             = getShipStar ship
        port             = Maybe.fromJust $ starPort star -- affordsAction checks here.
        player           = getShipOwner ship
        fuelPrice        = getFuelSellPrice star * (cargoFuel . shipCargo) ship

        newCargo         = (shipCargo ship) { cargoFuel = 0.0 }
        newPlayer        = player { playerCash = playerCash player + fuelPrice }
        newStarPort =
          port { portFuel = portFuel port + (cargoFuel . shipCargo) ship }
        newPlayers =
          Map.adjust (const newPlayer) (shipOwner ship) (uniPlayers uni)
        newShips =
          Map.adjust (\s -> s { shipCargo = newCargo }) shipNum (uniShips uni)
        newStars = IMap.adjust (\s -> s { starPort = Just newStarPort })
                               starId
                               (uniStars uni)
      in
        uni { uniShips   = newShips
            , uniPlayers = newPlayers
            , uniStars   = newStars
            }

    ATDockSellMetal shipNum ->
      let
        ship             = uniShips uni Map.! shipNum
        (StarNum starId) = shipLocation ship
        star             = getShipStar ship
        port             = Maybe.fromJust $ starPort star -- affordsAction checks here.
        player           = getShipOwner ship
        metalPrice = getMetalSellPrice star * (cargoMetal . shipCargo) ship

        newCargo         = (shipCargo ship) { cargoMetal = 0.0 }
        newPlayer = player { playerCash = playerCash player + metalPrice }
        newStarPort =
          port { portMetal = portMetal port + (cargoMetal . shipCargo) ship }
        newPlayers =
          Map.adjust (const newPlayer) (shipOwner ship) (uniPlayers uni)
        newShips =
          Map.adjust (\s -> s { shipCargo = newCargo }) shipNum (uniShips uni)
        newStars = IMap.adjust (\s -> s { starPort = Just newStarPort })
                               starId
                               (uniStars uni)
      in
        uni { uniShips   = newShips
            , uniPlayers = newPlayers
            , uniStars   = newStars
            }


    ATIntroducePlayer player ->
      uni { uniPlayers = Map.insert (playerId player) player $ uniPlayers uni }
    ATNewShip (StarNum sId) ship ->
      let
        star  = uniStars uni ! sId
        owner = uniPlayers uni Map.! shipOwner ship
      in
        uni
          { uniStars   =
            IMap.adjust
                (\s -> s { starShips = starShips star ++ [shipId ship] })
                sId
              $ uniStars uni
          , uniShips   = Map.insert (shipId ship) ship $ uniShips uni
          , uniPlayers = if Maybe.isNothing $ playerCmdShip owner
            then
              Map.adjust (\p -> p { playerCmdShip = Just (shipId ship) })
                         (shipOwner ship)
                $ uniPlayers uni
            else uniPlayers uni
          }
    ATPurchaseShip uuid shipTemplate shipName -> case user of
      Player.GameAdmin -> uni
      Player.GamePlayer playerId ->
        let
          player = uniPlayers uni Map.! playerId
          star   = case playerCmdShip player of
            Nothing -> uniStars uni ! 1 -- magic number for sol
            Just cmdShipId ->
              let ship = uniShips uni Map.! cmdShipId in getShipStar ship
          (StarNum sid) = starId star
          newShip =
            shipFromTemplate playerId uuid (StarNum sid) shipName shipTemplate
          newPlayer = player
            { playerCash    = playerCash player - shipTPrice shipTemplate
            , playerCmdShip =
              return $ Maybe.fromMaybe (shipId newShip) $ playerCmdShip player
            }
          newStar = star { starShips = starShips star ++ [shipId newShip] }
        in
          uni
            { uniPlayers = Map.adjust (const newPlayer) playerId
                             $ uniPlayers uni
            , uniShips   = Map.insert (shipId newShip) newShip $ uniShips uni
            , uniStars   = IMap.adjust (const newStar) sid $ uniStars uni
            }
    ATChangeCommand newCmdId -> case user of
      Player.GameAdmin           -> uni
      Player.GamePlayer playerId -> uni
        { uniPlayers =
          Map.adjust (\p -> p { playerCmdShip = Just newCmdId }) playerId
            $ uniPlayers uni
        }

    ATPurchaseFighters shipId amount -> case user of
      Player.GameAdmin -> uni
      Player.GamePlayer playerId ->
        let
          player    = uniPlayers uni Map.! playerId
          ship      = uniShips uni Map.! shipId
          newShip   = ship { shipFighters = shipFighters ship + amount }
          newPlayer = case shipStatus ship of
            SSPlanet pid ->
              let planet = uniPlanets uni Map.! pid
              in
                case planet of
                  PlanetHomeworld hwData -> player
                    { playerCash =
                      playerCash player
                        - (fromIntegral amount * homeworldFighterCost hwData)
                    }
                  _ -> player
            _ -> player
        in
          uni
            { uniPlayers = Map.adjust (const newPlayer) playerId
                             $ uniPlayers uni
            , uniShips   = Map.adjust (const newShip) shipId $ uniShips uni
            }


    ATMoveShip shid (StarNum toId) ->
      let
        ship             = uniShips uni Map.! shid
        StarNum fromId   = shipLocation ship
        fromStar         = uniStars uni ! fromId
        toStar           = uniStars uni ! toId
        shipOwnerId      = shipOwner ship
        shipTowingFilter = \s ->
          (  shipStatus s
          == SSTowedBy shid
          && shipLocation s
          == StarNum fromId
          && shipOwner s
          == shipOwnerId
          )
        movingShipIds =
          Map.keys
            $ Map.filter (\s -> (shipTowingFilter s) || shipId s == shid)
            $ uniShips uni
        newFromStar =
          fromStar { starShips = starShips fromStar \\ movingShipIds }
        newToStar =
          toStar { starShips = nub $ starShips toStar ++ movingShipIds }
        newShips =
          Map.map
              (\s -> if shipId s == shid
                then s { shipLocation = StarNum toId, shipStatus = SSSpaceIdle }
                else if shipTowingFilter s
                  then s { shipLocation = StarNum toId }
                  else s
              )
            $ uniShips uni
        player :: Player
        player    = uniPlayers uni Map.! shipOwnerId
        newPlayer = player
          { playerStarsVisited = Set.insert (StarNum toId)
                                   $ playerStarsVisited player
          }
      in
        if (shid `elem` starShips fromStar)
           && (StarNum toId `elem` starLinks fromStar)
        then
          uni
            { uniStars   = IMap.adjust (const newFromStar) fromId
                           $ IMap.adjust (const newToStar) toId
                           $ uniStars uni
            , uniShips   = newShips
            , uniPlayers = Map.adjust (const newPlayer) shipOwnerId
                             $ uniPlayers uni
            }
        else
          traceShow (shid `elem` starShips fromStar) $ uni
    ATShipStatus shid mode ->
      let newShips =
              Map.adjust (\s -> s { shipStatus = mode }) shid $ uniShips uni
      in  uni { uniShips = newShips }
    ATAttackShip cmdShipId targetShipId ->
      let newShips =
              Map.adjust
                  (\s -> s { shipStatus = SSSpaceAttacking targetShipId 1 })
                  cmdShipId
                $ Map.adjust
                    (\s -> case shipStatus s of
                      SSSpaceAttackedBy shipIds -> s
                        { shipStatus = SSSpaceAttackedBy
                                       $  nub
                                       $  shipIds
                                       ++ [cmdShipId]
                        }
                      _ -> s { shipStatus = SSSpaceAttackedBy [cmdShipId] }
                    )
                    targetShipId
                $ uniShips uni
      in  uni { uniShips = newShips }
    ATStopAttack cmdShipId ->
      let
        cmdShip          = uniShips uni Map.! cmdShipId
        targetShipAction = case shipStatus cmdShip of
          SSSpaceAttacking targetId _ ->
            Map.adjust
                (\targetShip -> case shipStatus targetShip of
                  SSSpaceAttackedBy [] ->
                    targetShip { shipStatus = SSSpaceIdle }
                  SSSpaceAttackedBy [attackingId] ->
                    if attackingId == cmdShipId
                      then targetShip { shipStatus = SSSpaceIdle }
                      else targetShip
                  SSSpaceAttackedBy manyShipIds -> targetShip
                    { shipStatus = SSSpaceAttackedBy
                                   $  manyShipIds
                                   \\ [cmdShipId]
                    }
                  _ -> targetShip
                )
                targetId
              $ uniShips uni

          _ -> uniShips uni
        newShips = Map.adjust (\s -> s { shipStatus = SSSpaceIdle })
                              cmdShipId
                              targetShipAction
      in
        uni { uniShips = newShips }
  else uni
 where
  affordsAction :: GameUser -> ActionType -> Bool
  affordsAction Player.GameAdmin (ATNewShip _ _) = True
  affordsAction _ (ATMoveShip shid _) =
    let ship = uniShips uni Map.! shid in (isShipInSpace . shipStatus) ship
  affordsAction _ (ATShipStatus shid SSSpaceMiningFuel) =
    let ship = uniShips uni Map.! shid
    in  (isShipInSpace . shipStatus) ship && not (isShipUnderAttack ship)
  affordsAction _ (ATShipStatus shid SSSpaceMiningMetal) =
    let ship = uniShips uni Map.! shid
    in  (isShipInSpace . shipStatus) ship && not (isShipUnderAttack ship)
  affordsAction _ (ATShipStatus shid (SSTowedBy towerId)) =
    let towedShip  = uniShips uni Map.! shid
        towingShip = uniShips uni Map.! towerId
    in  shipLocation towedShip
          == shipLocation towingShip
          && shipOwner towedShip
          == shipOwner towingShip
          && shid
          /= towerId

  affordsAction _ (ATShipStatus shid (SSPlanet pid)) =
    let ship = uniShips uni Map.! shid
        star = getShipStar ship
    in  pid `elem` starPlanets star
  affordsAction _ (ATShipStatus _ (SSSpaceAttacking _ _)) = False
  affordsAction _ (ATAttackShip cmdShipId targetShipId) =
    let cmdShip      = uniShips uni Map.! cmdShipId
        attackedShip = uniShips uni Map.! targetShipId
    in  (cargoFuel . shipCargo) cmdShip
          >  0
          && shipLocation cmdShip
          == shipLocation attackedShip
          && shipOwner cmdShip
          /= shipOwner attackedShip
          && shipFighters cmdShip
          >  0
          && not (isShipUnderAttack cmdShip)

  affordsAction _ (ATDockSellFuel shid) =
    let ship = uniShips uni Map.! shid
        star = getShipStar ship
    in  Maybe.isJust (starPort star) && (cargoFuel . shipCargo) ship > 0
  affordsAction _ (ATDockBuyFuel shid amount) =
    let ship = uniShips uni Map.! shid
        star = getShipStar ship
    in  Maybe.isJust (starPort star)
          && (portFuel . Maybe.fromJust . starPort) star
          >= amount
  affordsAction _ (ATDockSellMetal shid) =
    let ship = uniShips uni Map.! shid
        star = getShipStar ship
    in  Maybe.isJust (starPort star) && (cargoMetal . shipCargo) ship > 0
  affordsAction _ (ATDockBuyMetal shid amount) =
    let ship = uniShips uni Map.! shid
        star = getShipStar ship
    in  Maybe.isJust (starPort star)
          && (portFuel . Maybe.fromJust . starPort) star
          >= amount
  affordsAction (Player.GamePlayer playerId) (ATPurchaseShip _ shipType _) =
    let player = uniPlayers uni Map.! playerId
        checkPlanet pid =
            let planet = uniPlanets uni Map.! pid
            in  case planet of
                  PlanetHomeworld hwData ->
                    shipType `elem` homeworldShipsAvailable hwData
                  _ -> False
        onHomeworld
          | playerCmdShip player == Nothing
          = -- if player has no ship, we want to ensure 
                                                -- that shipType is in their homeworld.
            let homeworldStar = uniStars uni ! 1 -- magic num for sol.
                planetId      = head $ starPlanets homeworldStar
            in  checkPlanet planetId
          | otherwise
          = let ship = Maybe.fromJust $ playerCmdShip player >>= \x ->
                  Map.lookup x (uniShips uni)
            in  case shipStatus ship of
                  SSPlanet pid -> checkPlanet pid
                  _            -> False
    in  playerCash player > shipTPrice shipType && onHomeworld
  affordsAction (Player.GamePlayer playerId) (ATChangeCommand newCmdId) =
    let mShip = Map.lookup newCmdId (uniShips uni)
    in  case mShip of
          Just ship -> shipOwner ship == playerId
          Nothing   -> False
  affordsAction (Player.GamePlayer playerId) (ATPurchaseFighters shipId amount)
    = let player        = uniPlayers uni Map.! playerId
          ship          = uniShips uni Map.! shipId
          homeworldCost = case shipStatus ship of
            SSPlanet pid ->
              let planet = uniPlanets uni Map.! pid
              in  case planet of
                    PlanetHomeworld hwData ->
                      Just $ homeworldFighterCost hwData
                    _ -> Nothing
            _ -> Nothing
      in  case homeworldCost of
            Just cost ->
              (cost * fromIntegral amount)
                <= playerCash player
                && shipFighters ship
                +  amount
                <= shipMaxFighters ship
                && shipOwner ship
                == playerId
            Nothing -> False
  affordsAction _ _ = True

  getShipStar ship =
    let (StarNum starId) = shipLocation ship in uniStars uni ! starId

  getShipOwner ship = uniPlayers uni Map.! shipOwner ship

  getFuelSellPrice _ = (gameCfgFuelPrice . uniCfg) uni * 0.2
  getMetalSellPrice _ = (gameCfgMetalPrice . uniCfg) uni * 0.2
  getFuelBuyPrice _ = (gameCfgFuelPrice . uniCfg) uni
  getMetalBuyPrice _ = (gameCfgMetalPrice . uniCfg) uni

  roundDown x = (fromIntegral $ floor $ x * 100) / 100
