module Engine.Player
  ( GameUser(..)
  , Player(..)
  , ShortPlayer(..)
  , playerToShortPlayer
  )
where
import           Data.Aeson                     ( ToJSON
                                                , FromJSON
                                                )
import           Data.Set                       ( Set )
import           Data.Text                      ( Text )
import           GHC.Generics                   ( Generic )
import qualified Generics.SOP                  as SOP

import           Engine.Ids                     ( ShipNum(..)
                                                , PlayerNum(..)
                                                , StarNum(..)
                                                )


data Player = Player { playerScreenName :: Text
                     , playerId :: PlayerNum
                     , playerCmdShip :: Maybe ShipNum
                     , playerCash :: Double
                     , playerStarsVisited :: Set StarNum
                     }
  deriving (Eq, Generic, Read, Show, SOP.Generic, SOP.HasDatatypeInfo)
instance ToJSON Player

data ShortPlayer = ShortPlayer { shortPlayerName :: Text
                               , shortPlayerId :: PlayerNum
                               }
  deriving (Eq, Generic, Read,Show, SOP.Generic, SOP.HasDatatypeInfo)
instance ToJSON ShortPlayer
instance FromJSON ShortPlayer

playerToShortPlayer :: Player -> ShortPlayer
playerToShortPlayer p = ShortPlayer (playerScreenName p) (playerId p)


data GameUser = GameAdmin | GamePlayer PlayerNum
  deriving (Eq, Read, Show, Generic, SOP.Generic, SOP.HasDatatypeInfo)
instance ToJSON GameUser
instance FromJSON GameUser
