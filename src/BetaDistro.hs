module BetaDistro
  ( betaDistro
  , bernoulliDistro
  , normalDistro
  )
where
import qualified Control.Monad.Random          as CMR
import qualified Data.Random                   as DR
import           Data.Random.Distribution.Beta  ( beta )
import           Data.Random.Distribution.Bernoulli
                                                ( boolBernoulli )
import           Data.Random.Distribution.Normal
                                                ( normal )

import           Data.Word                      ( Word32 )

betaDistro :: forall m . CMR.MonadRandom m => Double -> Double -> m Double
betaDistro a b = DR.runRVar (beta a b) (CMR.getRandom :: m Word32)

bernoulliDistro :: forall m . CMR.MonadRandom m => Double -> m Bool
bernoulliDistro p = DR.runRVar (boolBernoulli p) (CMR.getRandom :: m Word32)

normalDistro :: forall m . CMR.MonadRandom m => Double -> Double -> m Double
normalDistro m s = DR.runRVar (normal m s) (CMR.getRandom :: m Word32)
