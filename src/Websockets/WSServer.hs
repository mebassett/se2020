{-# LANGUAGE ScopedTypeVariables #-}
module Websockets.WSServer
  ( WSClientMsg(..)
  , WSServerMsg(..)
  , wsServer
  )
where
import           Control.Concurrent.STM.TVar    ( TVar
                                                , readTVarIO
                                                )
import qualified Control.Concurrent.STM        as STM
import           Control.Concurrent            as CtrlCon
import           Control.Concurrent.Async       ( race_ )
import           Control.Monad                  ( forever
                                                , when
                                                )
import           Control.Monad.Except           ( runExceptT
                                                , liftIO
                                                )
import           Control.Monad.State            ( StateT
                                                , evalStateT
                                                , MonadState(..)
                                                , MonadIO(..)
                                                )
import qualified Crypto.JOSE                   as Jose
import qualified Crypto.JWT                    as Jose
import           Data.Aeson                     ( FromJSON(..)
                                                , ToJSON(..)
                                                , eitherDecode
                                                )
import           Data.ByteString                ( ByteString )
import qualified Data.ByteString.Lazy          as BSL
import qualified Data.Map.Strict               as Map
import qualified Data.HashMap.Strict           as HM
import qualified Data.IntMap.Strict            as IMap
import qualified Data.Maybe                    as Maybe
import           Data.Text                      ( Text
                                                , append
                                                , pack
                                                )
import           Data.Text.Encoding             ( encodeUtf8 )
import qualified Data.Time.Clock               as Time
import           Data.Tuple.Extra               ( second )
import           Data.UUID.V4                   ( nextRandom )
import qualified Generics.SOP                  as SOP
import           GHC.Generics                   ( Generic )
import qualified Network.WebSockets            as WS
import           Network.Wai.Handler.WebSockets ( websocketsOr )
import           Network.Wai                    ( Application )
import           Servant.Auth.Server            ( FromJWT(..)
                                                , JWTSettings
                                                )
import           Servant.Auth.Server.Internal.ConfigTypes
                                                ( jwtSettingsToJwtValidationSettings
                                                , validationKeys
                                                )

import           Engine
import           Notifications                  ( Notification
                                                , NotificationStatus(..)
                                                )
import           Servant.ServantApi             ( GameState(..)
                                                , queueAction
                                                )
import           User                           ( User(..)
                                                , Auth0User(..)
                                                )

data WSServerMsg = SetCmdShip (Maybe Ship)
           | SetCurrentStar (Maybe (Star, [ShortPlanet]))
           | SetStarShips [(ShortPlayer, ShortShip)]
           | SetPlayerShips [Ship]
           | SetPlayer Player
           | SetPlanet (Maybe Planet)
           | SendNotifications [Notification]
  deriving (Generic, Show, SOP.Generic, SOP.HasDatatypeInfo, Eq)
instance ToJSON WSServerMsg

data WSClientMsg = DockToPort
                 | UndockFromPort
                 | DockBuyFuel Double
                 | DockBuyMetal Double
                 | DockSellFuel
                 | DockSellMetal
                 | MineMetal
                 | MineFuel
                 | TowShip ShipNum
                 | StopTowShip ShipNum
                 | AttackShip ShipNum
                 | StopAttack
                 | LandOnPlanet PlanetNum
                 | PurchaseShip ShipTemplate Text
                 | ChangeCommand ShipNum
                 | PurchaseFighters Int
                 | AcceptNotifications [Notification]
                 | Login Text
  deriving (Generic, Show, SOP.Generic, SOP.HasDatatypeInfo, Eq)
instance FromJSON WSClientMsg

wsApp :: JWTSettings -> GameState -> WS.ServerApp
wsApp jwtSettings gameState pendingConn = do
  conn <- WS.acceptRequest pendingConn
  forever $ do
    msg <- WS.receiveData conn
    case eitherDecode $ BSL.fromStrict msg of
      Left err ->
        putStrLn $ "I don't understand the message: " ++ show msg ++ " " ++ err
      Right (Login jwtmsg) -> do
        eUser <- jwtAuthCheck jwtSettings $ encodeUtf8 jwtmsg
        case eUser of
          Left  err                      -> print ("WS Error: " `append` err)
          Right (auth0User :: Auth0User) -> do
            userList <- liftIO $ readTVarIO $ gsTUserList gameState
            case HM.lookup (sub auth0User) userList of
              Just user ->
                let playerNum = userPlayerNum user
                in  sendLoop conn playerNum gameState
                      `race_` receiveLoop conn playerNum gameState
              Nothing ->
                print ("WS Error: user not found" `append` (sub auth0User))

type WSClient = WS.Connection -> PlayerNum -> GameState -> IO ()

receiveLoop :: WSClient
receiveLoop conn playerNum gameState = forever $ do
  (rawJson :: ByteString) <- WS.receiveData conn
  let
    GameState { gsTUniverse = tUniverse, gsTActionQueue = tActionQueue, gsNotifications = tNotes }
      = gameState
  universe <- liftIO $ readTVarIO tUniverse
  let mPlayer  = Map.lookup playerNum $ uniPlayers universe
      mCmdShip = do
        player    <- mPlayer
        cmdShipId <- playerCmdShip player
        Map.lookup cmdShipId $ uniShips universe
      mStar = do
        cmdShip <- mCmdShip
        let (StarNum starId) = shipLocation cmdShip
        IMap.lookup starId $ uniStars universe

  case eitherDecode $ BSL.fromStrict rawJson of
    Left err ->
      putStrLn
        $  "I don't understand the message: "
        ++ show rawJson
        ++ " "
        ++ err
    Right (Login _) ->
      -- user is already logged in if we are down here, so just ignore this message.
      return ()
    Right DockToPort -> maybe
      (return ())
      (\ship ->
        let action = GA (GamePlayer playerNum) (ATDockShip (shipId ship))
        in  queueAction tActionQueue action
      )
      mCmdShip
    Right UndockFromPort -> maybe
      (return ())
      (\ship ->
        let action = GA (GamePlayer playerNum) (ATUndockShip (shipId ship))
        in  queueAction tActionQueue action
      )
      mCmdShip
    Right (DockBuyFuel amount) -> maybe
      (return ())
      (\ship ->
        let action =
                GA (GamePlayer playerNum) (ATDockBuyFuel (shipId ship) amount)
        in  queueAction tActionQueue action
      )
      mCmdShip
    Right (DockBuyMetal amount) -> maybe
      (return ())
      (\ship ->
        let action =
                GA (GamePlayer playerNum) (ATDockBuyMetal (shipId ship) amount)
        in  queueAction tActionQueue action
      )
      mCmdShip
    Right DockSellFuel -> maybe
      (return ())
      (\ship ->
        let action = GA (GamePlayer playerNum) (ATDockSellFuel (shipId ship))
        in  queueAction tActionQueue action
      )
      mCmdShip
    Right DockSellMetal -> maybe
      (return ())
      (\ship ->
        let action = GA (GamePlayer playerNum) (ATDockSellMetal (shipId ship))
        in  queueAction tActionQueue action
      )
      mCmdShip
    Right MineMetal -> maybe
      (return ())
      (\ship ->
        let action = GA (GamePlayer playerNum)
                        (ATShipStatus (shipId ship) SSSpaceMiningMetal)
        in  queueAction tActionQueue action
      )
      mCmdShip
    Right MineFuel -> maybe
      (return ())
      (\ship ->
        let action = GA (GamePlayer playerNum)
                        (ATShipStatus (shipId ship) SSSpaceMiningFuel)
        in  queueAction tActionQueue action
      )
      mCmdShip
    Right (TowShip towedShipId) -> maybe
      (return ())
      (\ship ->
        let action = GA (GamePlayer playerNum)
                        (ATShipStatus towedShipId (SSTowedBy (shipId ship)))
        in  queueAction tActionQueue action
      )
      mCmdShip
    Right (StopTowShip towedShipId) ->
      let action =
              GA (GamePlayer playerNum) (ATShipStatus towedShipId SSSpaceIdle)
      in  queueAction tActionQueue action
    Right (AttackShip targetId) -> maybe
      (return ())
      (\cmdShip ->
        let action = GA (GamePlayer playerNum)
                        (ATAttackShip (shipId cmdShip) targetId)
        in  queueAction tActionQueue action
      )
      mCmdShip
    Right StopAttack -> maybe
      (return ())
      (\cmdShip ->
        let action = GA (GamePlayer playerNum) (ATStopAttack (shipId cmdShip))
        in  queueAction tActionQueue action
      )
      mCmdShip

    Right (LandOnPlanet planetId) -> maybe
      (return ())
      (\ship ->
        let action = GA (GamePlayer playerNum)
                        (ATShipStatus (shipId ship) (SSPlanet planetId))
        in  queueAction tActionQueue action
      )
      mCmdShip
    Right (PurchaseShip shipTemplate newShipName) -> nextRandom >>= \uuid ->
      let action = GA (GamePlayer playerNum)
                      (ATPurchaseShip uuid shipTemplate newShipName)
      in  queueAction tActionQueue action
    Right (ChangeCommand newCmdShip) ->
      let action = GA (GamePlayer playerNum) (ATChangeCommand newCmdShip)
      in  queueAction tActionQueue action
    Right (PurchaseFighters amount) -> maybe
      (return ())
      (\ship ->
        let
          action =
            GA (GamePlayer playerNum) (ATPurchaseFighters (shipId ship) amount)
        in  queueAction tActionQueue action
      )
      mCmdShip
    Right (AcceptNotifications recievedNotes) -> do
      theTime <- Time.getCurrentTime
      STM.atomically $ STM.modifyTVar' tNotes $ Map.adjust
        (map
          (\(noteStatus, gameMsg) -> if gameMsg `elem` map snd recievedNotes
            then (NSRecievedAt theTime, gameMsg)
            else (noteStatus, gameMsg)
          )
        )
        playerNum




prepareForWS :: ToJSON a => a -> Text
prepareForWS = pack . show . toEncoding

data WSClientState = WSClientState
  { wscsCmdShip :: Maybe Ship
  , wscsCurrentStar :: Maybe (Star, [ShortPlanet])
  , wscsCurrentPlanet :: Maybe Planet
  , wscsPlayerShips :: [Ship]
  , wscsSystemShips :: [(ShortPlayer, ShortShip)]
  , wscsPlayer :: Player
  , wscsNotifications :: [Notification]
  }

shipIdToShortPlayerShip :: Universe -> ShipNum -> Maybe (ShortPlayer, Ship)
shipIdToShortPlayerShip uni shipId =
  let mShip   = Map.lookup shipId (uniShips uni)
      mPlayer = do
        ship <- mShip
        if (isShipInSpace . shipStatus) ship
          then do
            let playerId = shipOwner ship
            player <- Map.lookup playerId (uniPlayers uni)
            return $ ShortPlayer { shortPlayerId   = (shipOwner ship)
                                 , shortPlayerName = (playerScreenName player)
                                 }
          else Nothing
  in  case (mPlayer, mShip) of
        (Just p, Just s) -> Just (p, s)
        (_     , _     ) -> Nothing

planetNumToShortPlanet :: Universe -> PlanetNum -> Maybe ShortPlanet
planetNumToShortPlanet uni planetId = do
  planet <- Map.lookup planetId (uniPlanets uni)
  case planet of
    PlanetHomeworld hData -> return ShortPlanet
      { shortPlanetId   = planetId
      , shortPlanetName = (homeworldName hData)
      }
    Planet pData -> return ShortPlanet { shortPlanetId   = planetId
                                       , shortPlanetName = (planetName pData)
                                       }

getInitialWSClient :: MonadIO m => PlayerNum -> TVar Universe -> m WSClientState
getInitialWSClient playerNum tUniverse = do
  universe <- liftIO $ readTVarIO tUniverse
  let initPlayer = Maybe.fromJust $ Map.lookup playerNum $ uniPlayers universe
  return $ WSClientState Nothing Nothing Nothing [] [] initPlayer []

fiveMinutes :: Time.NominalDiffTime
fiveMinutes = 5 * 60

noteFilter :: Time.UTCTime -> Notification -> Bool
noteFilter currentTime (NSPendingSince _, _) = True
noteFilter currentTime (NSSentOn t, _) =
  Time.diffUTCTime currentTime t < fiveMinutes
noteFilter _ (NSRecievedAt _, _) = False

sendLoop :: WSClient
sendLoop conn playerNum gameState =
  let
    GameState { gsTUniverse = tUniverse, gsNotifications = tNotes } = gameState


    executeWSSendLoop :: StateT WSClientState IO ()
    executeWSSendLoop = forever $ do
      currentState@WSClientState {..} <- get
      universe                        <- liftIO $ readTVarIO tUniverse
      notifications                   <- liftIO $ readTVarIO tNotes
      currentTime                     <- liftIO $ Time.getCurrentTime
      let
        relevantNotifications = Maybe.fromMaybe [] $ do
          playerNotes <- Map.lookup playerNum notifications
          return $ filter (noteFilter currentTime) playerNotes
        newPlayer = Maybe.fromJust $ Map.lookup playerNum $ uniPlayers universe
        mNewShip  = do
          cmdShipId <- playerCmdShip newPlayer
          Map.lookup cmdShipId $ uniShips universe
        mNewStar = do
          ship <- mNewShip
          let (StarNum playerLoc) = shipLocation ship
          star <- IMap.lookup playerLoc $ uniStars universe
          let
            planets =
              Maybe.catMaybes
                $ map (planetNumToShortPlanet universe)
                $ starPlanets star
          return (star, planets)
        allShortPlayerShips = case mNewStar of
          Just (star, _) ->
            Maybe.catMaybes
              $   shipIdToShortPlayerShip universe
              <$> starShips star
          Nothing -> []
        playerShips =
          snd
            <$> filter (\(p, _) -> shortPlayerId p == playerNum)
                       allShortPlayerShips
        systemShips =
          (second shipToShortShip)
            <$> filter (\(p, _) -> shortPlayerId p /= playerNum)
                       allShortPlayerShips
        mNewPlanet = case mNewShip of
          Nothing -> -- player is dead, return to homeworld
            let homeworldStar = uniStars universe IMap.! 1
                homeworldId   = head $ starPlanets homeworldStar
            in  homeworldId `Map.lookup` uniPlanets universe
          Just ship -> case shipStatus ship of
            SSPlanet pid -> pid `Map.lookup` uniPlanets universe
            _            -> Nothing

      when (map snd relevantNotifications /= map snd wscsNotifications)
        $ liftIO
        $ do
            liftIO $ WS.sendTextData conn $ prepareForWS $ SendNotifications
              relevantNotifications
            STM.atomically $ STM.modifyTVar' tNotes $ Map.adjust
              (map
                (\(noteStatus, gameMsg) ->
                  if gameMsg `elem` map snd relevantNotifications
                    then (NSSentOn currentTime, gameMsg)
                    else (noteStatus, gameMsg)
                )
              )
              playerNum


      when (newPlayer /= wscsPlayer)
        $ liftIO
        $ WS.sendTextData conn
        $ prepareForWS
        $ SetPlayer newPlayer

      when (mNewShip /= wscsCmdShip)
        $ liftIO
        $ WS.sendTextData conn
        $ prepareForWS
        $ SetCmdShip mNewShip

      when (mNewStar /= wscsCurrentStar)
        $ liftIO
        $ WS.sendTextData conn
        $ prepareForWS
        $ SetCurrentStar mNewStar

      when (systemShips /= wscsSystemShips)
        $ liftIO
        $ WS.sendTextData conn
        $ prepareForWS
        $ SetStarShips systemShips

      when (playerShips /= wscsPlayerShips)
        $ liftIO
        $ WS.sendTextData conn
        $ prepareForWS
        $ SetPlayerShips playerShips

      when (mNewPlanet /= wscsCurrentPlanet)
        $ liftIO
        $ WS.sendTextData conn
        $ prepareForWS
        $ SetPlanet mNewPlanet

      put $ WSClientState { wscsCmdShip       = mNewShip
                          , wscsCurrentStar   = mNewStar
                          , wscsSystemShips   = systemShips
                          , wscsPlayerShips   = playerShips
                          , wscsCurrentPlanet = mNewPlanet
                          , wscsPlayer        = newPlayer
                          , wscsNotifications = relevantNotifications
                          }
      liftIO $ CtrlCon.threadDelay 500
      return ()
  in
    getInitialWSClient playerNum tUniverse
      >>= \initState -> evalStateT executeWSSendLoop initState


jwtAuthCheck :: FromJWT val => JWTSettings -> ByteString -> IO (Either Text val)
jwtAuthCheck config bs = do
  verifiedJwt <- liftIO $ runExceptT $ do

    unverifiedJWT <- Jose.decodeCompact $ BSL.fromStrict bs
    Jose.verifyClaims (jwtSettingsToJwtValidationSettings config)
                      (validationKeys config)
                      unverifiedJWT
  case verifiedJwt of
    Left  (err :: Jose.JWTError) -> return $ Left $ pack $ show err
    Right v                      -> case decodeJWT v of
      Left  err -> return $ Left err
      Right v'  -> return $ Right v'


wsServer :: JWTSettings -> GameState -> Application -> Application
wsServer jwtSettings gameState =
  websocketsOr WS.defaultConnectionOptions (wsApp jwtSettings gameState)
