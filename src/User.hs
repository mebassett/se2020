module User
  ( Auth0User(..)
  , LoginInfo(..)
  , SignupInfo(..)
  , User(..)
  , UserList
  , getUserList
  )
where
import           Control.Lens                   ( (^.) )
import           Control.Concurrent.STM        as STM
import qualified Crypto.JWT                    as Jose
import           Data.Aeson                     ( FromJSON
                                                , ToJSON
                                                , toJSON
                                                )
import qualified Data.HashMap.Strict           as HM
import           Data.Text                      ( Text )
import qualified Generics.SOP                  as SOP
import           GHC.Generics                   ( Generic )
import           Servant.Auth.Server            ( FromJWT(..)
                                                , ToJWT(..)
                                                )

import           Engine.Ids                     ( PlayerNum )

data LoginInfo = LoginInfo { loginInfoEmail :: Text
                           , loginInfoPassword :: Text}
  deriving (Eq, Show, Read, Generic, SOP.Generic, SOP.HasDatatypeInfo)
instance ToJSON LoginInfo
instance ToJWT LoginInfo
instance FromJSON LoginInfo
instance FromJWT LoginInfo

data Auth0User = Auth0User { sub :: Text}
  deriving (Eq, Show, Read, Generic)
instance FromJSON Auth0User
instance ToJSON Auth0User
instance FromJWT Auth0User where
  decodeJWT m = case (m ^. Jose.claimSub) of
    Nothing -> Left "Missing `sub` claim"
    Just v  -> Right $ Auth0User $ (v ^. Jose.string)
instance ToJWT Auth0User where
  encodeJWT a = Jose.addClaim "sub" (toJSON $ sub a) Jose.emptyClaimsSet


data User = User { userName :: Text
                 , userPlayerNum :: PlayerNum }
  deriving (Eq, Show, Read, Generic, SOP.Generic, SOP.HasDatatypeInfo)
instance ToJSON User
instance ToJWT User
instance FromJSON User
instance FromJWT User

data SignupInfo = SignupInfo { signupInfoName :: Text
                 }
  deriving (Eq, Show, Read, Generic, SOP.Generic, SOP.HasDatatypeInfo)
instance ToJSON SignupInfo
instance ToJWT SignupInfo
instance FromJSON SignupInfo
instance FromJWT SignupInfo

type UserList = HM.HashMap Text User
getUserList :: IO (STM.TVar UserList)
getUserList = STM.newTVarIO HM.empty
