module Engine
  ( module Engine.Action
  , module Engine.GameTick
  , module Engine.Ids
  , module Engine.Player
  , module Engine.Ship
  , module Engine.Star
  , resumeGame
  , startGame
  )
where

import           Engine.Action
import           Engine.GameTick
import           Engine.Ids
import           Engine.Player
import           Engine.Ship
import           Engine.Star
import           Notifications                  ( Notifications
                                                , msgsToNotifications
                                                , unionNotes
                                                )
import qualified Control.Concurrent.STM        as STM
import qualified Control.Concurrent            as CtrlCon
import qualified Control.Monad                 as Ctrl
import           Control.Monad.State.Strict     ( StateT
                                                , MonadState(..)
                                                , execStateT
                                                )
import           Control.Monad.IO.Class         ( MonadIO(..) )
import           Control.Monad.Random           ( runRand )
import           Data.Map.Strict                ( empty )
import           Data.Time.Clock                ( getCurrentTime )
import           System.Random                  ( RandomGen
                                                , getStdGen
                                                )

runGame
  :: RandomGen g
  => STM.TVar Notifications
  -> STM.TVar [GameAction]
  -> STM.TVar Universe
  -> StateT g IO ()
runGame tNotifications tActionQueue tUniverse = do
  gen               <- get
  theTime           <- liftIO getCurrentTime
  (newMsgs, newGen) <- liftIO $ STM.atomically $ do
    actionQueue  <- STM.readTVar tActionQueue
    initUniverse <- STM.readTVar tUniverse
    let ((newUni, newMsgs), newGen) =
          runRand (gameTick actionQueue initUniverse) gen
    STM.writeTVar tUniverse newUni
    STM.writeTVar tActionQueue []
    return (newMsgs, newGen)
  put newGen
  liftIO
    $ STM.atomically
    $ STM.modifyTVar' tNotifications
    $ unionNotes
    $ msgsToNotifications theTime newMsgs


resumeGame
  :: Int
  -> STM.TVar Notifications
  -> STM.TVar [GameAction]
  -> STM.TVar Universe
  -> IO ()
resumeGame gameSpeed tNotes tActionQueue tUniverse =
  Ctrl.void $ CtrlCon.forkIO $ do
    gen <- getStdGen
    Ctrl.void $ loop gen
 where
  loop :: RandomGen g => g -> IO ()
  loop g = do
    newG <- execStateT (runGame tNotes tActionQueue tUniverse) g
    CtrlCon.threadDelay gameSpeed
    loop newG

startGame
  :: Universe
  -> Int
  -> IO (STM.TVar Notifications, STM.TVar [GameAction], STM.TVar Universe)
startGame initUniverse gameSpeed = do
  tActionQueue   <- STM.newTVarIO []
  tUniverse      <- STM.newTVarIO initUniverse
  tNotifications <- STM.newTVarIO empty
  resumeGame gameSpeed tNotifications tActionQueue tUniverse
  return (tNotifications, tActionQueue, tUniverse)
