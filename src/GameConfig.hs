module GameConfig
  ( GameConfig(..)
  , GalaxyConfiguration(..)
  , defaultGameCfg
  , ClusterPoint
  , clusters
  )
where

type ClusterPoint = (Double, Double, Double, Double)

data GalaxyConfiguration
 = GalaxyClusters [ClusterPoint]
 | GalaxySpiral { spiralMinRadius :: Double
                , spiralMaxRadius :: Double
                , spiralThickness :: Double
                , spiralNumArms :: Int
                , spiralScale :: Double }
  deriving (Read, Show, Eq)


data GameConfig =
  GameConfig { gameCfgNumStars :: Int
             , gameCfgMinLinks :: Int
             , gameCfgMaxLinks :: Int
             , gameCfgMaxDistance :: Double
             , gameCfgGalaxy :: GalaxyConfiguration
             , gameCfgPortProb :: Double
             , gameCfgNormalParamsMetal :: (Double, Double)
             , gameCfgNormalParamsFuel :: (Double, Double)
             , gameCfgFuelPrice :: Double
             , gameCfgElecPrice :: Double
             , gameCfgOrganPrice :: Double
             , gameCfgMetalPrice :: Double
             , gameCfgRateFuelForAttack :: Double -- amount of fuel used in one game tick for one fighter to attack.
             }
  deriving (Read, Show, Eq)

clusters :: GalaxyConfiguration
clusters = GalaxyClusters [(0, 0, 0, 5), (0, 25, 0, 25)]

spiral :: GalaxyConfiguration
spiral = GalaxySpiral { spiralMinRadius = 0.05
                      , spiralMaxRadius = 1.10
                      , spiralThickness = 0.1
                      , spiralNumArms   = 2
                      , spiralScale     = 400
                      }

defaultGameCfg :: GameConfig
defaultGameCfg = GameConfig { gameCfgNumStars          = 250
                            , gameCfgMinLinks          = 1
                            , gameCfgMaxLinks          = 5
                            , gameCfgMaxDistance       = 110
                            , gameCfgGalaxy            = spiral
                            , gameCfgPortProb          = 0.18
                            , gameCfgNormalParamsMetal = (5500, 2500)
                            , gameCfgNormalParamsFuel  = (7500, 4500)
                            , gameCfgFuelPrice         = 180.0
                            , gameCfgMetalPrice        = 250.0
                            , gameCfgOrganPrice        = 57.0
                            , gameCfgElecPrice         = 475.0
                            , gameCfgRateFuelForAttack = 0.002
                            }
