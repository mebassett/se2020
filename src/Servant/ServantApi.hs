module Servant.ServantApi
  ( AppM
  , GameState(..)
  , SEApi
  , TotalApi
  , User(..)
  , seServer
  , queueAction
  )
where

import           Data.ByteString.Lazy.Char8     ( unpack )
import qualified Data.Map.Strict               as Map
import           Data.Maybe                     ( catMaybes
                                                , fromMaybe
                                                )
import qualified Data.IntMap.Strict            as IMap
import qualified Data.HashMap.Strict           as HM
import qualified Data.Set                      as Set
import           Data.Text                      ( Text
                                                , pack
                                                )
import           Data.UUID.V4                   ( nextRandom )
import           Control.Concurrent.STM.TVar    ( TVar
                                                , modifyTVar
                                                , readTVar
                                                , readTVarIO
                                                , writeTVar
                                                )
import           Control.Monad.IO.Class         ( liftIO )
import           Control.Monad.STM              ( atomically )
import           Control.Monad.Reader.Class     ( MonadReader )
import           Control.Monad.Trans.Reader     ( ReaderT
                                                , ask
                                                )
import           GHC.Generics                   ( Generic )
import           Servant                        ( (:>)
                                                , (:<|>)(..)
                                                , Capture
                                                , Get
                                                , Handler
                                                , JSON
                                                , NoContent(..)
                                                , PatchNoContent
                                                , Post
                                                , Raw
                                                , ReqBody
                                                , ServerT
                                                , err401
                                                , err500
                                                , serveDirectoryWebApp
                                                , throwError
                                                )
import           Servant.Auth.Server            ( Auth
                                                , AuthResult(..)
                                                , JWTSettings
                                                , makeJWT
                                                , throwAll
                                                )

import           Engine                         ( ActionType(..)
                                                , GameAction(..)
                                                , GameUser(..)
                                                , Player(..)
                                                , PlayerNum(..)
                                                , Ship(..)
                                                , Star(..)
                                                , DisplayableStar(..)
                                                , StarNum(..)
                                                , Universe(..)
                                                , basicShip
                                                , shipFromTemplate
                                                )
import           Notifications                  ( Notifications )
import           User                           ( LoginInfo(..)
                                                , SignupInfo(..)
                                                , User(..)
                                                , UserList
                                                , Auth0User(..)
                                                )

data GameState =
  GameState { gsTActionQueue :: TVar [GameAction]
            , gsNotifications :: TVar Notifications
            , gsTUniverse :: TVar Universe
            , gsTUserList :: TVar UserList}

type AppM = ReaderT GameState Handler

class (MonadReader GameState m) => HasGameUniverse m where
  withUser :: (User -> m a) -> Auth0User -> m a

instance HasGameUniverse AppM where
  withUser func auth0User = do
    GameState { gsTUserList = tUserList } <- ask
    userList                              <- liftIO $ readTVarIO tUserList
    case HM.lookup (sub auth0User) userList of
      Nothing   -> throwError err401
      Just user -> func user



-- brittany-disable-next-binding
type SEGameApi = "register" :> ReqBody '[JSON] SignupInfo :> Post '[JSON] Player
             :<|> "player" :> Get '[JSON] Player
             :<|> "getCmdShip" :> Get '[JSON] (Maybe Ship)
             :<|> "getCurrentStar" :> Get '[JSON] Star
             :<|> "moveShip" :> ReqBody '[JSON] StarNum :> PatchNoContent '[JSON] NoContent
             :<|> "getStars" :> Get '[JSON] [DisplayableStar]

seGameServer :: AuthResult Auth0User -> ServerT SEGameApi AppM
seGameServer (Authenticated auth0User) =
  registerUser auth0User
    :<|> getPlayer auth0User
    :<|> getCmdShip auth0User
    :<|> getCurrentStar auth0User
    :<|> moveShip auth0User
    :<|> getStars auth0User
seGameServer _ = throwAll err401

getStars :: Auth0User -> AppM [DisplayableStar]
getStars = withUser $ \user -> do
  GameState { gsTUniverse = tUniverse } <- ask
  universe                              <- liftIO $ readTVarIO tUniverse
  let stars     = uniStars universe
      playerNum = userPlayerNum user
      mPlayer   = Map.lookup (userPlayerNum user) $ uniPlayers universe
  case mPlayer of
    Nothing -> return []
    Just player ->
      return $ map (starToDisplayableStar stars player) (IMap.elems stars)
 where
  starToDisplayableStar :: IMap.IntMap Star -> Player -> Star -> DisplayableStar
  starToDisplayableStar stars Player {..} Star {..} = DisplayableStar
    { displayStarName  = censor $ starName
    , displayStarId    = censor $ starId
    , displayStarXLoc  = starXLoc
    , displayStarYLoc  = starYLoc
    , displayStarZLoc  = starZLoc
    , displayStarSize  = starSize
    , displayStarLinks =
      fromMaybe [] $ censor $ catMaybes $ map toLoc $ Set.toList starLinks
    }
   where
    hasSeenStar = Set.member starId playerStarsVisited
    censor value = if hasSeenStar then Just value else Nothing
    toLoc :: StarNum -> Maybe (Double, (Double, Double))
    toLoc (StarNum starNum) = do
      Star {..} <- IMap.lookup starNum stars
      if Set.member starId playerStarsVisited
        then return (starXLoc, (starYLoc, starZLoc))
        else Nothing

registerUser :: Auth0User -> SignupInfo -> AppM Player
registerUser auth0User signupInfo = do
  GameState { gsTUserList = tUserList, gsTActionQueue = tActionQueue } <- ask
  userList <- liftIO $ readTVarIO tUserList
  if HM.member (sub auth0User) userList
    then throwError err500
    else do
      (_, player, ship) <- liftIO
        $ registerPlayer tUserList auth0User signupInfo
      liftIO $ queueAction tActionQueue $ GA GameAdmin $ ATIntroducePlayer
        player
      liftIO $ queueAction tActionQueue $ GA GameAdmin $ ATNewShip (StarNum 1)
                                                                   ship
      return player

registerPlayer
  :: TVar UserList -> Auth0User -> SignupInfo -> IO (User, Player, Ship)
registerPlayer tUserList auth0User signupInfo = do
  newPlayerNumRaw <- nextRandom
  newShipNum      <- nextRandom
  let
    playerNum = PlayerNum newPlayerNumRaw
    user =
      User { userName = signupInfoName signupInfo, userPlayerNum = playerNum }
    player =
      Player (userName user) playerNum Nothing 0 (Set.singleton $ StarNum 1)
    newShip =
      shipFromTemplate playerNum newShipNum (StarNum 1) "newship" basicShip
  atomically $ do
    userList <- readTVar tUserList
    writeTVar tUserList $ HM.insert (sub auth0User) user userList
  return (user, player, newShip)

getPlayer :: Auth0User -> AppM Player
getPlayer = withUser $ \user -> do
  GameState { gsTUniverse = tUniverse } <- ask
  universe                              <- liftIO $ readTVarIO tUniverse
  case Map.lookup (userPlayerNum user) $ uniPlayers universe of
    Just player -> return player
    Nothing     -> throwError err500

getCmdShip :: Auth0User -> AppM (Maybe Ship)
getCmdShip = withUser $ \user -> do
  GameState { gsTUniverse = tUniverse } <- ask
  universe                              <- liftIO $ readTVarIO tUniverse
  let mCmdShip = do
        player    <- Map.lookup (userPlayerNum user) $ uniPlayers universe
        cmdShipId <- playerCmdShip player
        Map.lookup cmdShipId $ uniShips universe
  return mCmdShip

getCurrentStar :: Auth0User -> AppM Star
getCurrentStar = withUser $ \user -> do
  GameState { gsTUniverse = tUniverse } <- ask
  universe                              <- liftIO $ readTVarIO tUniverse
  let mStar = do
        player    <- Map.lookup (userPlayerNum user) (uniPlayers universe)
        cmdShipId <- playerCmdShip player
        cmdShip   <- Map.lookup cmdShipId $ uniShips universe
        let (StarNum playerLoc) = shipLocation cmdShip
        IMap.lookup playerLoc $ uniStars universe
  case mStar of
    Nothing   -> throwError err500
    Just star -> return star

moveShip :: Auth0User -> StarNum -> AppM NoContent
moveShip auth0User starNum = flip withUser auth0User $ \user -> do
  GameState { gsTActionQueue = tActionQueue, gsTUniverse = tUniverse } <- ask
  universe <- liftIO $ readTVarIO tUniverse
  let playerNum = userPlayerNum user
      mAction   = do
        player  <- Map.lookup playerNum $ uniPlayers universe
        shipNum <- playerCmdShip player
        return $ GA (GamePlayer playerNum) (ATMoveShip shipNum starNum)
  case mAction of
    Nothing     -> throwError err500
    Just action -> do
      liftIO $ queueAction tActionQueue action
      return NoContent


-- brittany-disable-next-binding
--type SEApi auths = "public" :> SEPublicApi :<|> "game" :> (Auth auths Auth0User :> SEGameApi)
type SEApi auths = "game" :> (Auth auths Auth0User :> SEGameApi)

type TotalApi auths = SEApi auths :<|> Raw

seServer :: ServerT (TotalApi auths) AppM
seServer = seGameServer :<|> serveDirectoryWebApp "www"


queueAction :: TVar [GameAction] -> GameAction -> IO ()
queueAction tActionQueue gameAction =
  atomically $ modifyTVar tActionQueue (\q -> q ++ [gameAction])
