module Notifications
  ( GameMsg(..)
  , MsgType(..)
  , Notification
  , Notifications
  , NotificationStatus(..)
  , msgsToNotifications
  , unionNotes
  )
where

import           Engine.Ids
import           Engine.Player                 as Player
import           Engine.Ship                    ( ShortShip(..) )

import           Data.Aeson                     ( FromJSON(..)
                                                , ToJSON(..)
                                                )
import           Data.List                      ( nub )
import           Data.Map.Strict               as Map
import           Data.Time.Clock                ( UTCTime )
import qualified Generics.SOP                  as SOP
import           GHC.Generics                   ( Generic )


data MsgType = MTShipCargoFull ShortShip
             | MTAttackCanceledShipMoved ShortShip ShortShip
             | MTAttackCanceledNoFuel ShortShip ShortShip
             | MTAttackCanceledNoFigs ShortShip ShortShip
             | MTAttackCanceledShipDestroyed ShortShip
             | MTUnderAttack ShortShip Player.ShortPlayer
             | MTShipDestroyed ShortShip Player.ShortPlayer
 deriving (Eq, Read, Show, Generic, SOP.Generic, SOP.HasDatatypeInfo)
instance ToJSON MsgType
instance FromJSON MsgType

data GameMsg = GameMsg { gameMsgSender :: Player.GameUser
                       , gameMsgRecipient :: PlayerNum
                       , gameMsgDatum :: MsgType }
  deriving (Eq, Read, Show, Generic, SOP.Generic, SOP.HasDatatypeInfo)
instance ToJSON GameMsg
instance FromJSON GameMsg

data NotificationStatus = NSPendingSince UTCTime
                        | NSSentOn UTCTime
                        | NSRecievedAt UTCTime
  deriving (Eq, Read, Show, Generic, SOP.Generic, SOP.HasDatatypeInfo)
instance ToJSON NotificationStatus
instance FromJSON NotificationStatus

type Notification = (NotificationStatus, GameMsg)

type Notifications = Map PlayerNum [Notification]

msgsToNotifications :: UTCTime -> [GameMsg] -> Notifications
msgsToNotifications t = Prelude.foldl
  (\theMap theMsg ->
    unionNotes theMap $ Map.singleton (gameMsgRecipient theMsg) $ toNotification
      theMsg
  )
  Map.empty
  where toNotification msg = return (NSPendingSince t, msg)


unionNotes
  :: Map PlayerNum [Notification]
  -> Map PlayerNum [Notification]
  -> Map PlayerNum [Notification]
unionNotes = Map.unionWith combineNotifications
 where
  combineNotifications :: [Notification] -> [Notification] -> [Notification]
  combineNotifications a b = nub $ a ++ b
