module GameGen
  ( generateGame
  , generateStar
  , linkStars
  , GameMode(..)
  )
where
import           Engine
import           GameConfig
import qualified System.Random                 as Rand
import qualified System.Random.MWC             as MWC
import qualified Control.Monad                 as CtrlM
import           Control.Monad.Random           ( Rand
                                                , evalRandIO
                                                )
import           Control.Monad.State.Lazy       ( StateT
                                                , execStateT
                                                , modify
                                                , MonadState(..)
                                                , MonadTrans(..)
                                                )
import qualified Data.IntMap.Strict            as IMap
import qualified Data.Map.Strict               as Map
import           Data.Set                       ( fromList
                                                , insert
                                                , union
                                                , size
                                                )
import qualified Data.Random                   as RandFu
import           Data.Random.Shuffle.Weighted   ( weightedSample )
import           Data.UUID                      ( UUID )
import           Data.UUID.V4                   ( nextRandom )


class Monad m => GameMode m g | m -> g  where
  randomGen :: (Rand.Random a) => m a
  fromMonadRand :: Rand g a -> m a
  randomSample :: RandFu.RVar a -> m a
  getUUID :: m UUID

instance GameMode IO Rand.StdGen where
  randomGen     = Rand.randomIO
  fromMonadRand = evalRandIO
  randomSample sample = do
    mwc <- MWC.createSystemRandom
    RandFu.sampleFrom mwc sample
  getUUID = nextRandom

generateStar
  :: (GameMode m g, Rand.RandomGen g)
  => GameConfig
  -> UUID
  -> Int
  -> m (Int, Engine.Star)
generateStar cfg earthUuid sId = do
  newStar <- fromMonadRand
    $ if sId == 1 then Engine.mkSol cfg earthUuid else mkStar cfg
  return (sId, newStar { starId = StarNum sId })

generateLinks
  :: (GameMode m g)
  => GameConfig
  -> [(Int, Engine.Star)]
  -> m (IMap.IntMap Engine.Star)
generateLinks cfg originalStars =
  randomSample $ execStateT (linkStars cfg) (IMap.fromList originalStars)

linkStars :: GameConfig -> StateT (IMap.IntMap Engine.Star) RandFu.RVar ()
linkStars cfg = get >>= \s -> CtrlM.forM_ (IMap.keys s) $ \starNum -> do
  starMap <- get
  let star              = starMap IMap.! starNum
      numPotentialLinks = gameCfgMaxLinks cfg - size (starLinks star)
      starsWeightedByDistance =
        map
            (\otherStar ->
              let distance = 1 / Engine.starDistance star otherStar
              in  (distance, starId otherStar)
            )
          $ filter
              (\st ->
                starId st
                  /= StarNum starNum
                  && starDistance st star
                  <  gameCfgMaxDistance cfg
                  && size (starLinks st)
                  <  gameCfgMaxLinks cfg
              )
              (IMap.elems starMap)
  if numPotentialLinks < gameCfgMinLinks cfg
    then return ()
    else do
      numLinks <- lift
        $ RandFu.randomElement [gameCfgMinLinks cfg .. numPotentialLinks]
      linkedStars <- lift $ weightedSample
        numLinks
        (starsWeightedByDistance :: [(Double, StarNum)])
      modify $ \sm -> IMap.adjust
        ( const
        $ star { starLinks = starLinks star `union` fromList linkedStars }
        )
        starNum
        sm
      CtrlM.forM_ linkedStars $ \linkedStarNum ->
        let StarNum lsn = linkedStarNum
        in  modify $ \sm -> IMap.adjust
              (\st -> st { starLinks = StarNum starNum `insert` starLinks st })
              lsn
              sm

generateGame
  :: (GameMode m g, Rand.RandomGen g) => GameConfig -> m Engine.Universe
generateGame cfg = do
  earthId <- getUUID
  let earth = solEarth earthId
  stars  <- mapM (generateStar cfg earthId) [1 .. (gameCfgNumStars cfg)]
  stars' <- generateLinks cfg stars
  return Engine.Universe
    { uniStars   = stars'
    , uniShips   = Map.empty
    , uniPlayers = Map.empty
    , uniPlanets = Map.fromList [(PlanetNum earthId, earth)]
    , uniCfg     = cfg
    }
