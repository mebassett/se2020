module Persistence
  ( loadServer
  , saveServer
  )
where
import           Control.Concurrent.STM         ( atomically )
import           Control.Concurrent.STM.TVar    ( readTVar
                                                , newTVarIO
                                                , modifyTVar'
                                                )
import           Data.Map.Strict                ( empty )
import qualified Data.Map.Strict               as Map
import qualified Data.Time.Clock               as Time
import           Servant.ServantApi             ( GameState(..) )
import           System.Directory               ( doesFileExist )


import           Notifications                  ( Notification
                                                , NotificationStatus(..)
                                                )

fifteenMinutes :: Time.NominalDiffTime
fifteenMinutes = 15 * 60

expiredNote :: Time.UTCTime -> Notification -> Bool
expiredNote currentTime (NSPendingSince t, _) = True -- for now we will leave these until the user logs on.
expiredNote currentTime (NSSentOn t, _) =
  Time.diffUTCTime currentTime t < fifteenMinutes
expiredNote _ (NSRecievedAt _, _) = False

saveServer :: GameState -> IO ()
saveServer gameState = do
  currentTime                   <- Time.getCurrentTime
  (universe, userList, actions) <- atomically $ do
    universe <- readTVar $ gsTUniverse gameState
    userList <- readTVar $ gsTUserList gameState
    actions  <- readTVar $ gsTActionQueue gameState
    modifyTVar' (gsNotifications gameState) $ Map.map $ filter $ expiredNote
      currentTime

    return (universe, userList, actions)
  writeFile "./game-universe.dat" $ show universe
  writeFile "./game-userList.dat" $ show userList
  writeFile "./game-actionqueue.dat" $ show actions
  print "PERSISTENCE: wrote game data to files."

loadServer :: IO (Maybe GameState)
loadServer = do
  filesExist <- traverse
    doesFileExist
    ["./game-universe.dat", "./game-userList.dat", "./game-actionqueue.dat"]
  print filesExist
  if and filesExist
    then do
      rawUniverse    <- readFile "./game-universe.dat"
      rawUserList    <- readFile "./game-userList.dat"
      rawActions     <- readFile "./game-actionqueue.dat"
      tUniverse      <- newTVarIO $ read rawUniverse
      tUserList      <- newTVarIO $ read rawUserList
      tActions       <- newTVarIO $ read rawActions
      tNotifications <- newTVarIO empty
      return $ Just $ GameState tActions tNotifications tUniverse tUserList
    else return Nothing
