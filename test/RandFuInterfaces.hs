module RandFuInterfaces where

import qualified Data.Random                   as RandFu
import           Data.Random.Source
import qualified System.Random                 as Rand
import           Test.QuickCheck.Gen            ( Gen(..) ) 
$(randomSource [d|
    instance (Rand.RandomGen g) => RandFu.RandomSource Gen g where
        getRandomDoubleFrom _ = 
          MkGen (\r _ -> let (x, _) = Rand.random r in x)
    |])
