import           Engine
import           Engine.Universe

import           Data.Maybe                     ( maybe )
import qualified Data.Map.Strict               as Map
import qualified Data.IntMap.Strict            as IMap
import           Debug.Trace                    ( traceId
                                                , trace
                                                , traceShow
                                                , traceShowId
                                                )
import           Test.Hspec
import           Test.Hspec.Core.QuickCheck
import           Test.Hspec.QuickCheck



prop_nonEmpty :: TestSimulation -> Bool
prop_nonEmpty (TestSimulation _ uni _) = not $ null $ uniStars uni

prop_isMiningMetal :: TestSimulation -> Bool
prop_isMiningMetal (TestSimulation _ uni actions) =
  let shipMetal =
          Map.foldl (\preSum ship -> preSum + (cargoMetal . shipCargo) ship) 0
            $ uniShips uni
      isMineAction (GA _ (ATShipStatus shipNum SSSpaceMiningMetal)) =
          let ship              = uniShips uni Map.! shipNum
              (StarNum starNum) = shipLocation ship
              star              = uniStars uni IMap.! starNum
          in  starMetal star > 0
      isMineAction _ = False
      mineActions = foldl (||) False $ map isMineAction actions
  in  shipMetal > 0 || not mineActions

prop_isMiningFuel :: TestSimulation -> Bool
prop_isMiningFuel (TestSimulation _ uni actions) =
  let shipFuel =
          Map.foldl (\preSum ship -> preSum + ((cargoFuel . shipCargo) ship)) 0
            $ uniShips uni
      isMineAction (GA _ (ATShipStatus shipNum SSSpaceMiningFuel)) =
          let ship              = uniShips uni Map.! shipNum
              (StarNum starNum) = shipLocation ship
              star              = uniStars uni IMap.! starNum
          in  starFuel star > 0
      isMineAction _ = False
      mineActions = foldl (||) False $ map isMineAction actions
  in  shipFuel > 0 || not mineActions

-- this test will cease to be valid when we implement, e.g., planet production.
-- But for now, we assume that if mining just moves stuff from stars to ships
-- so if we sum them up it should be constant (within floating point tolerance)
prop_isMetalConstant :: TestSimulation -> Bool
prop_isMetalConstant (TestSimulation initUni finalUni _) =
  let getStarMetal s = starMetal s + maybe 0 portMetal (starPort s)
      initStarMetal =
          IMap.foldl (\preSum s -> preSum + getStarMetal s) 0 $ uniStars initUni
      finalStarMetal = IMap.foldl (\preSum s -> preSum + getStarMetal s) 0
        $ uniStars finalUni
      finalShipMetal =
          Map.foldl (\preSum ship -> preSum + ((cargoMetal . shipCargo) ship)) 0
            $ uniShips finalUni
  in  abs (initStarMetal - (finalStarMetal + finalShipMetal)) < 0.01

-- this test will cease to be valid when we implement, e.g., planet production.
-- But for now, we assume that if mining just moves stuff from stars to ships
-- so if we sum them up it should be constant (within floating point tolerance)
prop_isFuelConstant :: TestSimulation -> Bool
prop_isFuelConstant (TestSimulation initUni finalUni _) =
  let getStarFuel s = starFuel s + maybe 0.0 portFuel (starPort s)
      initStarFuel =
          IMap.foldl (\preSum s -> preSum + getStarFuel s) 0 $ uniStars initUni
      finalStarFuel =
          IMap.foldl (\preSum s -> preSum + getStarFuel s) 0 $ uniStars finalUni
      finalShipFuel =
          Map.foldl (\preSum ship -> preSum + ((cargoFuel . shipCargo) ship)) 0
            $ uniShips finalUni
      finalTotalFuel = finalStarFuel + finalShipFuel
  in  abs (initStarFuel - finalTotalFuel) < 0.01


prop_isDockingPreventedWhenNoPort :: TestSimulation -> Bool
prop_isDockingPreventedWhenNoPort (TestSimulation _ finalUni _) =
  IMap.null
    $ IMap.filter
        (\star ->
          not
            $ null
            $ filter
                (\sid ->
                  let ship = (uniShips finalUni) Map.! sid
                  in  shipStatus ship == SSDocked
                )
            $ starShips star
        )
    $ IMap.filter (\s -> starPort s == Nothing)
    $ uniStars finalUni

prop_doShipsHaveMaxCargo :: TestSimulation -> Bool
prop_doShipsHaveMaxCargo (TestSimulation _ finalUni _) =
  Map.null
    $ Map.filter (\s -> (cargoSize . shipCargo) s > shipMaxCargo s)
    $ uniShips finalUni

prop_noShipsHaveNegativeCargo :: TestSimulation -> Bool
prop_noShipsHaveNegativeCargo (TestSimulation _ finalUni _) =
  Map.null
    $ Map.filter
        (\s ->
          let cargo = shipCargo s
          in  cargoFuel cargo
                <  0
                || cargoMetal cargo
                <  0
                || cargoOrgan cargo
                <  0
                || cargoElec cargo
                <  0
        )
    $ uniShips finalUni

prop_noPortsHaveNegativeCargo :: TestSimulation -> Bool
prop_noPortsHaveNegativeCargo (TestSimulation _ finalUni _) =
  IMap.null
    $ IMap.filter
        (\s -> case starPort s of
          Nothing -> False
          Just p ->
            portFuel p
              <  0
              || portMetal p
              <  0
              || portOrgan p
              <  0
              || portElec p
              <  0
        )
    $ uniStars finalUni

prop_noPlayersHaveDebt :: TestSimulation -> Bool
prop_noPlayersHaveDebt (TestSimulation _ finalUni _) =
  Map.null $ Map.filter (\p -> playerCash p < 0) $ uniPlayers finalUni

prop_shipsOnlyLandOnExistingPlanets :: TestSimulation -> Bool
prop_shipsOnlyLandOnExistingPlanets (TestSimulation _ finalUni _) =
  Map.null
    $ Map.filter
        (\s ->
          let (StarNum starNum) = shipLocation s
              star              = uniStars finalUni IMap.! starNum
          in  case shipStatus s of
                SSPlanet pid -> not $ pid `elem` starPlanets star
                _            -> False
        )
    $ uniShips finalUni

prop_sensibleAttack :: TestSimulation -> Bool
prop_sensibleAttack (TestSimulation _ finalUni _) =
  Map.null
    $ Map.filter
        (\s -> case shipStatus s of
          SSSpaceAttacking targetId _ ->
            let targetShip = uniShips finalUni Map.! targetId
            in  (cargoFuel . shipCargo) s
                  <  0.01
                  || shipLocation targetShip
                  /= shipLocation s
                  || shipOwner targetShip
                  == shipOwner s
                  || shipFighters s
                  <  1
                  || isShipUnderAttack s
          _ -> False
        )
    $ uniShips finalUni

prop_towedShipsMove :: TowingSimulation -> Bool
prop_towedShipsMove (TowingSimulation _ finalUni actions) =
  if null $ filter
       (\case
         GA _ (ATShipStatus _ (SSTowedBy _)) -> True
         _ -> False
       )
       actions
    then True
    else
      Map.null
      $ Map.filter
          (\s -> case shipStatus s of
            SSTowedBy towerShipId ->
              let towerShip = uniShips finalUni Map.! towerShipId
              in  shipLocation towerShip /= shipLocation s
            _ -> False
          )
      $ uniShips finalUni

starShipShipLocConsistency :: Universe -> Bool
starShipShipLocConsistency uni =
  ( IMap.null
    $ IMap.filter
        (\star ->
          not
            $ null
            $ filter
                (\shipId ->
                  let ship = uniShips uni Map.! shipId
                  in  shipLocation ship /= starId star
                )
            $ starShips star
        )
    $ uniStars uni
    )
    && ( Map.null
       $ Map.filter
           (\ship ->
             let StarNum sid = shipLocation ship
                 star        = uniStars uni IMap.! sid
             in  not $ shipId ship `elem` starShips star
           )
       $ uniShips uni
       )

prop_consistentShipLocations :: TowingSimulation -> Bool
prop_consistentShipLocations (TowingSimulation _ finalUni _) =
  starShipShipLocConsistency finalUni
prop_consistentShipLocations' :: TestSimulation -> Bool
prop_consistentShipLocations' (TestSimulation _ finalUni _) =
  starShipShipLocConsistency finalUni


main :: IO ()
main = hspec $ parallel $ modifyMaxSuccess (const 2) $ do
  describe "Game Engine Property Tests" $ do

    prop "does the universe remain nonempty?" prop_nonEmpty
    prop "does metal mining work?"            prop_isMiningMetal
    prop "does fuel mining work?"             prop_isMiningFuel
    prop "does metal remain constant?"        prop_isMetalConstant
    prop "does fuel remain constant?"         prop_isFuelConstant
    prop "ships are prevented from docking when star doesn't have a port?"
         prop_isDockingPreventedWhenNoPort
    prop "ships don't go beyond max cargo" prop_doShipsHaveMaxCargo
    prop "ships don't have negative cargo" prop_noShipsHaveNegativeCargo
    prop "ports don't have negative cargo" prop_noPortsHaveNegativeCargo
    prop "ships don't land on planets that aren't there"
         prop_shipsOnlyLandOnExistingPlanets
    prop "players can't have negative money" prop_noPlayersHaveDebt
    prop "players can only purchase ships when cmdShip is on a homeworld"
      $ pendingWith "I'm not sure how to test this."
    prop
      "towed ships are always in the same system as the ship that's towing them"
      prop_towedShipsMove
    prop
      "ships aren't attacking ships when they have no fuel or when the target ships aren't there"
      prop_sensibleAttack
    prop "ships have consistent locations with stars - towing action"
         prop_consistentShipLocations
    prop "ships have consistent locations with stars - single action"
         prop_consistentShipLocations'
