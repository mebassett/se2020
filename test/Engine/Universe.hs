module Engine.Universe where
import           Engine.Action                  ( runAction
                                                , Universe(..)
                                                , GameAction(..)
                                                , ActionType(..)
                                                )
import           Engine.GameTick                ( gameTick )
import           Engine.Player                  ( GameUser(..)
                                                , Player(..)
                                                )
import           Engine.Ship                    ( Ship(..)
                                                , ShipStatus(..)
                                                , ShipTemplate(..)
                                                , shipFromTemplate
                                                , basicShip
                                                , isShipInSpace
                                                )
import           Engine.Star                    ( Star(..) )
import           Engine.Ids
import           GameGen                        ( generateGame
                                                , GameMode(..)
                                                )
import           GameConfig                     ( defaultGameCfg
                                                , GameConfig(..)
                                                , clusters
                                                )

import           RandFuInterfaces               ( )

import           Control.Monad                  ( foldM )
import qualified Data.Random                   as RandFu
import qualified Data.UUID                     as UUID
import           Data.IntMap.Strict             ( (!) )
import qualified Data.Map.Strict               as Map
import           Data.Map.Strict                ( elems
                                                , keys
                                                )
import           Data.Maybe                     ( catMaybes )
import qualified Data.Set                      as Set
import           Data.Text                      ( Text
                                                , pack
                                                )
import           Test.QuickCheck
import           Test.QuickCheck.Gen            ( chooseAny
                                                , Gen(..)
                                                )
import           Test.QuickCheck.Random         ( mkQCGen
                                                , QCGen(..)
                                                )


import           System.Random                  ( RandomGen )
import           Control.Monad.Random           ( Rand
                                                , evalRand
                                                )


chooseAnyRand :: Rand QCGen a -> Gen a
chooseAnyRand m = MkGen helper where helper qcgen _ = evalRand m qcgen

qcGameTick ac uni = chooseAnyRand $ gameTick ac uni

instance GameMode Gen QCGen where
  -- randomGen :: (Rand.Random a) => m a
  randomGen     = chooseAny
  fromMonadRand = chooseAnyRand

-- randomSample :: RandFu.RVar a -> m a
  randomSample  = RandFu.sampleFrom (mkQCGen 0)

  getUUID       = arbitrary


instance Arbitrary Universe where
  arbitrary = do
    numStars <- arbitrary `suchThat` (>= 5) `suchThat` (< 8)
    universe <- generateGame $ defaultGameCfg { gameCfgNumStars = numStars
                                              , gameCfgPortProb = 0.0
                                              , gameCfgGalaxy   = clusters
                                              }
    players <-
      arbitrary
      `suchThat` (\s -> length s >= 10)
      `suchThat` (\s -> length s < 15)
    playerShips  <- sequence [ shipGen p | p <- players ]
    playerShips2 <- sequence [ shipGen p | p <- players ]
    playerShips3 <- sequence [ shipGen p | p <- players ]
    let initialActions =
          [ GA GameAdmin (ATIntroducePlayer p) | p <- players ]
            ++ [ GA GameAdmin (ATNewShip (StarNum 1) s) | s <- playerShips ]
            ++ [ GA GameAdmin (ATNewShip (StarNum 1) s) | s <- playerShips2 ]
            ++ [ GA GameAdmin (ATNewShip (StarNum 1) s) | s <- playerShips3 ]
    return $ foldl runAction universe initialActions

instance Arbitrary UUID.UUID where
  arbitrary = chooseAny

instance Arbitrary Text where
  arbitrary = pack <$> arbitrary

instance Arbitrary PlayerNum where
  arbitrary = PlayerNum <$> arbitrary

instance Arbitrary ShipNum where
  arbitrary = ShipNum <$> arbitrary

instance Arbitrary Player where
  arbitrary =
    Player
      <$>        arbitrary
      <*>        arbitrary
      <*>        pure Nothing
      <*>        arbitrary
      `suchThat` (>= 0)
      <*>        pure (Set.fromList [StarNum 1])

shipGen :: Player -> Gen Ship
shipGen owner =
  shipFromTemplate
    <$> pure (playerId owner)
    <*> arbitrary
    <*> pure (StarNum 1)
    <*> arbitrary
    <*> pure (basicShip { shipTCargo = 2.0 })  -- smaller ship means quicker testing


shipActions :: Universe -> Ship -> [Gen GameAction]
shipActions uni ship =
  let
    StarNum shipStarNum = shipLocation ship
    star                = uniStars uni ! shipStarNum
    starNums            = starLinks star
    playerAction        = return . GA (GamePlayer (shipOwner ship))
    shipNum             = shipId ship
    basicActions =
      [ playerAction $ ATMoveShip shipNum starNum
        | starNum <- Set.toList starNums
        ]
        ++ [ playerAction $ ATShipStatus shipNum (SSPlanet pid)
           | pid <- keys $ uniPlanets uni
           ]
        ++ [ playerAction $ ATDockShip shipNum
           , playerAction $ ATShipStatus shipNum SSSpaceMiningMetal
           , playerAction $ ATShipStatus shipNum SSSpaceMiningFuel
           ]
        ++ [ playerAction $ ATShipStatus shipNum (SSTowedBy newShipNum)
           | newShipNum <-
             filter
                 (\sid ->
                   let ship' = uniShips uni Map.! sid
                   in  shipOwner ship' == shipOwner ship
                 )
               $ starShips star
           ]
        ++ [ playerAction $ ATAttackShip shipNum targetShipNum
           | targetShipNum <- map shipId $ Map.elems $ uniShips uni
           ]
    dockedActions =
      [ playerAction $ ATDockSellFuel shipNum
      , playerAction $ ATDockSellMetal shipNum
      , arbitrary `suchThat` (> 0) >>= \amount ->
        playerAction $ ATDockBuyFuel shipNum amount
      , arbitrary `suchThat` (> 0) >>= \amount ->
        playerAction $ ATDockBuyMetal shipNum amount
      , playerAction $ ATUndockShip shipNum
      ]
    planetActions = [playerAction $ ATUndockShip shipNum]
  in
    case shipStatus ship of
      SSSpaceIdle -> basicActions
      SSDocked    -> dockedActions
      SSPlanet _  -> planetActions
      _           -> []



generateAction :: Universe -> Gen GameAction
generateAction uni =
  let
    userDrivenActions =
      [ ATPurchaseShip <$> arbitrary <*> pure basicShip <*> arbitrary >>= \a ->
          return $ GA (GamePlayer pid) a
      | pid <- keys $ uniPlayers uni
      ]
    shipDrivenActions =
      concat [ shipActions uni s | s <- elems $ uniShips uni ]
  in
    oneof $ userDrivenActions ++ shipDrivenActions


data TestSimulation = TestSimulation Universe Universe [GameAction] deriving Show

instance Arbitrary TestSimulation where
  arbitrary = do
    numActions          <- arbitrary `suchThat` (>= 200) `suchThat` (< 1500)
    numTicks <- arbitrary `suchThat` (>= 60 * 60) `suchThat` (< 60 * 60 * 5)
    initUni             <- arbitrary
    (finalUni, actions) <- runSimulation numActions numTicks initUni
    return $ TestSimulation initUni finalUni actions

data TowingSimulation = TowingSimulation Universe Universe [GameAction] deriving Show

runTicks :: Int -> Universe -> Gen Universe
runTicks numTicks uni =
  foldM (\u _ -> fst <$> qcGameTick [] u) uni [1 .. numTicks]

instance Arbitrary TowingSimulation where
  arbitrary = do
    numActions          <- arbitrary `suchThat` (>= 1) `suchThat` (< 5)
    numTicks <- arbitrary `suchThat` (>= 60 * 60) `suchThat` (< 60 * 60 * 5)
    initUni             <- arbitrary
    (finalUni, actions) <- runChainActions numActions numTicks initUni
    return $ TowingSimulation initUni finalUni actions
   where
    runChainActions :: Int -> Int -> Universe -> Gen (Universe, [GameAction])
    runChainActions numActions numTicks uni =
      foldM helper (uni, []) [1 .. numActions]
        >>= (\(uni', a) -> do
              retUni <- runTicks numTicks uni'
              return (retUni, a)
            )
     where
      helper (uni', a) _ = do
        actions    <- generateChain uni'
        (uni'', _) <- qcGameTick actions uni'
        retUni     <- runTicks 20 uni''
        return (retUni, a ++ actions)
    theList :: Universe -> [Gen [GameAction]]
    theList uni =
      [ let playerAction = GA (GamePlayer (shipOwner ship))
            towAction    = ATShipStatus newShipNum (SSTowedBy cmdShipId)
            moveShip     = ATMoveShip cmdShipId starNum
        in  return [playerAction towAction, playerAction moveShip]
      | cmdShipId <- (catMaybes . elems) (playerCmdShip <$> uniPlayers uni)
      , let ship           = uniShips uni Map.! cmdShipId
            StarNum starId = shipLocation ship
            star           = uniStars uni ! starId
      , newShipNum <-
        filter
            (\sid ->
              let ship' = uniShips uni Map.! sid
              in  shipOwner ship' == shipOwner ship && sid /= cmdShipId
            )
          $ starShips star
      , starNum    <- Set.toList $ starLinks star
      ]
    generateChain uni =
      oneof
        $ [ let playerAction = GA (GamePlayer (shipOwner ship))
                towAction    = ATShipStatus newShipNum (SSTowedBy cmdShipId)
                moveShip     = ATMoveShip cmdShipId starNum
            in  return [playerAction towAction, playerAction moveShip]
          | cmdShipId <- (catMaybes . elems) (playerCmdShip <$> uniPlayers uni)
          , let ship           = uniShips uni Map.! cmdShipId
                StarNum starId = shipLocation ship
                star           = uniStars uni ! starId
          , newShipNum <-
            filter
                (\sid ->
                  let ship' = uniShips uni Map.! sid
                  in  shipOwner ship' == shipOwner ship && sid /= cmdShipId
                )
              $ starShips star
          , starNum    <- Set.toList $ starLinks star
          ]




runSimulation :: Int -> Int -> Universe -> Gen (Universe, [GameAction])
runSimulation numActions numTicks uni =
  foldM helper (uni, []) [1 .. numActions]
    >>= (\(uni', a) -> do
          retUni <- runTicks numTicks uni'
          return (retUni, a)
        )
 where
  helper (uni', a) _ = do
    action     <- generateAction uni'
    (uni'', _) <- qcGameTick [action] uni'
    retUni     <- runTicks 20 uni''
    return (retUni, a ++ [action])
