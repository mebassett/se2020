module Main where

import           Engine
import           GameGen
import           GameConfig                     ( defaultGameCfg )
import           Control.Concurrent.STM        as STM
import           Control.Monad                 as Ctrl

import           Data.IntMap.Strict             ( (!) )
import qualified Data.Map.Strict               as Map
import           Data.Semigroup                 ( (<>) )
import qualified Data.Set                      as Set
import           Data.Text                      ( pack )
import           Data.UUID.V4                   ( nextRandom )
import           Options.Applicative           as CmdOpt
import           Options.Applicative            ( ParserInfo
                                                , ParserResult(..)
                                                , defaultPrefs
                                                , execParserPure
                                                , subparser
                                                , command
                                                , info
                                                , idm
                                                , progDesc
                                                , auto
                                                , metavar
                                                )
import           Text.Regex.PCRE.Heavy
import           System.Exit                    ( exitSuccess )

gameSpeed :: Int
gameSpeed = 1000000 `div` 24 -- 24 times a second.

data GameCmd
  = PrintStarSystem
  | Status
  | MoveShip ShipNum Int
  | MineMetal ShipNum
  | MineFuel ShipNum
  | MineStop ShipNum
  | Exit
  deriving Show

gameCmdParser :: ParserInfo GameCmd
gameCmdParser = info
  (subparser
    (  command "pss"       (info pssParser (progDesc "Print Star System"))
    <> command "status"    (info (pure Status) (progDesc "print ship status"))
    <> command "moveShip"  (info moveShipParser (progDesc ""))
    <> command "mineMetal" (info mineMetalParser (progDesc ""))
    <> command "mineFuel"  (info mineFuelParser (progDesc ""))
    <> command "mineStop"  (info mineStopParser (progDesc ""))
    <> command "exit"      (info exitParser (progDesc ""))
    )
  )
  idm
 where
  pssParser      = pure PrintStarSystem
  moveShipParser = MoveShip <$> argument auto (metavar "ship id") <*> argument
    auto
    (metavar "to star system")
  mineMetalParser = MineMetal <$> argument auto (metavar "ship id")
  mineFuelParser  = MineFuel <$> argument auto (metavar "ship id")
  mineStopParser  = MineStop <$> argument auto (metavar "ship id")
  exitParser      = pure Exit

queueAction :: TVar [GameAction] -> GameAction -> IO ()
queueAction tActionQueue gameAction =
  atomically $ STM.modifyTVar tActionQueue (\q -> q ++ [gameAction])

main :: IO ()
main = do
  initUniverse                 <- generateGame defaultGameCfg
  (_, tActionQueue, tUniverse) <- startGame initUniverse gameSpeed
  putStrLn "player name:"
  playerName     <- getLine
  newPlayerIdRaw <- nextRandom
  let playerId' = PlayerNum newPlayerIdRaw
      player =
        Player (pack playerName) playerId' Nothing 0 $ Set.singleton $ StarNum 1
  queueAction tActionQueue $ GA GameAdmin $ ATIntroducePlayer player
  newShipId <- nextRandom
  let newShip = shipFromTemplate playerId'
                                 newShipId
                                 (StarNum 1)
                                 (pack playerName)
                                 basicShip
  queueAction tActionQueue $ GA GameAdmin $ ATNewShip (StarNum 1) newShip

  forever $ do
    userCmdRaw <- getLine
    let userCmd = execParserPure defaultPrefs gameCmdParser
          $ split [re|\s+(?![^(]*\))|] userCmdRaw
    case userCmd of
      Failure errMsg -> do
        putStrLn "I didn't understand that."
        putStrLn $ show errMsg
      CompletionInvoked _      -> putStrLn "I didn't understand that."
      Success           Status -> do
        universe <- STM.readTVarIO tUniverse
        let mCmdShip = do
              cmdShipId <- playerCmdShip $ uniPlayers universe Map.! playerId'
              return $ uniShips universe Map.! cmdShipId
        putStrLn $ show $ mCmdShip
      Success PrintStarSystem -> do
        universe <- STM.readTVarIO tUniverse
        let player'  = uniPlayers universe Map.! playerId'
            mCmdShip = playerCmdShip player'
        case mCmdShip of
          Nothing -> putStrLn "You don't have a ship."
          Just cmdShipId ->
            let cmdShip             = uniShips universe Map.! cmdShipId
                (StarNum playerLoc) = shipLocation cmdShip
            in  putStrLn $ show $ uniStars universe ! playerLoc

      (Success (MoveShip shId toStarId)) ->
        queueAction tActionQueue $ GA GameAdmin $ ATMoveShip
          shId
          (StarNum toStarId)
      (Success (MineMetal sid)) ->
        queueAction tActionQueue $ GA GameAdmin $ ATShipStatus
          sid
          SSSpaceMiningMetal
      (Success (MineFuel sid)) ->
        queueAction tActionQueue $ GA GameAdmin $ ATShipStatus
          sid
          SSSpaceMiningFuel
      (Success (MineStop sid)) ->
        queueAction tActionQueue $ GA GameAdmin $ ATShipStatus sid SSSpaceIdle


      (Success Exit) -> exitSuccess
