module Auth0Fix where
import           Data.Aeson
import           Data.ByteString.Lazy           ( ByteString )
import qualified Crypto.JOSE.JWK               as JWK

auth0TokenFix :: ByteString -> Maybe JWK.JWKSet
auth0TokenFix = decode
