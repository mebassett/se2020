module Main where
import qualified Control.Concurrent            as CtrlCon
import           Control.Monad                  ( forever )
import           Control.Monad.Trans.Reader     ( runReaderT )
import           Crypto.JOSE.JWK                ( JWK
                                                , JWKSet(..)
                                                )
import           Data.Yaml.Config               ( load
                                                , lookup
                                                )
import           Servant                        ( Application
                                                , Context(..)
                                                , Handler
                                                , Proxy(..)
                                                , hoistServerWithContext
                                                , serveWithContext
                                                )
import           Servant.Auth.Server            ( CookieSettings(..)
                                                , JWT
                                                , JWTSettings(..)
                                                , IsMatch(..)
                                                , defaultJWTSettings
                                                , defaultCookieSettings
                                                )
import           Network.HTTP.Simple            ( getResponseBody
                                                , httpLbs
                                                , parseRequest
                                                )
import qualified Network.Wai.Handler.Warp      as Warp
import           Network.Wai.Logger             ( withStdoutLogger )
import           Network.Wai.Middleware.AddHeaders
                                                ( addHeaders )

import           Auth0Fix                       ( auth0TokenFix )
import           Engine                         ( resumeGame
                                                , startGame
                                                )
import           GameConfig                     ( defaultGameCfg )
import           GameGen                        ( generateGame )
import           Persistence                    ( loadServer
                                                , saveServer
                                                )
import           Servant.ServantApi             ( AppM
                                                , GameState(..)
                                                , TotalApi
                                                , seServer
                                                )
import           User                           ( getUserList )
import           Websockets.WSServer            ( wsServer )

gameSpeed :: Int
gameSpeed = 1000000 `div` 24 -- 24 times a second.

proxySEApi :: Proxy (TotalApi '[JWT])
proxySEApi = Proxy


nt :: GameState -> AppM a -> Handler a
nt s x = runReaderT x s


seApplication :: JWTSettings -> GameState -> Application
seApplication jwtSettings gs =
  let context :: Context '[CookieSettings, JWTSettings]
      context =
          (  defaultCookieSettings { cookieXsrfSetting = Nothing }
          :. jwtSettings --defaultJWTSettings jwkKey
          :. EmptyContext
          )
      proxyCtx :: Proxy '[CookieSettings, JWTSettings]
      proxyCtx = Proxy
  in  serveWithContext proxySEApi context
        $ hoistServerWithContext proxySEApi proxyCtx (nt gs) seServer

getJwks :: String -> IO JWK
getJwks uri = do
  bs <- parseRequest uri >>= httpLbs >>= return . getResponseBody
  case auth0TokenFix bs of
    Nothing ->
      error "Cannot get jwk.  It return Nothing. Please check se2020.yaml"
    Just (JWKSet [jwk]) -> return jwk
    Just (JWKSet a    ) -> return $ head a

persistGame :: GameState -> IO ()
persistGame gs = forever $ do
  CtrlCon.threadDelay $ 1000000 * 60 * 3 -- save game every 3 minutes
  saveServer gs


getGameState :: IO GameState
getGameState = do
  mGameState <- loadServer
  case mGameState of
    Just gs -> do
      putStrLn "Loading game from files..."
      resumeGame gameSpeed
                 (gsNotifications gs)
                 (gsTActionQueue gs)
                 (gsTUniverse gs)
      return gs
    Nothing -> do
      putStrLn "Starting new game..."
      initUniverse        <- generateGame defaultGameCfg
      (notes, queue, uni) <- startGame initUniverse gameSpeed
      tUserList           <- getUserList
      return $ GameState queue notes uni tUserList




main :: IO ()
main = do
  config      <- load "./se2020.yaml"
  jwksUri     <- Data.Yaml.Config.lookup "jwksUri" config
  jwkAudience <- Data.Yaml.Config.lookup "jwkAudience" config
  jwks        <- getJwks jwksUri
  gameState   <- getGameState

  _           <- CtrlCon.forkIO $ persistGame gameState

  putStrLn "Starting server"
  withStdoutLogger $ \logger ->
    let
      settings =
        Warp.setLogger logger $ Warp.setPort 8081 $ Warp.defaultSettings
      jwtSettings = (defaultJWTSettings jwks)
        { audienceMatches = \aud ->
          if show aud == "OrURI " ++ jwkAudience then Matches else DoesNotMatch
        }
    in
      Warp.runSettings settings
      $ addHeaders [("Cache-Control", "no-cache")]
      $ wsServer jwtSettings gameState
      $ seApplication jwtSettings gameState
