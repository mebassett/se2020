make www/se.js DEBUG=1
while inotifywait -r -e modify --exclude '[^(elm|scss|ts)]$' frontend/; do
    echo "files changed, cleaning..."
    rm www/*.js
    rm www/*.css
    echo "rebuilding..."
    make www/se.js DEBUG=1
done 
