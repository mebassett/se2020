const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require('autoprefixer');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

const PROD = 'production';
const DEV = 'dev';
const TARGET_ENV =  process.env.npm_lifecycle_event === 'build' ? PROD : DEV;

const entryPath = path.join(__dirname, 'frontend/js/index.js');
const outputPath = path.join(__dirname, 'www');
const outputFilename = TARGET_ENV === PROD ? '[name]-[hash].js' : '[name].js';

const commonConfig = {
    output: {
        path: outputPath,
        filename: `${outputFilename}`
    },
    resolve: {
        extensions: ['.js', '.ts', '.elm'],
        modules: ['node_modules']
    },
    module: {
        noParse: /\.elm$/,
        rules: [{
            test: /\.(eot|ttf|woff|woff2|svg)$/,
            use: 'file-loader?publicPath=../../&name=[hash].[ext]'
        }, {
            test: /\.tsx?$/,
            use: 'ts-loader',
            exclude: /node_modules/,            
        }]
    },
    plugins: [
        new webpack.LoaderOptionsPlugin({
            options: {
                postcss: [autoprefixer()]
            }
        }),
        new HtmlWebpackPlugin({
            template: 'frontend/index.html',
            inject:'body',
            filename:'index.html'
        })
    ]
};

if (TARGET_ENV === DEV) {
    module.exports = merge(commonConfig, {
        devtool: "source-map",
        entry: [
            'webpack-dev-server/client?http://localhost:8080',
            entryPath
        ],
        devServer: {
            historyApiFallback: true,
            contentBase: './www',
            hot: true,
            proxy: {
                '/game/**': { target: 'http://localhost:8081'},
                '/game': {target: 'ws://localhost:8081', ws:true}
            }

        },
        module: {
            rules: [{
                test: /\.elm$/,
                exclude: [/elm-stuff/, /node_modules/],
                use: [{
                    loader: 'elm-webpack-loader',
                    options: {
                        verbose: true,
                        debug: true
                    }
                }]
            },{
                test: /\.sc?ss$/,
                use: ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader']
            }]
        }
    });
}

if (TARGET_ENV === PROD) {
    module.exports = merge(commonConfig, {
        entry: entryPath,
        optimization: {
            minimizer: [
                // https://elm-lang.org/0.19.0/optimize
                new TerserPlugin({
                    terserOptions: {
                        mangle: false,
                        compress: {
                            pure_funcs: ['F2','F3','F4','F5','F6','F7','F8','F9','A2','A3','A4','A5','A6','A7','A8','A9'],
                            pure_getters: true,
                            keep_fargs: false,
                            unsafe_comps: true,
                            unsafe: true,
                        }
                    },
                    extractComments:false
                }),
               new TerserPlugin({ terserOptions: { mangle: true },
                                  extractComments: false })
            ]
        },

        module: {
            rules: [{
                test: /\.elm$/,
                exclude: [/elm-stuff/, /node_modules/],
                use: { loader: 'elm-webpack-loader',
                       options: { optimize: true } }
            }, {
                test: /\.sc?ss$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'postcss-loader', 'sass-loader']
                })
            }]
        },
        plugins: [
            new ExtractTextPlugin({
                filename: '[name]-[hash].css',
                allChunks: true,
            })
        ]
    });
}
