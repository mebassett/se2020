import { PortManager } from '../typescript/PortManager.ts';
import { UniMap } from '../typescript/UniMap.ts';
require('../sass/site.scss');
const Elm = require('../src/Main').Elm;


let auth0 = null;
const fetchAuthConfig = () => fetch("/auth_config.json");
async function configureClient () {
    const response = await fetchAuthConfig();
    const config = await response.json();

    auth0 = await createAuth0Client({ domain: config.domain, 
                                      client_id: config.clientId,
                                      audience: config.audience});

};
const loadApp = async() => {
  const isAuthenticated = await auth0.isAuthenticated();
  if(!isAuthenticated)
    return;

  const token = await auth0.getTokenSilently();

  const app = Elm.Main.init({flags:{jwt: token}});
  const portManager = new PortManager(app);
  const uniMap = new UniMap(app, "uniMap");
  window.onbeforeunload = function(){
      portManager.closeSocket();
  };
};
const login = async () => {

    await auth0.loginWithRedirect({ redirect_uri: window.location.origin+"/index.html" });
};
window.onload = async() => {
  await configureClient();
  const isAuthenticated = await auth0.isAuthenticated();
  if (isAuthenticated) {
      loadApp();
      return;
  } 
  const query = window.location.search;
  if(query.includes("code=") && query.includes("state=")){
      await auth0.handleRedirectCallback();
      loadApp();
  }else
    login();
};

