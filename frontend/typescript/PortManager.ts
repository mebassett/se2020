export { PortManager } ;
const getWsUrl = ():string => {
    const loc : Location = window.location;
    const wsProtocol : string = (loc.protocol === "https:") ? "wss://" : "ws://";
    const wsPath : string = (loc.protocol === "https:") ? "/wsgame" : "/game";
    return wsProtocol + loc.host + wsPath;
};

const jwtToWSMsg = (jwtStr: string): string => {
  const ret = { tag: 'Login', contents: jwtStr } ;
  return JSON.stringify(ret);
};


class PortManager {
    private elm : any;
    private websocket : WebSocket;
    private jwtStr: string
    private messageQueue : Array<string>;

    constructor(app : any) {
        this.elm = app;
        this.messageQueue = [];
        this.jwtStr = "";

        this.websocket = this.connectWebsocket();
        this.elm.ports.initWs.subscribe(this.initWebsocket);
        this.elm.ports.gameWs.subscribe(this.gameWs);
    }

    public initWebsocket = (jwt: string):void => {
        this.jwtStr = jwt;
        if(this.websocket.readyState === WebSocket.OPEN)
            this.websocket.send(jwtToWSMsg(jwt));
        else
            this.messageQueue.push(jwtToWSMsg(jwt));
    }

    private connectWebsocket = ():WebSocket => {
        const ws = new WebSocket(getWsUrl());
        ws.onopen = this.processBacklog;
        ws.onmessage = this.processIncoming;
        ws.onerror = (e: Event) => {
            console.log(e);
        };
        ws.onclose = this.wsClose;
        return ws;
    }
    
    private wsClose = (e: CloseEvent):void => {
        console.log("Websocket connection close..Trying to reopen.");
        console.log(e);
        this.websocket = this.connectWebsocket();
        this.initWebsocket(this.jwtStr);
    };

    private processBacklog = (event: Event):void => {
        for (const msg of this.messageQueue)
            this.websocket.send(msg);
    }

    private processIncoming = (event: MessageEvent):void => {
        let msg = this.decodeMsg(event.data);
        this.elm.ports.incomingWSMsg.send(msg);
    }
    private decodeMsg = (rawMsg: string):WSMsg => {
        return JSON.parse(JSON.parse(rawMsg));

    }
    private gameWs = (rawValue: string): void => {
        const msg = JSON.stringify(rawValue);
        
        if(this.websocket.readyState === WebSocket.OPEN)
            this.websocket.send(msg);
        else
            this.messageQueue.push(msg);
    }

    public closeSocket = ():void => {
        this.websocket.close();
    }

}

interface WSMsg {
    tag: string;
    content: any;
}
