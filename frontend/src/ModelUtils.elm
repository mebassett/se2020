module ModelUtils exposing (sendGameMsg, setModel)

import Generated.Websockets as WS
import Messages exposing (Msg)
import Ports


setModel : a -> ( a, Cmd Msg )
setModel m =
    ( m, Cmd.none )


sendGameMsg : WS.WSClientMsg -> Cmd msg
sendGameMsg wsClientMsg =
    Ports.gameWs <| WS.wsClientMsgEncoder <| wsClientMsg
