module FloatUtils exposing (flToString)

import String exposing (fromFloat)


flToString : Float -> String
flToString fl =
    fl
        * 100
        |> round
        |> toFloat
        |> \v ->
            v
                / 100
                |> fromFloat
