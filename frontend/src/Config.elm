module Config exposing (decodeSet, encodeSet, prepareJwt, urlBase)

import EverySet exposing (EverySet)
import Json.Decode
import Json.Encode


urlBase : String
urlBase =
    ""


encodeSet : (a -> Json.Encode.Value) -> EverySet a -> Json.Encode.Value
encodeSet func entries =
    Json.Encode.list func <| EverySet.toList entries


decodeSet : Json.Decode.Decoder a -> Json.Decode.Decoder (EverySet a)
decodeSet d =
    Json.Decode.andThen (EverySet.fromList >> Json.Decode.succeed) (Json.Decode.list d)


prepareJwt : String -> String
prepareJwt jwt =
    "Bearer " ++ jwt
