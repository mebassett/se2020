module Messages exposing (GameMsgType(..), Msg(..), SignupMsgType(..))

import Browser exposing (UrlRequest)
import Game.PlanetModels as PlanetModels
import Generated.Engine.Ids as Ids
import Generated.Engine.Player as Player
import Generated.Engine.Ship as Ship
import Generated.Engine.Star as Star
import Generated.Websockets as WS
import Http
import Json.Decode as D
import Url exposing (Url)
import Generated.Notifications as Notes


type Msg
    = UrlChange Url
    | LinkClicked UrlRequest
    | SignupMsg SignupMsgType
    | GameMsg GameMsgType
    | ReceiveLogin (ServerResponse Player.Player)


type alias ServerResponse a =
    Result ( Http.Error, Maybe { body : String, metadata : Http.Metadata } ) a


type SignupMsgType
    = SubmitSignup
    | SetSignupName String


type GameMsgType
    = DockToPort
    | UndockFromPort
    | LeavePlanet
    | DockBuyFuel Float
    | DockBuyMetal Float
    | DockSellFuel
    | DockSellMetal
    | GetCurrentStar (ServerResponse Star.Star)
    | GetCmdShip (ServerResponse (Maybe Ship.Ship))
    | LandOnPlanet Ids.PlanetNum
    | PatchMoveShip (ServerResponse ())
    | MoveToStar Ids.StarNum
    | TowShip Ids.ShipNum
    | StopTowShip Ids.ShipNum
    | AttackShip Ids.ShipNum
    | StopAttack
    | ChangeCommand Ids.ShipNum
    | IncomingWS (Result D.Error WS.WSServerMsg)
    | MapRendererReady (Result D.Error Bool)
    | MineMetal
    | MineFuel
    | HomeworldMsg PlanetModels.HomeworldMsgType
    | DismissNotification ( Notes.NotificationStatus, Notes.GameMsg )
    | GetStars (ServerResponse (List Star.DisplayableStar))
