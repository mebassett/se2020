module Views.StatusBars exposing (bottomStatus, topStatus)

import FloatUtils exposing (flToString)
import EngineUtils exposing (starNumToString)
import Game.PlayerStatus as PS
import Generated.Engine.Ship as Ship
import Html exposing (..)
import Html.Attributes exposing (class, for, href, id, type_)
import Html.Events exposing (onClick, onInput, onSubmit)
import Maybe.Extra as MaybeE
import Messages exposing (Msg(..))


topStatus : PS.PlayerStatus -> Html Msg
topStatus playerStatus =
    let
        player =
            playerStatus.thePlayer

        playerNameBlock =
            div [ class "__playerName" ]
                [ text "playing as: "
                , em [ class "playerName" ]
                    [ text player.playerScreenName
                    ]
                ]

        playerInfoBlock =
            div [ class "__playerInfo --inactive" ]
                [ p [] []
                , h1 [] [ text "1" ]
                , p [] [ text "players online" ]
                ]

        cashInfoBlock =
            div [ class "__cashInfo --inactive" ]
                [ p [] [ text "cash" ]
                , h1 [] [ text <| flToString player.playerCash ]
                , p [] [ text "more player info" ]
                ]

        locationStatusBlock =
            case ( playerStatus.currentStar, playerStatus.cmdShip ) of
                ( Just ( star, _ ), Just cmdShip ) ->
                    div [ class "__locationStatus --active" ]
                        [ p [] [ text "star system" ]
                        , h1 [] [ text <| "#" ++ starNumToString star.starId ]
                        , p [] [ PS.renderMiningStatus cmdShip.shipStatus ]
                        ]

                _ ->
                    div [ class "__locationStatus" ]
                        [ h1 [] [ text "???" ]
                        ]

        gameInfoBlock =
            div [] []
    in
        div [ class "topStatus" ]
            [ playerNameBlock
            , playerInfoBlock
            , cashInfoBlock
            , locationStatusBlock
            , gameInfoBlock
            ]


bottomStatus : PS.PlayerStatus -> Html Msg
bottomStatus playerStatus =
    div [ class "bottomStatus" ] <|
        case playerStatus.cmdShip of
            Nothing ->
                [ text "No command ship." ]

            Just ship ->
                [ renderCmdShip ship ]


renderCmdShip : Ship.Ship -> Html Msg
renderCmdShip ship =
    let
        cargoBox name amt =
            li []
                [ h2 [] [ text name ]
                , h1 [] [ text <| flToString amt ]
                ]
    in
        div [ class "cmdShip" ]
            [ div [ class "cmdShip__name" ]
                [ h2 [ class "name__nameHeader" ] [ text "ship name" ]
                , h1 [ class "name__name" ] [ text <| ship.shipName ]
                , h2 [ class "name__classHeader" ] [ text "ship class" ]
                , h1 [ class "name__class" ] [ text ship.shipClass ]
                ]
            , div [ class "cmdShip__tactical" ]
                [ h2 [] [ text "tactical" ]
                , ul []
                    [ li []
                        [ h2 [] [ text "Fighters" ]
                        , h1 [] [ text <| String.fromInt ship.shipFighters ++ "/" ++ String.fromInt ship.shipMaxFighters ]
                        ]
                    , li []
                        [ h2 [] [ text "Shields" ]
                        , h1 [] [ text "0/0" ]
                        ]
                    ]
                ]
            , div [ class "cmdShip__cargo" ]
                [ h2 [] [ text "cargo" ]
                , ul []
                    [ cargoBox "Fuel" ship.shipCargo.cargoFuel
                    , cargoBox "Metal" ship.shipCargo.cargoMetal
                    , cargoBox "Electronics" ship.shipCargo.cargoElec
                    , cargoBox "Organics" ship.shipCargo.cargoOrgan
                    , cargoBox "Free Space" <| PS.calcFreeCargoSpace ship
                    ]
                ]
            ]
