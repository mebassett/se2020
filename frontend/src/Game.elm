module Game exposing (updateGame, viewGame)

import Game.Planet as Planet
import Game.PlanetModels as PlanetModels
import Game.PlayerStatus as PS
import Game.StarPort as StarPort
import Game.StarSystem as StarSystem
import Generated.Api as Api
import Generated.Engine.Ship as Ship
import Generated.Engine.Star as Star
import Generated.Websockets as WS
import Html exposing (..)
import Html.Attributes exposing (class, for, href, id, type_)
import Html.Events exposing (onClick, onInput, onSubmit)
import Json.Encode
import List.Extra as ListE
import Messages exposing (GameMsgType(..), Msg(..))
import ModelUtils exposing (sendGameMsg)
import NotificationUtils as NoteUtils
import Ports
import Utils
import Views.StatusBars as StatusBars


type GamePage
    = GPStarSystem
    | GPDock
    | GPPlanet


gamePageEncoder : GamePage -> Json.Encode.Value
gamePageEncoder gp =
    case gp of
        GPStarSystem ->
            Json.Encode.string "GPStarSystem"

        GPDock ->
            Json.Encode.string "GPDock"

        GPPlanet ->
            Json.Encode.string "GPPlanet"


addPageStateCmds : PS.PlayerStatus -> ( PS.PlayerStatus, Cmd Msg ) -> ( PS.PlayerStatus, Cmd Msg )
addPageStateCmds playerStatus ( newPS, cmds ) =
    let
        newCmds =
            case decodePlayerStatus playerStatus of
                Just gamePage ->
                    Cmd.batch [ Ports.notifyCurrentGamePage <| gamePageEncoder gamePage, cmds ]

                Nothing ->
                    cmds
    in
        ( newPS, newCmds )


playerStatusToHtml : PS.PlayerStatus -> Html GameMsgType
playerStatusToHtml playerStatus =
    case decodePlayerStatus playerStatus of
        Just GPStarSystem ->
            StarSystem.viewStarSystem playerStatus

        Just GPDock ->
            StarPort.viewStarPort playerStatus

        Just GPPlanet ->
            Planet.viewPlanet playerStatus

        Nothing ->
            text "I cannot decode your player status, sorry."


decodePlayerStatus : PS.PlayerStatus -> Maybe GamePage
decodePlayerStatus playerStatus =
    case ( playerStatus.cmdShip, playerStatus.currentStar ) of
        ( Just ship, Just star ) ->
            case ship.shipStatus of
                Ship.SSDocked ->
                    Just GPDock

                Ship.SSPlanet _ ->
                    Just GPPlanet

                _ ->
                    Just GPStarSystem

        ( Nothing, Just _ ) ->
            Just GPPlanet

        ( _, _ ) ->
            Nothing


updateGame : String -> GameMsgType -> PS.PlayerStatus -> ( PS.PlayerStatus, Cmd Msg )
updateGame jwt msg playerStatus =
    addPageStateCmds playerStatus <|
        case msg of
            MapRendererReady (Err e) ->
                let
                    _ =
                        Utils.log "failed to decode MapRendererReady msg" e
                in
                    ( playerStatus, Cmd.none )

            MapRendererReady (Ok _) ->
                let
                    cmd =
                        Cmd.map (GameMsg << GetStars) <|
                            Api.getGameGetStars (Just jwt)
                in
                    ( playerStatus, cmd )

            GetStars (Err e) ->
                let
                    _ =
                        Utils.log "get stars err" e
                in
                    ( playerStatus, Cmd.none )

            GetStars (Ok stars) ->
                case ( decodePlayerStatus playerStatus, playerStatus.currentStar ) of
                    ( Just GPStarSystem, Just ( currentStar, _ ) ) ->
                        let
                            jsStars =
                                Json.Encode.list Star.displayableEncoder stars

                            jsArgs =
                                Json.Encode.object
                                    [ ( "stars", jsStars )
                                    , ( "currentStar", Star.encoder currentStar )
                                    ]
                        in
                            ( playerStatus, Ports.viewMap jsArgs )

                    _ ->
                        ( playerStatus, Cmd.none )

            DismissNotification note ->
                let
                    newNotes =
                        ListE.remove note playerStatus.notifications
                in
                    ( { playerStatus | notifications = newNotes }, Cmd.none )

            HomeworldMsg subMsg ->
                case ( playerStatus.currentPlanet, playerStatus.subModel ) of
                    ( Just (Star.PlanetHomeworld hwData), PS.Homeworld hwId subModel ) ->
                        let
                            ( newModel, newCmd ) =
                                Planet.update playerStatus hwData subMsg subModel

                            newStatus =
                                { playerStatus | subModel = PS.Homeworld hwId newModel }
                        in
                            ( newStatus, Cmd.map (HomeworldMsg >> GameMsg) newCmd )

                    _ ->
                        ( playerStatus, Cmd.none )

            DockToPort ->
                ( playerStatus, sendGameMsg WS.DockToPort )

            DockBuyFuel amount ->
                ( playerStatus, sendGameMsg <| WS.DockBuyFuel amount )

            DockBuyMetal amount ->
                ( playerStatus, sendGameMsg <| WS.DockBuyMetal amount )

            DockSellFuel ->
                ( playerStatus, sendGameMsg WS.DockSellFuel )

            DockSellMetal ->
                ( playerStatus, sendGameMsg WS.DockSellMetal )

            LandOnPlanet planetId ->
                ( playerStatus, sendGameMsg <| WS.LandOnPlanet planetId )

            UndockFromPort ->
                ( playerStatus, sendGameMsg WS.UndockFromPort )

            LeavePlanet ->
                ( { playerStatus | currentPlanet = Nothing, subModel = PS.None }, sendGameMsg WS.UndockFromPort )

            GetCurrentStar (Err e) ->
                let
                    _ =
                        Utils.log "cmd star err" e
                in
                    ( playerStatus, Cmd.none )

            GetCurrentStar (Ok star) ->
                let
                    newParams =
                        { playerStatus | currentStar = Just ( star, [] ) }
                in
                    ( newParams, Cmd.none )

            GetCmdShip (Err e) ->
                let
                    _ =
                        Utils.log "cmd ship err" e
                in
                    ( playerStatus, Cmd.none )

            GetCmdShip (Ok mShip) ->
                let
                    newParams =
                        { playerStatus | cmdShip = mShip }
                in
                    ( newParams, Cmd.none )

            PatchMoveShip (Err e) ->
                let
                    _ =
                        Utils.log "couldn't move" e
                in
                    ( playerStatus, Cmd.none )

            PatchMoveShip (Ok ()) ->
                ( playerStatus, Cmd.none )

            MoveToStar starNum ->
                let
                    cmd =
                        Cmd.map (GameMsg << PatchMoveShip) <|
                            Api.patchGameMoveShip (Just jwt) starNum
                in
                    ( playerStatus, cmd )

            MineMetal ->
                ( playerStatus, sendGameMsg WS.MineMetal )

            MineFuel ->
                ( playerStatus, sendGameMsg WS.MineFuel )

            TowShip shipId ->
                ( playerStatus, sendGameMsg <| WS.TowShip shipId )

            StopTowShip shipId ->
                ( playerStatus, sendGameMsg <| WS.StopTowShip shipId )

            AttackShip targetId ->
                ( playerStatus, sendGameMsg <| WS.AttackShip targetId )

            StopAttack ->
                ( playerStatus, sendGameMsg WS.StopAttack )

            ChangeCommand shipId ->
                ( playerStatus, sendGameMsg <| WS.ChangeCommand shipId )

            IncomingWS (Err e) ->
                let
                    _ =
                        Utils.log "Failed to decode Websockets msg" e
                in
                    ( playerStatus, Cmd.none )

            IncomingWS (Ok wsServerMsg) ->
                let
                    newPlayerStatus =
                        updateWSServerMsg wsServerMsg playerStatus

                    newCmd =
                        case wsServerMsg of
                            WS.SendNotifications notes ->
                                sendGameMsg <| WS.AcceptNotifications notes

                            WS.SetCurrentStar mStar ->
                                case mStar of
                                    Just ( star, _ ) ->
                                        Ports.setMapLocation <| Star.encoder star

                                    Nothing ->
                                        Cmd.none

                            _ ->
                                Cmd.none
                in
                    ( newPlayerStatus, newCmd )


updateWSServerMsg : WS.WSServerMsg -> PS.PlayerStatus -> PS.PlayerStatus
updateWSServerMsg wsServerMsg playerStatus =
    case wsServerMsg of
        WS.SendNotifications notes ->
            let
                newNotes =
                    ListE.uniqueBy NoteUtils.toComp <| playerStatus.notifications ++ notes
            in
                { playerStatus | notifications = newNotes }

        WS.SetCmdShip mShip ->
            { playerStatus | cmdShip = mShip }

        WS.SetCurrentStar mStar ->
            { playerStatus | currentStar = mStar }

        WS.SetStarShips ships ->
            { playerStatus | systemShips = ships }

        WS.SetPlayerShips ships ->
            { playerStatus | playerShips = ships }

        WS.SetPlayer newPlayer ->
            { playerStatus | thePlayer = newPlayer }

        WS.SetPlanet newPlanet ->
            let
                newModel =
                    case newPlanet of
                        Just (Star.PlanetHomeworld hwData) ->
                            PS.Homeworld hwData.homeworldId PlanetModels.Homeworld

                        _ ->
                            playerStatus.subModel
            in
                { playerStatus | currentPlanet = newPlanet, subModel = newModel }


viewGame : PS.PlayerStatus -> Html Msg
viewGame playerStatus =
    div [] <|
        (if List.length playerStatus.notifications > 0 then
            [ Html.map GameMsg <|
                div [ class "gameNotes" ] <|
                    List.map (NoteUtils.renderNote playerStatus) playerStatus.notifications
            ]
         else
            []
        )
            ++ [ div [ class "gameView" ]
                    [ StatusBars.topStatus playerStatus
                    , Html.map GameMsg <| playerStatusToHtml playerStatus
                    , StatusBars.bottomStatus playerStatus
                    ]
               ]
