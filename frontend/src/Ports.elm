port module Ports exposing (..)

import Json.Encode as E


port initWs : E.Value -> Cmd msg


port gameWs : E.Value -> Cmd msg


port incomingWSMsg : (E.Value -> msg) -> Sub msg


port viewMap : E.Value -> Cmd msg


port mapReadyToRender : (E.Value -> msg) -> Sub msg


port setMapLocation : E.Value -> Cmd msg


port notifyCurrentGamePage : E.Value -> Cmd msg
