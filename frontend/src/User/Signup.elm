module User.Signup exposing (SignupModel, updateSignup, viewCheckLogin, viewSignup)

import Generated.Api as Api
import Generated.User
import Html exposing (..)
import Html.Attributes exposing (class, for, href, id, type_)
import Html.Events exposing (onClick, onInput, onSubmit)
import Messages exposing (Msg(..), SignupMsgType(..))
import ModelUtils exposing (setModel)


type alias SignupModel =
    Generated.User.SignupInfo


updateSignup : Maybe String -> SignupMsgType -> SignupModel -> ( SignupModel, Cmd Msg )
updateSignup mJwt msg model =
    case msg of
        SetSignupName t ->
            setModel <| { model | signupInfoName = t }

        SubmitSignup ->
            if model.signupInfoName /= "" then
                let
                    newCmd =
                        Cmd.map ReceiveLogin <| Api.postGameRegister mJwt model
                in
                ( model, newCmd )

            else
                setModel <| model


viewSignup : SignupModel -> Html SignupMsgType
viewSignup m =
    div [ class "signup" ]
        [ h1 [] [ text "Signup" ]
        , form [ onSubmit SubmitSignup ]
            [ input [ id "playerName", onInput SetSignupName ] []
            , label [ for "playerName" ] [ text "game name" ]
            , button [ type_ "submit" ] [ text "signup" ]
            , a [ href "/login" ] [ text "back to login." ]
            ]
        ]


viewCheckLogin : Html SignupMsgType
viewCheckLogin =
    div [ class "checkLogin" ]
        [ h1 [] [ text "Loading." ]
        , h3 [] [ text "this might take a sec..." ]
        ]
