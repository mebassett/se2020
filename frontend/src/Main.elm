module Main exposing (main)

-- import User.Login exposing (LoginModel, updateLogin, viewLogin)

import Browser exposing (Document, UrlRequest, application)
import Browser.Navigation as Nav
import Game
import Game.PlayerStatus as PS
import Generated.Api as Api
import Generated.Engine.Ids as Ids
import Generated.Engine.Player as Player
import Generated.User as User
import Generated.Websockets as WS
import Html exposing (..)
import Html.Attributes exposing (class, for, href, id, type_)
import Html.Events exposing (onClick, onInput, onSubmit)
import Http
import Json.Decode as D
import Json.Encode as E
import Messages exposing (..)
import ModelUtils exposing (sendGameMsg, setModel)
import Ports
import Url exposing (Url)
import User.Signup exposing (SignupModel, updateSignup, viewCheckLogin, viewSignup)
import Utils


-- we don't actually need this


type alias Flag =
    { jwt : String }


type alias GlobalModel =
    { key : Nav.Key
    , m : Model
    }


type Model
    = PageGame (Maybe String) PS.PlayerStatus
    | PageCheckLogin (Maybe String)
    | PageSignup (Maybe String) SignupModel



-- = PageLogin LoginModel
-- | PageSignup SignupModel
-- | PageGame (Maybe String) PS.PlayerStatus PS.GamePage


subscriptions : GlobalModel -> Sub Msg
subscriptions _ =
    Sub.batch
        [ Ports.incomingWSMsg (D.decodeValue WS.wsServerMsgDecoder >> IncomingWS >> GameMsg)
        , Ports.mapReadyToRender (D.decodeValue D.bool >> MapRendererReady >> GameMsg)
        ]


globalUpdate : Msg -> GlobalModel -> ( GlobalModel, Cmd Msg )
globalUpdate msg model =
    let
        goLogin =
            model

        --{ model | m = PageLogin { loginInfoEmail = "", loginInfoPassword = "" } }
        processLink : UrlRequest -> ( GlobalModel, Cmd Msg )
        processLink link =
            case link of
                Browser.Internal url ->
                    let
                        newModel =
                            processUrl url.path
                    in
                        ( newModel, Nav.pushUrl model.key <| Url.toString url )

                Browser.External href ->
                    ( model, Nav.load href )

        processUrl : String -> GlobalModel
        processUrl urlPath =
            case urlPath of
                "/signup" ->
                    {- let
                           pass =
                               { password = "", passwordAgain = "" }

                           newModel =
                               { model
                                   | m =
                                       PageSignup
                                           { signupEmail = ""
                                           , signupPassword = pass
                                           , signupName = ""
                                           , signupPasswordsMatch = True
                                           }
                               }
                       in
                       newModel
                    -}
                    model

                "/login" ->
                    goLogin

                "/index.html" ->
                    goLogin

                _ ->
                    model
    in
        case ( msg, model ) of
            ( LinkClicked link, _ ) ->
                processLink link

            ( UrlChange url, _ ) ->
                ( processUrl url.path, Cmd.none )

            ( _, _ ) ->
                let
                    ( newModel, newCmd ) =
                        update msg model.m
                in
                    ( { model | m = newModel }, newCmd )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( msg, model ) of
        {- ( LoginMsg submsg, PageLogin submodel ) ->
           let
               ( newModel, nextCmd ) =
                   updateLogin submsg submodel
           in
           ( PageLogin newModel, nextCmd )

        -}
        ( SignupMsg submsg, PageSignup mJwt submodel ) ->
            let
                ( newModel, nextCmd ) =
                    updateSignup mJwt submsg submodel
            in
                ( PageSignup mJwt newModel, nextCmd )

        ( GameMsg submsg, PageGame mJwt playerStatus ) ->
            let
                ( newModel, nextCmd ) =
                    updateGame mJwt submsg playerStatus
            in
                ( newModel, nextCmd )

        ( ReceiveLogin (Ok player), PageCheckLogin mJwt ) ->
            processLogin mJwt player

        ( ReceiveLogin (Ok player), PageSignup mJwt _ ) ->
            processLogin mJwt player

        ( ReceiveLogin (Err err), PageCheckLogin mJwt ) ->
            let
                _ =
                    Utils.log "login error" err
            in
                ( PageSignup mJwt { signupInfoName = "" }, Cmd.none )

        _ ->
            ( model, Cmd.none )


processLogin : Maybe String -> Player.Player -> ( Model, Cmd Msg )
processLogin mJwt player =
    let
        newModel =
            PageGame mJwt { thePlayer = player, currentPlanet = Nothing, currentStar = Nothing, cmdShip = Nothing, systemShips = [], notifications = [], playerShips = [], subModel = PS.None }

        initWebsocket =
            case mJwt of
                Just jwt ->
                    Ports.initWs <| E.string jwt

                Nothing ->
                    Cmd.none
    in
        ( newModel, initWebsocket )


updateGame : Maybe String -> GameMsgType -> PS.PlayerStatus -> ( Model, Cmd Msg )
updateGame mJwt msg playerStatus =
    case mJwt of
        Nothing ->
            ( PageSignup mJwt { signupInfoName = "" }, Cmd.none )

        --( PageLogin { loginInfoEmail = "", loginInfoPassword = "" }, Cmd.none )
        Just jwt ->
            let
                ( newStatus, newCmd ) =
                    Game.updateGame jwt msg playerStatus
            in
                ( PageGame mJwt newStatus, newCmd )


view : GlobalModel -> Document Msg
view model =
    { title = "Solar Empire 2020"
    , body =
        [ case model.m of
            {- PageLogin m ->
               Html.map LoginMsg <| viewLogin m
            -}
            PageSignup _ m ->
                Html.map SignupMsg <| viewSignup m

            PageCheckLogin _ ->
                Html.map SignupMsg <| viewCheckLogin

            PageGame _ playerStatus ->
                Game.viewGame playerStatus
        ]
    }


main : Program Flag GlobalModel Msg
main =
    let
        init : Flag -> Url -> Nav.Key -> ( GlobalModel, Cmd Msg )
        init flag _ key =
            ( { key = key, m = PageCheckLogin (Just flag.jwt) }, Cmd.map ReceiveLogin <| Api.getGamePlayer (Just flag.jwt) )
    in
        application
            { init = init
            , view = view
            , update = globalUpdate
            , subscriptions = subscriptions
            , onUrlRequest = LinkClicked
            , onUrlChange = UrlChange
            }
