module NotificationUtils exposing (renderNote, toComp)

import Game.PlayerStatus as PS
import Generated.Engine.Ids exposing (..)
import Generated.Notifications as Notes
import Html exposing (..)
import Html.Attributes exposing (class, for, href, id, type_)
import Html.Events exposing (onClick, onInput, onSubmit)
import Messages exposing (..)
import Time


shipIdStr : ShipNum -> String
shipIdStr shipNum =
    case shipNum of
        ShipNum n ->
            "ShipNum " ++ n


starIdStr : StarNum -> String
starIdStr starNum =
    case starNum of
        StarNum n ->
            "StarNum " ++ String.fromInt n


renderNote : PS.PlayerStatus -> ( Notes.NotificationStatus, Notes.GameMsg ) -> Html GameMsgType
renderNote playerStatus ( noteStatus, gameMsg ) =
    let
        noteText =
            case gameMsg.gameMsgDatum of
                Notes.MTShipCargoFull shortShip ->
                    if Maybe.map .shipId playerStatus.cmdShip == Just shortShip.shortShipId then
                        div [] [ text "This ship's cargo is full, it cannot mine." ]
                    else
                        div []
                            [ text "your ship, a "
                            , text shortShip.shortShipClass
                            , text " class vessel in Star System "
                            , text <| starIdStr shortShip.shortShipLocation
                            , text ", has mined to its full cargo capacity."
                            , button [ onClick <| ChangeCommand shortShip.shortShipId ] [ text "Command ship." ]
                            ]

                Notes.MTAttackCanceledShipMoved attacker defender ->
                    if Maybe.map .shipId playerStatus.cmdShip == Just attacker.shortShipId then
                        div [] [ text "Your attack has stopped as the ship has moved. " ]
                    else
                        div []
                            [ text "your ship, a "
                            , text attacker.shortShipClass
                            , text " a class vessel in Star System "
                            , text <| starIdStr attacker.shortShipLocation
                            , text ", has stopped its attack on the "
                            , text defender.shortShipClass
                            , text " class vessel as it moved to another system."
                            , button [ onClick <| ChangeCommand attacker.shortShipId ] [ text "Command ship." ]
                            ]

                Notes.MTAttackCanceledNoFuel attacker defender ->
                    if Maybe.map .shipId playerStatus.cmdShip == Just attacker.shortShipId then
                        div [] [ text "Your attack has stopped as your ship is out of fuel. " ]
                    else
                        div []
                            [ text "your ship, a "
                            , text attacker.shortShipClass
                            , text " a class vessel in Star System "
                            , text <| starIdStr attacker.shortShipLocation
                            , text ", has stopped its attack on the "
                            , text defender.shortShipClass
                            , text " class vessel as your ship ran out of fuel."
                            , button [ onClick <| ChangeCommand attacker.shortShipId ] [ text "Command ship." ]
                            ]

                Notes.MTAttackCanceledNoFigs attacker defender ->
                    if Maybe.map .shipId playerStatus.cmdShip == Just attacker.shortShipId then
                        div [] [ text "Your attack has stopped as your ship is out of fighters. " ]
                    else
                        div []
                            [ text "your ship, a "
                            , text attacker.shortShipClass
                            , text " a class vessel in Star System "
                            , text <| starIdStr attacker.shortShipLocation
                            , text ", has stopped its attack on the "
                            , text defender.shortShipClass
                            , text " class vessel as your ship no longer has any fighters to continue the attack."
                            , button [ onClick <| ChangeCommand attacker.shortShipId ] [ text "Command ship." ]
                            ]

                Notes.MTAttackCanceledShipDestroyed attacker ->
                    div []
                        [ text "your ship, a "
                        , text attacker.shortShipClass
                        , text " a class vessel in Star System "
                        , text <| starIdStr attacker.shortShipLocation
                        , text ", was destroyed after receiving heavy counter-damage in an attack."
                        ]

                Notes.MTUnderAttack defendingShip attackingPlayer ->
                    if Maybe.map .shipId playerStatus.cmdShip == Just defendingShip.shortShipId then
                        div []
                            [ text "You are under attack by a vessel operated by "
                            , text attackingPlayer.shortPlayerName
                            , text ". Move!"
                            ]
                    else
                        div []
                            [ text "your ship, a "
                            , text defendingShip.shortShipClass
                            , text " a class vessel in Star System "
                            , text <| starIdStr defendingShip.shortShipLocation
                            , text ", is under attack by a vessel operated by "
                            , text <| attackingPlayer.shortPlayerName
                            , text "."
                            , button [ onClick <| ChangeCommand defendingShip.shortShipId ] [ text "Command ship." ]
                            ]

                Notes.MTShipDestroyed defendingShip attackingPlayer ->
                    div []
                        [ text "your ship, a "
                        , text defendingShip.shortShipClass
                        , text " a class vessel in Star System "
                        , text <| starIdStr defendingShip.shortShipLocation
                        , text ", was attacked and destroyed by a vessel operated by "
                        , text <| attackingPlayer.shortPlayerName
                        , text "."
                        ]

        dismissBtn =
            button [ onClick <| DismissNotification ( noteStatus, gameMsg ) ] [ text "dismiss" ]
    in
        div [ class "gameNotes__Note" ] [ noteText, dismissBtn ]


toComp : ( Notes.NotificationStatus, Notes.GameMsg ) -> String
toComp ( noteStatus, gameMsg ) =
    case gameMsg.gameMsgDatum of
        Notes.MTShipCargoFull shortShip ->
            "mtshipcargoful " ++ shipIdStr shortShip.shortShipId

        Notes.MTAttackCanceledShipMoved attacker defender ->
            "mtattackcanceledshipmoved " ++ (shipIdStr attacker.shortShipId) ++ " " ++ (shipIdStr defender.shortShipId)

        Notes.MTAttackCanceledNoFuel attacker defender ->
            "mtattackcancelednofuel " ++ (shipIdStr attacker.shortShipId) ++ " " ++ (shipIdStr defender.shortShipId)

        Notes.MTAttackCanceledNoFigs attacker defender ->
            "mtattackcancelednofuel " ++ (shipIdStr attacker.shortShipId) ++ " " ++ (shipIdStr defender.shortShipId)

        Notes.MTAttackCanceledShipDestroyed attacker ->
            "mtattackcanceledshipdestroyed " ++ (shipIdStr attacker.shortShipId)

        Notes.MTUnderAttack defendingShip attackingPlayer ->
            "mtunderattack " ++ (shipIdStr defendingShip.shortShipId) ++ " " ++ attackingPlayer.shortPlayerName

        Notes.MTShipDestroyed defendingShip attackingPlayer ->
            "mtshipdestroyed " ++ shipIdStr defendingShip.shortShipId ++ " " ++ attackingPlayer.shortPlayerName
