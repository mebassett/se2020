module Game.PlanetModels exposing (HomeworldModel(..), HomeworldMsgType(..), ShipEmporiumState(..), DocklandsState(..), defaultState)

import Generated.Engine.Ship as Ship


type HomeworldModel
    = Homeworld
    | HomeworldShipEmporium ShipEmporiumState
    | HomeworldDocklands DocklandsState


type ShipEmporiumState
    = ShipEmporium
    | NotEnoughCash
    | AskShipName Ship.ShipTemplate String
    | CompletePurchase


type DocklandsState
    = Docklands
    | AskFighterAmount Int
    | CompleteFighterPurchase


type HomeworldMsgType
    = ShipPurchaseTrigger Ship.ShipTemplate
    | ShipPurchaseLeave
    | ShipPurchaseComplete
    | ShipPurchaseEditName String
    | ClickEmporium
    | ClickDocklands
    | ClickHome
    | FighterPurchaseTrigger Int
    | FighterPurchaseAmount String
    | FighterPurchaseComplete


defaultState : HomeworldModel -> Bool
defaultState m =
    m == Homeworld || m == HomeworldShipEmporium NotEnoughCash
