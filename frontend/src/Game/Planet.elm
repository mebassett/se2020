module Game.Planet exposing (update, viewPlanet)

import FloatUtils exposing (flToString)
import Game.PlayerStatus as PS
import Game.PlanetModels exposing (..)
import Generated.Engine.Ship as Ship
import Generated.Engine.Star as Star
import Html exposing (..)
import Html.Attributes as Attr exposing (class, for, href, id, type_)
import Html.Events exposing (onClick, onInput, onSubmit)
import Messages exposing (GameMsgType(..))
import Maybe.Extra as MaybeE
import ModelUtils exposing (sendGameMsg)
import Generated.Websockets as WS


update : PS.PlayerStatus -> Star.HomeworldData -> HomeworldMsgType -> HomeworldModel -> ( HomeworldModel, Cmd HomeworldMsgType )
update { thePlayer } hwData msg model =
    case msg of
        ClickEmporium ->
            ( HomeworldShipEmporium ShipEmporium, Cmd.none )

        ClickDocklands ->
            ( HomeworldDocklands Docklands, Cmd.none )

        ClickHome ->
            ( Homeworld, Cmd.none )

        FighterPurchaseTrigger amt ->
            ( HomeworldDocklands (AskFighterAmount amt), Cmd.none )

        FighterPurchaseAmount strAmt ->
            ( HomeworldDocklands (AskFighterAmount <| Maybe.withDefault 0 <| String.toInt strAmt), Cmd.none )

        FighterPurchaseComplete ->
            case model of
                HomeworldDocklands (AskFighterAmount amt) ->
                    ( HomeworldDocklands CompleteFighterPurchase, sendGameMsg <| WS.PurchaseFighters amt )

                _ ->
                    ( model, Cmd.none )

        ShipPurchaseTrigger shipType ->
            if shipType.shipTPrice > thePlayer.playerCash then
                ( HomeworldShipEmporium NotEnoughCash, Cmd.none )
            else
                ( HomeworldShipEmporium (AskShipName shipType ""), Cmd.none )

        ShipPurchaseLeave ->
            ( HomeworldShipEmporium ShipEmporium, Cmd.none )

        ShipPurchaseComplete ->
            case model of
                HomeworldShipEmporium (AskShipName shipType newName) ->
                    if newName == "" then
                        ( model, Cmd.none )
                    else
                        let
                            cmd =
                                sendGameMsg <| WS.PurchaseShip shipType newName
                        in
                            ( HomeworldShipEmporium CompletePurchase, cmd )

                _ ->
                    ( model, Cmd.none )

        ShipPurchaseEditName newName ->
            case model of
                HomeworldShipEmporium (AskShipName shipType _) ->
                    ( HomeworldShipEmporium (AskShipName shipType newName), Cmd.none )

                _ ->
                    ( model, Cmd.none )


viewHomeworld : PS.PlayerStatus -> Star.HomeworldData -> Html GameMsgType
viewHomeworld playerStatus hData =
    let
        renderShipEmporium state =
            div [ class "homeworld" ]
                [ h1 [] [ text hData.homeworldName ]
                , div [ class "homeworld__market--ship" ]
                    [ h2 [] [ text "starship emporium" ]
                    , renderStarshipEmporium state hData.homeworldShipsAvailable
                        |> Html.map HomeworldMsg
                    ]
                , if MaybeE.isJust playerStatus.cmdShip then
                    button [ onClick LeavePlanet ] [ text "take off" ]
                  else
                    text ""
                ]
    in
        case playerStatus.subModel of
            PS.Homeworld homeworldId subModel ->
                case subModel of
                    Homeworld ->
                        div [ class "homeworld" ]
                            [ h1 [] [ text hData.homeworldName ]
                            , button [ onClick <| HomeworldMsg ClickEmporium ] [ text "Starship Emporium" ]
                            , button [ onClick <| HomeworldMsg ClickDocklands ] [ text "Docklands Outfitter" ]
                            , if MaybeE.isJust playerStatus.cmdShip then
                                button [ onClick LeavePlanet ] [ text "take off" ]
                              else
                                text ""
                            ]

                    HomeworldShipEmporium state ->
                        renderShipEmporium state

                    HomeworldDocklands state ->
                        Html.map HomeworldMsg <| renderDocklands playerStatus hData state

            _ ->
                div [] [ text "I am not on a homeworld model.  weird." ]


renderDocklands : PS.PlayerStatus -> Star.HomeworldData -> DocklandsState -> Html HomeworldMsgType
renderDocklands playerStatus hData state =
    let
        shipFigsAvail =
            Maybe.withDefault 0 <| Maybe.map (\s -> s.shipMaxFighters - s.shipFighters) playerStatus.cmdShip

        maxFigsAfford =
            floor <| playerStatus.thePlayer.playerCash / hData.homeworldFighterCost

        maxFigs =
            min
                shipFigsAvail
                maxFigsAfford
    in
        div [ class "homeworld" ]
            [ h1 [] [ text hData.homeworldName ]
            , h2 [] [ text "docklands outfitters" ]
            , case state of
                AskFighterAmount initFig ->
                    div [ class "homeworld__market--docklands" ]
                        [ form [ onSubmit FighterPurchaseComplete ]
                            [ text "how many fighters would you like to buy?"
                            , input
                                [ Attr.type_ "number"
                                , Attr.max <| String.fromInt maxFigs
                                , Attr.min "0"
                                , Attr.value <| String.fromInt initFig
                                , onInput FighterPurchaseAmount
                                ]
                                []
                            , button [] [ text "Buy" ]
                            ]
                        ]

                _ ->
                    div [ class "homeworld__market--docklands" ]
                        [ h3 [] [ text "starship consumables:" ]
                        , if state == CompleteFighterPurchase then
                            div [] [ text "Fighter purchase complete." ]
                          else
                            div [] []
                        , table []
                            [ tbody []
                                [ tr []
                                    [ td [] [ text "Fighters" ]
                                    , td [] [ text <| flToString hData.homeworldFighterCost ]
                                    , td [] [ button [ onClick <| FighterPurchaseTrigger maxFigs ] [ text "Buy" ] ]
                                    ]
                                ]
                            ]
                        ]
            , button [ onClick ClickHome ] [ text "Back to planet" ]
            ]


renderStarshipEmporium : ShipEmporiumState -> List Ship.ShipTemplate -> Html HomeworldMsgType
renderStarshipEmporium model shipTypes =
    let
        infoText t =
            td [] [ text t ]

        infoNum n =
            td [] [ text <| flToString n ]

        infoInt n =
            td [] [ text <| String.fromInt n ]

        renderRow shipType =
            tr []
                [ infoText shipType.shipTClass
                , infoInt shipType.shipTFighters
                , infoNum shipType.shipTCargo
                , infoNum <| shipType.shipTMiningRateFuel * 60.0 * 60.0
                , infoNum <| shipType.shipTMiningRateMetal * 60.0 * 60.0
                , infoNum shipType.shipTPrice
                , button [ onClick <| ShipPurchaseTrigger shipType ] [ text "Purchase" ]
                ]

        headerCol name =
            th [] [ text name ]

        content =
            case model of
                AskShipName shipType newShipName ->
                    [ form [ onSubmit ShipPurchaseComplete ]
                        [ text "What would you like to name your new ship?"
                        , input
                            [ onInput ShipPurchaseEditName
                            , Attr.placeholder <| "new " ++ shipType.shipTClass ++ " ship"
                            ]
                            []
                        , button [] [ text "Launch ship." ]
                        ]
                    , button [ onClick ShipPurchaseLeave ] [ text "Back" ]
                    ]

                _ ->
                    -- default, just show the ship listing...
                    let
                        extraText =
                            case model of
                                NotEnoughCash ->
                                    [ div [] [ text "You don't have enough credits to purchase that ship." ] ]

                                CompletePurchase ->
                                    [ div [] [ text "You've purchased a new ship! It should be in orbit soon." ] ]

                                _ ->
                                    -- default, don't show any other message
                                    []
                    in
                        extraText
                            ++ [ table []
                                    [ thead []
                                        [ tr []
                                            [ headerCol "Ship Class"
                                            , headerCol "Max fighters"
                                            , headerCol "Max cargo"
                                            , headerCol "fuel mining rate"
                                            , headerCol "metal mining rate"
                                            , headerCol "price"
                                            ]
                                        ]
                                    , tbody [] <| List.map renderRow shipTypes
                                    ]
                               ]
    in
        div [] <| content ++ [ button [ onClick ClickHome ] [ text "Back to planet" ] ]


viewPlanet_ : PS.PlayerStatus -> Star.PlanetData -> Html GameMsgType
viewPlanet_ playerStatus planetData =
    text "Not Implemented."


viewPlanet : PS.PlayerStatus -> Html GameMsgType
viewPlanet playerStatus =
    let
        mPlanet =
            playerStatus.currentPlanet
    in
        case mPlanet of
            Just planet ->
                case planet of
                    Star.PlanetHomeworld hData ->
                        viewHomeworld playerStatus hData

                    Star.Planet pdata ->
                        viewPlanet_ playerStatus pdata

            Nothing ->
                text "Loading planet..."
