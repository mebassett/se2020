module Game.PlayerStatus exposing (PlayerStatus, calcFreeCargoSpace, GameSubModel(..), renderMiningStatus)

import FloatUtils exposing (flToString)
import Game.PlanetModels as Planet
import Generated.Engine.Player as Player
import Generated.Engine.Ship as Ship
import Generated.Engine.Star as Star
import Generated.Engine.Ids as Ids
import Generated.Notifications as Notes
import Html exposing (..)
import Html.Attributes exposing (class, for, href, id, type_)
import Html.Events exposing (onClick, onInput, onSubmit)
import Messages exposing (Msg)
import String exposing (fromInt)


type GameSubModel
    = None
    | Homeworld Ids.PlanetNum Planet.HomeworldModel


type alias PlayerStatus =
    { currentStar : Maybe ( Star.Star, List Star.ShortPlanet )
    , currentPlanet : Maybe Star.Planet
    , cmdShip : Maybe Ship.Ship
    , systemShips : List ( Player.ShortPlayer, Ship.ShortShip )
    , playerShips : List Ship.Ship
    , thePlayer : Player.Player
    , notifications : List ( Notes.NotificationStatus, Notes.GameMsg )
    , subModel : GameSubModel
    }


renderMiningStatus : Ship.ShipStatus -> Html Msg
renderMiningStatus miningMode =
    case miningMode of
        Ship.SSSpaceMiningMetal ->
            text "mining metal"

        Ship.SSSpaceMiningFuel ->
            text "mining fuel"

        Ship.SSSpaceAttacking _ _ ->
            text "attacking"

        Ship.SSSpaceAttackedBy _ ->
            text "under attack"

        Ship.SSDocked ->
            text "docked"

        Ship.SSPlanet _ ->
            text "landed on planet"

        _ ->
            text "orbiting"


calcFreeCargoSpace : Ship.Ship -> Float
calcFreeCargoSpace ship =
    let
        cargo =
            ship.shipCargo
    in
        ship.shipMaxCargo - (cargo.cargoMetal + cargo.cargoFuel + cargo.cargoElec + cargo.cargoOrgan)
