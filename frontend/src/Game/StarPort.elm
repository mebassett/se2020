module Game.StarPort exposing (viewStarPort)

import Game.PlayerStatus as PS
import Html exposing (..)
import Html.Attributes as Attr exposing (class, for, href, id, type_)
import Html.Events exposing (onClick, onInput, onSubmit)
import Messages exposing (GameMsgType(..))
import Generated.Engine.Ship as Ship
import Generated.Engine.Star as Star
import FloatUtils exposing (flToString)


affordsPort : Star.Star -> Ship.Ship -> Bool
affordsPort star ship =
    star.starPort /= Nothing && ship.shipStatus == Ship.SSDocked


viewStarPort_ : Star.Star -> Star.Port -> Ship.Ship -> Html GameMsgType
viewStarPort_ star starPort ship =
    let
        fuelMarket =
            div [ class "starPort__market--fuel" ]
                [ strong [] [ text "Fuel" ]
                , text <| flToString starPort.portFuel
                , button
                    [ class "starPort__market-button"
                    , Attr.disabled <| starPort.portFuel <= 0
                    , onClick <| DockBuyFuel 10
                    ]
                    [ text "Buy" ]
                , button
                    [ class "starPort__market--button"
                    , Attr.disabled <| ship.shipCargo.cargoFuel <= 0
                    , onClick DockSellFuel
                    ]
                    [ text "Sell" ]
                ]

        metalMarket =
            div [ class "starPort__market--metal" ]
                [ strong [] [ text "Metal" ]
                , text <| flToString starPort.portMetal
                , button
                    [ class "starPort__market-button"
                    , Attr.disabled <| starPort.portMetal <= 0
                    , onClick <| DockBuyMetal 10
                    ]
                    [ text "Buy" ]
                , button
                    [ class "starPort__market--button"
                    , Attr.disabled <| ship.shipCargo.cargoMetal <= 0
                    , onClick DockSellMetal
                    ]
                    [ text "Sell" ]
                ]
    in
        div [ class "starPort" ]
            [ h1 [] [ text "Star Port" ]
            , fuelMarket
            , metalMarket
            , button [ onClick UndockFromPort ] [ text "Undock" ]
            ]


viewStarPort : PS.PlayerStatus -> Html GameMsgType
viewStarPort playerStatus =
    let
        mStar =
            playerStatus.currentStar

        mShip =
            playerStatus.cmdShip
    in
        case ( mStar, mShip ) of
            ( Just ( star, _ ), Just ship ) ->
                case ( star.starPort, ship.shipStatus ) of
                    ( Just starPort, Ship.SSDocked ) ->
                        viewStarPort_ star starPort ship

                    ( _, _ ) ->
                        text "you aren't actually at a port.  weird."

            ( _, _ ) ->
                text "no star or no ship (or neither!) How strange."
