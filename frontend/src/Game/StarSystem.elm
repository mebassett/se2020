module Game.StarSystem exposing (viewStarSystem)

import EngineUtils exposing (starNumToString)
import EverySet exposing (EverySet, toList)
import FloatUtils exposing (flToString)
import Game.PlayerStatus as PS
import Generated.Engine.Ids as Ids
import Generated.Engine.Star as Star
import Generated.Engine.Ship as Ship
import Generated.Engine.Player as Player
import Html exposing (..)
import Html.Attributes exposing (class, for, href, id, type_)
import Html.Attributes as Attr
import Html.Events exposing (onClick, onInput, onSubmit)
import Messages exposing (GameMsgType(..))
import Maybe.Extra as MaybeE


renderPort : Maybe Star.Port -> Html GameMsgType
renderPort mPort =
    case mPort of
        Just p ->
            li [ class "__port" ]
                [ h1 [] [ text "port" ]
                , button [ onClick DockToPort ] [ text "dock" ]
                ]

        Nothing ->
            li [] []


renderFuel : Float -> Html GameMsgType
renderFuel fuelAmt =
    li [ class "__resource" ] <|
        [ h1 [] [ text "Fuel" ]
        , h1 [] [ text <| flToString fuelAmt ]
        ]
            ++ if fuelAmt > 0 then
                [ button [ onClick MineFuel ] [ text "Mine" ] ]
               else
                []


renderMetal : Float -> Html GameMsgType
renderMetal metalAmt =
    li [ class "__resource" ] <|
        [ h1 [] [ text "Metal" ]
        , h1 [] [ text <| flToString metalAmt ]
        ]
            ++ if metalAmt > 0 then
                [ button [ onClick MineMetal ] [ text "Mine" ] ]
               else
                []


renderOtherShips : Maybe Ship.Ship -> List ( Player.ShortPlayer, Ship.ShortShip ) -> Html GameMsgType
renderOtherShips mCmdShip ships =
    let
        msgBtn p =
            button [] [ text "msg" ]

        attackBtn s =
            button [ onClick <| AttackShip s.shortShipId ] [ text "attack" ]

        stopAttackBtn =
            button [ onClick StopAttack ] [ text "stop attack" ]

        cmdShipResponse p s =
            MaybeE.unwrap [ msgBtn p ]
                (\cmdShip ->
                    case cmdShip.shipStatus of
                        Ship.SSSpaceAttacking targetId _ ->
                            if targetId == s.shortShipId then
                                [ stopAttackBtn, msgBtn p ]
                            else
                                [ attackBtn s, msgBtn p ]

                        Ship.SSSpaceAttackedBy _ ->
                            [ msgBtn p ]

                        _ ->
                            if cmdShip.shipFighters > 0 then
                                [ attackBtn s, msgBtn p ]
                            else
                                [ msgBtn p ]
                )
                mCmdShip

        renderRow ( p, s ) =
            tr []
                [ td [] [ text p.shortPlayerName ]
                , td [] [ text <| s.shortShipClass ++ " class with " ++ String.fromInt s.shortShipFighters ++ " fighters." ]
                , td [] <| cmdShipResponse p s
                ]
    in
        table [] [ tbody [] <| List.map renderRow ships ]


viewStarSystem : PS.PlayerStatus -> Html GameMsgType
viewStarSystem playerStatus =
    let
        mStar =
            playerStatus.currentStar

        otherShips =
            playerStatus.systemShips

        playerShips =
            playerStatus.playerShips

        renderStarSystemMap =
            div [ class "__mapContainer" ]
                [ canvas [ id "uniMap" ] []
                ]

        renderStarLinks : EverySet Ids.StarNum -> Html GameMsgType
        renderStarLinks starNums =
            let
                renderStarLink : Ids.StarNum -> Html GameMsgType
                renderStarLink starNum =
                    li [] [ button [ onClick <| MoveToStar starNum ] [ text <| starNumToString starNum ] ]
            in
                ul [ class "starSystem__starNav" ] <| List.map renderStarLink <| toList starNums

        renderPlanets : List Star.ShortPlanet -> Html GameMsgType
        renderPlanets planets =
            div [ class "__starPlanets" ] <|
                if List.length planets == 0 then
                    []
                else
                    let
                        renderRow p =
                            li []
                                [ h2 [] [ text "planet" ]
                                , h1 [] [ text p.shortPlanetName ]
                                , button [ onClick <| LandOnPlanet p.shortPlanetId ] [ text "Land" ]
                                ]
                    in
                        [ ul [] <| List.map renderRow planets ]

        renderUserShips ships =
            let
                renderRow s =
                    tr []
                        [ td [] [ text s.shipName ]
                        , td [] [ text <| s.shipClass ++ " with " ++ String.fromInt s.shipFighters ++ " fighters." ]
                        , td [] [ renderStatus s.shipStatus ]
                        , td [] [ renderTowBtn s ]
                        , td [] [ button [ onClick <| ChangeCommand s.shipId ] [ text "command" ] ]
                        ]

                renderTowBtn ship =
                    case ship.shipStatus of
                        Ship.SSTowedBy shipId ->
                            case playerStatus.cmdShip of
                                Just s ->
                                    if s.shipId == shipId then
                                        button [ onClick <| StopTowShip ship.shipId ] [ text "Stop towing" ]
                                    else
                                        button [ onClick <| TowShip ship.shipId ] [ text "tow" ]

                                Nothing ->
                                    text ""

                        _ ->
                            button [ onClick <| TowShip ship.shipId ] [ text "tow" ]

                renderStatus status =
                    case status of
                        Ship.SSSpaceIdle ->
                            text "Idle"

                        Ship.SSSpaceMiningMetal ->
                            text "Mining metal"

                        Ship.SSSpaceMiningFuel ->
                            text "Mining fuel"

                        Ship.SSSpaceAttacking _ _ ->
                            text "Attacking"

                        Ship.SSSpaceAttackedBy _ ->
                            text "Under Attack"

                        Ship.SSTowedBy shipId ->
                            case playerStatus.cmdShip of
                                Just s ->
                                    if s.shipId == shipId then
                                        text "Towing"
                                    else
                                        text "Towed by other ship"

                                Nothing ->
                                    text "Towed by other ship"

                        Ship.SSDocked ->
                            text "BUG!"

                        Ship.SSPlanet _ ->
                            text "BUG!"
            in
                table [] [ tbody [] <| List.map renderRow ships ]
    in
        div [ class "gameMain starSystem" ] <|
            case mStar of
                Nothing ->
                    [ text "no star, that's weird." ]

                Just ( star, planets ) ->
                    [ h1 [ class "__starTitle" ] [ text <| star.starName ++ " System" ]
                    , renderStarSystemMap
                    , div [ class "__starResources" ]
                        [ ul []
                            [ renderPort star.starPort
                            , renderFuel star.starFuel
                            , renderMetal star.starMetal
                            ]
                        ]
                    , renderPlanets planets
                    , div [ class "__starLinks" ]
                        [ h2 [] [ text "warp links" ]
                        , renderStarLinks star.starLinks
                        ]
                    , div [ class "__yourShips" ]
                        (if List.length playerShips < 2 then
                            -- one is the cmdShip, which we don't show.
                            []
                         else
                            [ h4 [] [ text "Your ships:" ]
                            , renderUserShips <|
                                List.filter
                                    (\ship ->
                                        case playerStatus.cmdShip of
                                            Just cmdShip ->
                                                cmdShip.shipId /= ship.shipId

                                            Nothing ->
                                                True
                                    )
                                    playerShips
                            ]
                        )
                    , div [ class "__otherShips" ]
                        (if List.length otherShips == 0 then
                            []
                         else
                            [ h4 [] [ text "Others' ships:" ]
                            , renderOtherShips playerStatus.cmdShip otherShips
                            ]
                        )
                    ]
