module EngineUtils exposing (starNumToString)

import Generated.Engine.Ids as Ids
import String exposing (fromInt)


starNumToString : Ids.StarNum -> String
starNumToString starNum =
    case starNum of
        Ids.StarNum starId ->
            fromInt starId
