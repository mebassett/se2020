{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeApplications #-}
module Instances where
import           Engine
import Notifications
import Websockets.WSServer(WSClientMsg, WSServerMsg)

import qualified Data.Aeson                    as Aeson
import           Data.Aeson                     ( Value )
import           Data.Set                       ( Set )
import Data.Text (Text)
import qualified Data.UUID                     as UUID
import           Language.Haskell.To.Elm        ( HasElmDecoder(..)
                                                , HasElmEncoder(..)
                                                , HasElmType(..)
                                                , defaultOptions
                                                , deriveElmTypeDefinition
                                                , deriveElmJSONDecoder
                                                , deriveElmJSONEncoder
                                                )
import qualified Language.Elm.Expression       as Expression
import qualified Language.Elm.Type             as Type
import           Servant                        ( (:>) 
                                                , Header'
                                                , Strict
                                                )   
import           Servant.Auth.Server            ( Auth
                                                , JWT)
import           Servant.To.Elm (HasElmEndpoints(..))
import User (User, SignupInfo, LoginInfo)

instance HasElmEndpoints api => HasElmEndpoints (Auth '[JWT] user :> api) where
  elmEndpoints' = elmEndpoints' 
                    @(Header' 
                      '[ Strict ] "Authorization" JWT :> api)

instance HasElmType JWT where
  elmType = "String.String"
                      
instance HasElmEncoder Text JWT where
  elmEncoder = "Config.prepareJwt"

instance HasElmType UUID.UUID where
  elmType = "String.String"

instance HasElmDecoder Value UUID.UUID where
  elmDecoder = "Json.Decode.string"

instance HasElmEncoder Value UUID.UUID where
  elmEncoder = "Json.Encode.string"

instance HasElmType a => HasElmType (Set a) where
  elmType = Type.App "EverySet.EverySet" (elmType @a)

instance HasElmDecoder Value a => HasElmDecoder Value(Set a) where
  elmDecoder = Expression.App "Config.decodeSet" (elmDecoder @Value @a)

instance HasElmEncoder Value a => HasElmEncoder Value (Set a)  where
  elmEncoder = Expression.App "Config.encodeSet" (elmEncoder @Value @a)

instance HasElmType WSServerMsg where
  elmDefinition =
    Just $ deriveElmTypeDefinition @WSServerMsg defaultOptions "Generated.Websockets.WSServerMsg"
instance HasElmDecoder Value WSServerMsg where
  elmDecoderDefinition = Just $ deriveElmJSONDecoder @WSServerMsg
    defaultOptions
    Aeson.defaultOptions
    "Generated.Websockets.wsServerMsgDecoder"
instance HasElmEncoder Value WSServerMsg where
  elmEncoderDefinition = Just $ deriveElmJSONEncoder @WSServerMsg
    defaultOptions
    Aeson.defaultOptions
    "Generated.Websockets.wsServerMsgEncoder"

instance HasElmType WSClientMsg where
  elmDefinition =
    Just $ deriveElmTypeDefinition @WSClientMsg defaultOptions "Generated.Websockets.WSClientMsg"
instance HasElmDecoder Value WSClientMsg where
  elmDecoderDefinition = Just $ deriveElmJSONDecoder @WSClientMsg
    defaultOptions
    Aeson.defaultOptions
    "Generated.Websockets.wsClientMsgDecoder"
instance HasElmEncoder Value WSClientMsg where
  elmEncoderDefinition = Just $ deriveElmJSONEncoder @WSClientMsg
    defaultOptions
    Aeson.defaultOptions
    "Generated.Websockets.wsClientMsgEncoder"

instance HasElmType MsgType where
  elmDefinition =
    Just $ deriveElmTypeDefinition @MsgType defaultOptions "Generated.Notifications.MsgType"
instance HasElmDecoder Value MsgType where
  elmDecoderDefinition = Just $ deriveElmJSONDecoder @MsgType
    defaultOptions
    Aeson.defaultOptions
    "Generated.Notifications.msgTypeDecoder"
instance HasElmEncoder Value MsgType where
  elmEncoderDefinition = Just $ deriveElmJSONEncoder @MsgType
    defaultOptions
    Aeson.defaultOptions
    "Generated.Notifications.msgTypeEncoder"
instance HasElmType GameMsg where
  elmDefinition =
    Just $ deriveElmTypeDefinition @GameMsg defaultOptions "Generated.Notifications.GameMsg"
instance HasElmDecoder Value GameMsg where
  elmDecoderDefinition = Just $ deriveElmJSONDecoder @GameMsg
    defaultOptions
    Aeson.defaultOptions
    "Generated.Notifications.gameMsgDecoder"
instance HasElmEncoder Value GameMsg where
  elmEncoderDefinition = Just $ deriveElmJSONEncoder @GameMsg
    defaultOptions
    Aeson.defaultOptions
    "Generated.Notifications.gameMsgEncoder"

instance HasElmType NotificationStatus where
  elmDefinition =
    Just $ deriveElmTypeDefinition @NotificationStatus defaultOptions "Generated.Notifications.NotificationStatus"
instance HasElmDecoder Value NotificationStatus where
  elmDecoderDefinition = Just $ deriveElmJSONDecoder @NotificationStatus
    defaultOptions
    Aeson.defaultOptions
    "Generated.Notifications.notificationStatusDecoder"
instance HasElmEncoder Value NotificationStatus where
  elmEncoderDefinition = Just $ deriveElmJSONEncoder @NotificationStatus
    defaultOptions
    Aeson.defaultOptions
    "Generated.Notifications.notificationStatusEncoder"





instance HasElmType StarNum where
  elmDefinition =
    Just $ deriveElmTypeDefinition @StarNum defaultOptions "Generated.Engine.Ids.StarNum"
instance HasElmDecoder Value StarNum where
  elmDecoderDefinition = Just $ deriveElmJSONDecoder @StarNum
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Ids.starNumDecoder"
instance HasElmEncoder Value StarNum where
  elmEncoderDefinition = Just $ deriveElmJSONEncoder @StarNum
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Ids.starNumEncoder"

instance HasElmType PlanetNum where
  elmDefinition =
    Just $ deriveElmTypeDefinition @PlanetNum defaultOptions "Generated.Engine.Ids.PlanetNum"
instance HasElmDecoder Value PlanetNum where
  elmDecoderDefinition = Just $ deriveElmJSONDecoder @PlanetNum
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Ids.planetNumDecoder"
instance HasElmEncoder Value PlanetNum where
  elmEncoderDefinition = Just $ deriveElmJSONEncoder @PlanetNum
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Ids.planetNumEncoder"

instance HasElmType ShipNum where
  elmDefinition =
    Just $ deriveElmTypeDefinition @ShipNum defaultOptions "Generated.Engine.Ids.ShipNum"
instance HasElmDecoder Value ShipNum where
  elmDecoderDefinition = Just $ deriveElmJSONDecoder @ShipNum
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Ids.shipNumDecoder"
instance HasElmEncoder Value ShipNum where
  elmEncoderDefinition = Just $ deriveElmJSONEncoder @ShipNum
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Ids.shipNumEncoder"
instance HasElmType PlayerNum where
  elmDefinition =
    Just $ deriveElmTypeDefinition @PlayerNum defaultOptions "Generated.Engine.Ids.PlayerNum"
instance HasElmDecoder Value PlayerNum where
  elmDecoderDefinition = Just $ deriveElmJSONDecoder @PlayerNum
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Ids.playerNumDecoder"
instance HasElmEncoder Value PlayerNum where
  elmEncoderDefinition = Just $ deriveElmJSONEncoder @PlayerNum
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Ids.playerNumEncoder"

instance HasElmType Star where
  elmDefinition =
    Just $ deriveElmTypeDefinition @Star defaultOptions "Generated.Engine.Star.Star"
instance HasElmDecoder Value Star where
  elmDecoderDefinition = Just $ deriveElmJSONDecoder @Star
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Star.decoder"
instance HasElmEncoder Value Star where
  elmEncoderDefinition = Just $ deriveElmJSONEncoder @Star
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Star.encoder"
instance HasElmType DisplayableStar where
  elmDefinition =
    Just $ deriveElmTypeDefinition @DisplayableStar defaultOptions "Generated.Engine.Star.DisplayableStar"
instance HasElmDecoder Value DisplayableStar where
  elmDecoderDefinition = Just $ deriveElmJSONDecoder @DisplayableStar
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Star.displayableDecoder"
instance HasElmEncoder Value DisplayableStar where
  elmEncoderDefinition = Just $ deriveElmJSONEncoder @DisplayableStar
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Star.displayableEncoder"
instance HasElmType Planet where
  elmDefinition =
    Just $ deriveElmTypeDefinition @Planet defaultOptions "Generated.Engine.Star.Planet"
instance HasElmDecoder Value Planet where
  elmDecoderDefinition = Just $ deriveElmJSONDecoder @Planet
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Star.planetDecoder"
instance HasElmEncoder Value Planet where
  elmEncoderDefinition = Just $ deriveElmJSONEncoder @Planet
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Star.planetEncoder"
instance HasElmType PlanetData where
  elmDefinition =
    Just $ deriveElmTypeDefinition @PlanetData defaultOptions "Generated.Engine.Star.PlanetData"
instance HasElmDecoder Value PlanetData where
  elmDecoderDefinition = Just $ deriveElmJSONDecoder @PlanetData
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Star.planetDataDecoder"
instance HasElmEncoder Value PlanetData where
  elmEncoderDefinition = Just $ deriveElmJSONEncoder @PlanetData
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Star.planetDataEncoder"
instance HasElmType HomeworldData where
  elmDefinition =
    Just $ deriveElmTypeDefinition @HomeworldData defaultOptions "Generated.Engine.Star.HomeworldData"
instance HasElmDecoder Value HomeworldData where
  elmDecoderDefinition = Just $ deriveElmJSONDecoder @HomeworldData
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Star.homeworldDataDecoder"
instance HasElmEncoder Value HomeworldData where
  elmEncoderDefinition = Just $ deriveElmJSONEncoder @HomeworldData
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Star.homeworldDataEncoder"
instance HasElmType Port where
  elmDefinition =
    Just $ deriveElmTypeDefinition @Port defaultOptions "Generated.Engine.Star.Port"
instance HasElmDecoder Value Port where
  elmDecoderDefinition = Just $ deriveElmJSONDecoder @Port
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Star.portDecoder"
instance HasElmEncoder Value Port where
  elmEncoderDefinition = Just $ deriveElmJSONEncoder @Port
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Star.portEncoder"


instance HasElmType Ship where
  elmDefinition =
    Just $ deriveElmTypeDefinition @Ship defaultOptions "Generated.Engine.Ship.Ship"
instance HasElmDecoder Value Ship where
  elmDecoderDefinition = Just $ deriveElmJSONDecoder @Ship
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Ship.decoder"
instance HasElmEncoder Value Ship where
  elmEncoderDefinition = Just $ deriveElmJSONEncoder @Ship
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Ship.encoder"
instance HasElmType ShortShip where
  elmDefinition =
    Just $ deriveElmTypeDefinition @ShortShip defaultOptions "Generated.Engine.Ship.ShortShip"
instance HasElmDecoder Value ShortShip where
  elmDecoderDefinition = Just $ deriveElmJSONDecoder @ShortShip
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.ShortShip.shortDecoder"
instance HasElmEncoder Value ShortShip where
  elmEncoderDefinition = Just $ deriveElmJSONEncoder @ShortShip
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.ShortShip.shortEncoder"
instance HasElmType ShipStatus where
  elmDefinition = Just $ deriveElmTypeDefinition @ShipStatus
    defaultOptions
    "Generated.Engine.Ship.ShipStatus"
instance HasElmDecoder Value ShipStatus where
  elmDecoderDefinition = Just $ deriveElmJSONDecoder @ShipStatus
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Ship.smmDecoder"
instance HasElmEncoder Value ShipStatus where
  elmEncoderDefinition = Just $ deriveElmJSONEncoder @ShipStatus
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Ship.smmEncoder"
instance HasElmType Cargo where
  elmDefinition =
    Just $ deriveElmTypeDefinition @Cargo defaultOptions "Generated.Engine.Ship.Cargo"
instance HasElmDecoder Value Cargo where
  elmDecoderDefinition = Just $ deriveElmJSONDecoder @Cargo
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Ship.cargoDecoder"
instance HasElmEncoder Value Cargo where
  elmEncoderDefinition = Just $ deriveElmJSONEncoder @Cargo
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Ship.cargoEncoder"

instance HasElmType ShipTemplate where
  elmDefinition =
    Just $ deriveElmTypeDefinition @ShipTemplate defaultOptions "Generated.Engine.Ship.ShipTemplate"
instance HasElmDecoder Value ShipTemplate where
  elmDecoderDefinition = Just $ deriveElmJSONDecoder @ShipTemplate
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Ship.shipTemplateDecoder"
instance HasElmEncoder Value ShipTemplate where
  elmEncoderDefinition = Just $ deriveElmJSONEncoder @ShipTemplate
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Ship.shipTemplateEncoder"


instance HasElmType Player where
  elmDefinition =
    Just $ deriveElmTypeDefinition @Player defaultOptions "Generated.Engine.Player.Player"
instance HasElmDecoder Value Player where
  elmDecoderDefinition = Just $ deriveElmJSONDecoder @Player
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Player.playerDecoder"
instance HasElmEncoder Value Player where
  elmEncoderDefinition = Just $ deriveElmJSONEncoder @Player
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Player.playerEncoder"
instance HasElmType GameUser where
  elmDefinition =
    Just $ deriveElmTypeDefinition @GameUser defaultOptions "Generated.Engine.Player.GameUser"
instance HasElmDecoder Value GameUser where
  elmDecoderDefinition = Just $ deriveElmJSONDecoder @GameUser
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Player.gameUserDecoder"
instance HasElmEncoder Value GameUser where
  elmEncoderDefinition = Just $ deriveElmJSONEncoder @GameUser
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Player.gameUserEncoder"


instance HasElmType ShortPlayer where
  elmDefinition =
    Just $ deriveElmTypeDefinition @ShortPlayer defaultOptions "Generated.Engine.Player.ShortPlayer"
instance HasElmDecoder Value ShortPlayer where
  elmDecoderDefinition = Just $ deriveElmJSONDecoder @ShortPlayer
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Player.shortPlayerDecoder"
instance HasElmEncoder Value ShortPlayer where
  elmEncoderDefinition = Just $ deriveElmJSONEncoder @ShortPlayer
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Player.shortPlayerEncoder"

instance HasElmType ShortPlanet where
  elmDefinition =
    Just $ deriveElmTypeDefinition @ShortPlanet defaultOptions "Generated.Engine.Star.ShortPlanet"
instance HasElmDecoder Value ShortPlanet where
  elmDecoderDefinition = Just $ deriveElmJSONDecoder @ShortPlanet
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Star.shortPlanetDecoder"
instance HasElmEncoder Value ShortPlanet where
  elmEncoderDefinition = Just $ deriveElmJSONEncoder @ShortPlanet
    defaultOptions
    Aeson.defaultOptions
    "Generated.Engine.Star.shortPlanetEncoder"

instance HasElmType User where
  elmDefinition =
    Just $ deriveElmTypeDefinition @User defaultOptions "Generated.User.User"
instance HasElmDecoder Value User where
  elmDecoderDefinition = Just $ deriveElmJSONDecoder @User
    defaultOptions
    Aeson.defaultOptions
    "Generated.User.userDecoder"
instance HasElmEncoder Value User where
  elmEncoderDefinition = Just $ deriveElmJSONEncoder @User
    defaultOptions
    Aeson.defaultOptions
    "Generated.User.userEncoder"

instance HasElmType LoginInfo where
  elmDefinition =
    Just $ deriveElmTypeDefinition @LoginInfo defaultOptions "Generated.User.LoginInfo"
instance HasElmDecoder Value LoginInfo where
  elmDecoderDefinition = Just $ deriveElmJSONDecoder @LoginInfo
    defaultOptions
    Aeson.defaultOptions
    "Generated.User.loginInfoDecoder"
instance HasElmEncoder Value LoginInfo where
  elmEncoderDefinition = Just $ deriveElmJSONEncoder @LoginInfo
    defaultOptions
    Aeson.defaultOptions
    "Generated.User.loginInfoEncoder"

instance HasElmType SignupInfo where
  elmDefinition =
    Just $ deriveElmTypeDefinition @SignupInfo defaultOptions "Generated.User.SignupInfo"
instance HasElmDecoder Value SignupInfo where
  elmDecoderDefinition = Just $ deriveElmJSONDecoder @SignupInfo
    defaultOptions
    Aeson.defaultOptions
    "Generated.User.signupInfoDecoder"
instance HasElmEncoder Value SignupInfo where
  elmEncoderDefinition = Just $ deriveElmJSONEncoder @SignupInfo
    defaultOptions
    Aeson.defaultOptions
    "Generated.User.signupInfoEncoder"
