{-# LANGUAGE TypeApplications #-}
module Main where

import           Control.Monad                  ( forM_ )
import qualified Data.HashMap.Lazy             as HashMap
import           Data.Text.Prettyprint.Doc.Internal
                                                ( Doc )
import           Data.Text                      ( Text
                                                , unpack
                                                )
import qualified Language.Elm.Pretty           as Pretty
import           Language.Haskell.To.Elm        ( jsonDefinitions )

import           Servant.Auth.Server            ( JWT )
import           Servant.To.Elm
import           System.Directory               ( createDirectoryIfMissing )

import           Servant.ServantApi             ( SEApi )
import qualified Engine
import qualified Notifications
import qualified Websockets.WSServer           as WS
import           Instances                      ( )
import qualified User


prefix :: FilePath
prefix = "frontend/src/"

createModule :: (Doc ann) -> FilePath -> [Text] -> IO ()
createModule _ _ [] = return ()
createModule doc path [fileName] =
  writeFile (prefix ++ path ++ (unpack fileName) ++ ".elm") $ show doc
createModule doc path (folderName : rest) = do
  createDirectoryIfMissing False $ prefix ++ path ++ (unpack folderName)
  createModule doc (path ++ (unpack folderName) ++ "/") rest


main :: IO ()
main = do
  let definitions =
        map (elmEndpointDefinition "Config.urlBase" ["Generated", "Api"])
            (elmEndpoints @(SEApi '[JWT]))
          <> jsonDefinitions @Engine.Ship
          <> jsonDefinitions @Engine.ShortShip
          <> jsonDefinitions @Engine.ShipTemplate
          <> jsonDefinitions @Engine.Cargo
          <> jsonDefinitions @Engine.ShipStatus
          <> jsonDefinitions @Engine.Star
          <> jsonDefinitions @Engine.DisplayableStar
          <> jsonDefinitions @Engine.Planet
          <> jsonDefinitions @Engine.ShortPlanet
          <> jsonDefinitions @Engine.PlanetData
          <> jsonDefinitions @Engine.HomeworldData
          <> jsonDefinitions @Engine.Port
          <> jsonDefinitions @Engine.Player
          <> jsonDefinitions @Engine.GameUser
          <> jsonDefinitions @Engine.ShortPlayer
          <> jsonDefinitions @Engine.ShipNum
          <> jsonDefinitions @Engine.StarNum
          <> jsonDefinitions @Engine.PlanetNum
          <> jsonDefinitions @Engine.PlayerNum
          <> jsonDefinitions @Notifications.Notification
          <> jsonDefinitions @Notifications.MsgType
          <> jsonDefinitions @Notifications.GameMsg
          <> jsonDefinitions @Notifications.NotificationStatus
          <> jsonDefinitions @WS.WSServerMsg
          <> jsonDefinitions @WS.WSClientMsg
          <> jsonDefinitions @User.User
          <> jsonDefinitions @User.LoginInfo
          <> jsonDefinitions @User.SignupInfo

      modules = Pretty.modules definitions
  print modules
  forM_ (HashMap.toList modules) $ \(moduleName, contents) -> do
    createModule contents "" moduleName
