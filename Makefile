elmcodegen = $(stack path --local-install-root)/bin/elm-code-gen
elmsrc = $(find ./ -path ./frontend/src/ -prune -o -type f -name '*.elm')

hssrc = $(find ./src -type f -name '*.hs')
hsbin = $(stack path --local-install-root)/bin/se2020-server


ifdef DEBUG
  ELMFLAGS =--debug
  HASKELLFLAGS =--fast
  SASSFLAGS = --source-map --embed-source-map --embed-sources 
  TSFLAGS =  --strict --inlineSourceMap --inlineSources
else
  ELMFLAGS =--optimize
  HASKELLFLAGS = 
  SASSFLAGS = --no-source-map
  TSFLAGS = --removeComments --strict

endif

all: $(hsbin) $(elmcodegen) www/index.html www/main.js

www/main.js www/index.html : $(elmsrc) frontend/src/Generated/Api.elm
	npm run build


frontend/src/Generated/Api.elm: $(elmsrc) $(hsbin)
	stack exec elm-code-gen


$(elmsrcgen): $(elmcodegen)
	stack exec elm-code-gen

$(elmcodegen): $(hssrc)
	stack build $(HASKELLFLAGS)

hsbin: $(hsbin)

$(hsbin): $(hssrc)
	stack build $(HASKELLFLAGS)

.PHONY: clean
clean:
	stack clean
	rm www/*js
	rm www/*html
